package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.io.InputStream;

import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

/**
 *
 * @author Shamim Ahmad
 *
 */
public class MongoFileInfoImpl extends BaseFileInfo {

	private GridFSFile internalObject;
	
	public MongoFileInfoImpl(){
		super();
	}
	
	public MongoFileInfoImpl(String fileName){
		this();
		this.fileName = fileName;
	}

	public MongoFileInfoImpl(GridFSFile internalObject){
		this.internalObject = internalObject;
	}

	@Override
	public String idToString() {
		String strId = null;
		if(id!=null){
			strId = id.toString();
		}
		return strId;
	}

	@Override
	public InputStream getInputStream() {
		if(this.internalObject instanceof GridFSDBFile){
			GridFSDBFile dbFile = (GridFSDBFile) this.internalObject;
			return dbFile.getInputStream();	
		}
		return null;
	}

	@Override
	public Object getInternalObject() {
		return internalObject;
	}

	@Override
	public void setInternalObject(Object internalObject) {
		if(internalObject instanceof GridFSFile){
			this.internalObject = (GridFSFile) internalObject;	
		}
	}
}
