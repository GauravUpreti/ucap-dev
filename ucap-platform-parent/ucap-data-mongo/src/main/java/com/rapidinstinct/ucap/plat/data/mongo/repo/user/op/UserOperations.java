package com.rapidinstinct.ucap.plat.data.mongo.repo.user.op;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Component
public interface UserOperations {

	public List<UserDetailsDO> getUsersByRole(String orgId, List<String> role);

	public List<UserDetailsDO> getUsers(Set<String> userIds);

	public UserDetailsDO getUser(String userId);

}
