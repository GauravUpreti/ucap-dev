package com.rapidinstinct.ucap.plat.data.mongo.repo.user.op;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO;

/**
 * @author Shiva Kalgudi
 *
 */

@Component
public interface VerificationOperations {

	public VerificationDO findById(String id);

	public VerificationDO findByToken(String token);

	public VerificationDO findByUserContextRef(String userId, String contextRefId);

	public VerificationDO findContextRefAndContext(String contextRefId, int context);

	public VerificationDO add(VerificationDO vdo);

	public VerificationDO update(VerificationDO vdo);

	public void delete(VerificationDO vdo);

}
