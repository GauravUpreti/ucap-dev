package com.rapidinstinct.ucap.plat.data.mongo.collection;

import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentStatus;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class BaseDocumentDO extends CommonProperties {

	protected int status;
	
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	/********************************************************************************/
	public boolean isStatusActive(){
		return status == DocumentStatus.PUBLISHED_ACTIVATED.getValue();
	}

	public boolean isUnPublished(){
		return status < DocumentStatus.PUBLISHED_ACTIVATED.getValue();
	}

	public boolean isStatusInActive(){
		return status == DocumentStatus.PUBLISHED_INACTIVATED.getValue();
	}
	
	public boolean isStatusAuthored(){
		return status == DocumentStatus.UNPUBLISHED_AUTHORED.getValue();
	}

}