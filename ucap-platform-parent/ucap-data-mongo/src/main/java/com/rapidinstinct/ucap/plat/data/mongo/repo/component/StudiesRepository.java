package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudiesDO;

/**
 * 
 * @author Gaurav Upreti
 * 
 */

@Repository
public interface StudiesRepository extends MongoRepository<StudiesDO, String>{

	@Query("{VisitDate :{'$ne' : null}}")
	public List<StudiesDO> findAll();
}
