package com.rapidinstinct.ucap.plat.data.mongo.collection;

import org.springframework.data.mongodb.core.index.Indexed;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class BaseUserDocument extends CommonProperties{
	
	@Indexed protected String userId;
	protected String displayName;
	protected String profileImg;
	
	public BaseUserDocument(){
		super();
	}
	
	public BaseUserDocument(String userId, String orgId){
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProfileImg() {
		return profileImg;
	}

	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}
	
}
