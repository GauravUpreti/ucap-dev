package com.rapidinstinct.ucap.plat.data.mongo.collection.oauth2;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "authRefreshToken")
@TypeAlias("authRefreshToken")
public class AuthRefreshTokenDO implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id	protected String id;
	protected Date createdDT;
	protected Date modifiedDT;
    private int version;
    private String tokenValue;
    private OAuth2RefreshToken refreshToken;
    private OAuth2Authentication authentication;

    public AuthRefreshTokenDO(){
    	this.createdDT = new Date();
    	this.modifiedDT = new Date();
    }
    
    public AuthRefreshTokenDO(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
    	this();
        this.refreshToken = refreshToken;
        this.authentication = authentication;
        this.tokenValue = refreshToken.getValue();
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public OAuth2RefreshToken getRefreshToken() {
        return refreshToken;
    }

    public OAuth2Authentication getAuthentication() {
        return authentication;
    }

	public String getId() {
		return id;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public Date getModifiedDT() {
		return modifiedDT;
	}

	public int getVersion() {
		return version;
	}

	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}
	
}
