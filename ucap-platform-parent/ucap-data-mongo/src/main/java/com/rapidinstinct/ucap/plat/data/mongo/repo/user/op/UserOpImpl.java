package com.rapidinstinct.ucap.plat.data.mongo.repo.user.op;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Component
public class UserOpImpl implements UserOperations {
	@Autowired private MongoOperations ops;
	@Autowired private UserDetailsRepository dtlRepo;
	
	@Override
	public List<UserDetailsDO> getUsersByRole(String orgId, List<String> role) {
		String keyOrgGrps = DocumentField.ORG_GROUPS.getValue();
		String keyOrgId = DocumentField.ORGID.getValue();
		String keyGrpIds = DocumentField.GRP_IDS.getValue();
		
		Query query = new Query(Criteria.where(keyOrgGrps).elemMatch(Criteria.where(keyOrgId).is(orgId).and(keyGrpIds).in(role)));
		query.fields().include(DocumentField.ID.getValue());
		List<UserGroupsDO> grpUsers = ops.find(query, UserGroupsDO.class);
		Set<String> ugps = new HashSet<String>();
		for(UserGroupsDO grpUser:grpUsers){
			ugps.add(grpUser.getUserId());
		}
		
		return (List<UserDetailsDO>) dtlRepo.findAll(ugps);
	}

	@Override
	public List<UserDetailsDO> getUsers(Set<String> userIds) {
		Query query = new Query(Criteria.where(DocumentField.ID.getValue()).in(userIds));
		query.fields().include(DocumentField.ID.getValue());
		query.fields().include(DocumentField.FIRSTNAME.getValue());
		query.fields().include(DocumentField.LASTNAME.getValue());
		return ops.find(query, UserDetailsDO.class);
	}

	@Override
	public UserDetailsDO getUser(String userId) {
		Query query = new Query(Criteria.where(DocumentField.ID.getValue()).is(userId));
		query.fields().include(DocumentField.ID.getValue());
		query.fields().include(DocumentField.FIRSTNAME.getValue());
		query.fields().include(DocumentField.LASTNAME.getValue());
		return ops.findOne(query, UserDetailsDO.class);
	}
}