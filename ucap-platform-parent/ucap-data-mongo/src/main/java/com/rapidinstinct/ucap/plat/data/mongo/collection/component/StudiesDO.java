package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Gaurav Upreti
 *
 */
@Document(collection="InstanceViews")
@TypeAlias ("InstanceViews")
public class StudiesDO {
	
	private String StudyOID;
	private String LocationOID;
	private Date visitDate;

	public String getStudyOID() {
		return StudyOID;
	}
	public void setStudyOID(String studyOID) {
		StudyOID = studyOID;
	}
	public String getLocationOID() {
		return LocationOID;
	}
	public void setLocationOID(String locationOID) {
		LocationOID = locationOID;
	}
	public Date getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
		
}
