package com.rapidinstinct.ucap.plat.data.mongo.repo.user.op;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.VerificationRepository;

/**
 * @author Shiva Kalgudi
 *
 */

@Component
public class VerificationOpImpl implements VerificationOperations {

	@Autowired VerificationRepository repo;
	
	@Override
	public VerificationDO findById(String id) {
		if(StringUtils.isNotBlank(id)){
			return repo.findOne(id);
		}
		return null;
	}

	@Override
	public VerificationDO findByToken(String token) {
		if(StringUtils.isNotBlank(token)){
			List<VerificationDO> resList = repo.findByToken(token);
			if(resList.size() > 0){
				return resList.get(0);
			}
		}
		return null;
	}

	@Override
	public VerificationDO add(VerificationDO vdo) {
		if(vdo!=null){
			return repo.save(vdo); 
		}
		return null;
	}

	@Override
	public VerificationDO update(VerificationDO vdo) {
		if(vdo!=null){
			return repo.save(vdo); 
		}
		return null;
	}

	@Override
	public void delete(VerificationDO vdo) {
		if(vdo!=null){
			repo.delete(vdo); 
		}
	}

	@Override
	public VerificationDO findByUserContextRef(String userId, String contextRefId) {
		return repo.findByCreatedForAndContextRefId(userId, contextRefId);
	}

	@Override
	public VerificationDO findContextRefAndContext(String contextRefId, int context) {
		return repo.findByContextRefIdAndContext(contextRefId, context);
	}

}
