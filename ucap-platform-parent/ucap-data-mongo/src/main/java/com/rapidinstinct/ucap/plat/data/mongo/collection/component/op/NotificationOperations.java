package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.NotificationsDO;

/**
*
* @author Gaurav Upreti
*
*/

@Component
public interface NotificationOperations {

	public List<NotificationsDO> findNotificationsbyStudyName(String studyName);

	public List<NotificationsDO> findPendingNotificationsbyStudyName(String studyName, int status);
	
	public List<NotificationsDO> findAllNotifications();

	public NotificationsDO findByNotificationId(String id);

	public void updateNotification(NotificationsDO notificationDO);

}
