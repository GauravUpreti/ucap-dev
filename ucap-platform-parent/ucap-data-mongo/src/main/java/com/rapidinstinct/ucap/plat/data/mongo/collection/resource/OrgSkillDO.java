package com.rapidinstinct.ucap.plat.data.mongo.collection.resource;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "orgSkill")
@TypeAlias("orgSkill")
public class OrgSkillDO {

	@Id	private String id;
	@Indexed private String orgId;
	@Indexed private String name;
	private String creator;
	
	public OrgSkillDO(){ }
	
	
	public OrgSkillDO(String orgId, String name){ 
		this();
		this.orgId = orgId;
		this.name = name;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getId() {
		return id;
	}
	
}
