package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;

@Document(collection="UserAccess")
@TypeAlias ("UserAccess")
public class UserAccessDO extends BaseDocumentDO {

	private List<String> studyName;
	private String siteName;
	private String userId;
	
	public List<String> getStudyName() {
		return studyName;
	}
	public void setStudyName(List<String> studyName) {
		this.studyName = studyName;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
