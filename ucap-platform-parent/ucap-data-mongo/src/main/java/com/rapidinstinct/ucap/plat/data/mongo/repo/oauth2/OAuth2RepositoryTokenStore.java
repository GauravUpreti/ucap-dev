package com.rapidinstinct.ucap.plat.data.mongo.repo.oauth2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.rapidinstinct.ucap.plat.cache.CacheService;
import com.rapidinstinct.ucap.plat.cache.CacheServiceFactory;
import com.rapidinstinct.ucap.plat.data.mongo.collection.oauth2.AuthAccessTokenDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.oauth2.AuthRefreshTokenDO;

/**
 * @author Gaurav Upreti
 *
 */

public class OAuth2RepositoryTokenStore implements TokenStore {

	@Autowired private AuthAccessTokenRepository accessTokenRepo;
    @Autowired private AuthRefeshTokenRepository refreshTokenRepo;
    private AuthenticationKeyGenerator authKeyGen = new DefaultAuthenticationKeyGenerator();
    private CacheService cacheService = CacheServiceFactory.getMemcachedService();
    
	@Override
	public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
		return readAuthentication(token.getValue());
		
	}

	@Override
	public OAuth2Authentication readAuthentication(String tokenValue) {
		AuthAccessTokenDO  token = (AuthAccessTokenDO) cacheService.get(tokenValue);
		if(token == null){
			token = accessTokenRepo.findByTokenValue(tokenValue);
			cacheService.put(token.getTokenValue(), token);
		}
		return token.getAuthentication();
	}

	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		AuthAccessTokenDO accToken = new AuthAccessTokenDO(token, authentication, authKeyGen.extractKey(authentication));
		accessTokenRepo.save(accToken);
		cacheService.put(accToken.getTokenValue(), accToken);
	}

	@Override
	public OAuth2AccessToken readAccessToken(String tokenValue) {
		AuthAccessTokenDO  token = (AuthAccessTokenDO) cacheService.get(tokenValue);
		if(token == null){
			token = accessTokenRepo.findByTokenValue(tokenValue);
			if(token!=null){
				cacheService.put(token.getTokenValue(), token);	
			}
		}
		return token == null ? null : token.getAccessToken();
	}

	@Override
	public void removeAccessToken(OAuth2AccessToken token) {
		AuthAccessTokenDO accToken = accessTokenRepo.findByTokenValue(token.getValue());
        if(accToken != null) {
        	accessTokenRepo.delete(accToken.getId());
        }
	}

	@Override
	public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
		AuthRefreshTokenDO refToken = new AuthRefreshTokenDO(refreshToken, authentication);
		refreshTokenRepo.save(refToken);
	}

	@Override
	public OAuth2RefreshToken readRefreshToken(String tokenValue) {
		AuthRefreshTokenDO refToken = refreshTokenRepo.findByTokenValue(tokenValue);
		return refToken == null ? null : refToken.getRefreshToken();
	}

	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
		AuthRefreshTokenDO refToken = refreshTokenRepo.findByTokenValue(token.getValue());
		return refToken == null ? null : refToken.getAuthentication();
	}

	@Override
	public void removeRefreshToken(OAuth2RefreshToken token) {
		AuthRefreshTokenDO refToken = refreshTokenRepo.findByTokenValue(token.getValue());
		if(refToken!=null){
			refreshTokenRepo.delete(refToken.getId());
		}
	}

	@Override
	public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
		AuthAccessTokenDO accToken = accessTokenRepo.findByRefreshToken(refreshToken.getValue());
		if(accToken!=null){
			accessTokenRepo.delete(accToken.getId());
		}
	}

	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
		AuthAccessTokenDO accToken =  accessTokenRepo.findByAuthenticationId(authKeyGen.extractKey(authentication));
        return accToken == null ? null : accToken.getAccessToken();
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
        List<AuthAccessTokenDO> tokens = accessTokenRepo.findByClientIdAndUserName(clientId, userName);
        return extractAccessTokens(tokens);
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
        List<AuthAccessTokenDO> tokens = accessTokenRepo.findByClientId(clientId);
        return extractAccessTokens(tokens);
	}

    private Collection<OAuth2AccessToken> extractAccessTokens(List<AuthAccessTokenDO> accTokens) {
        List<OAuth2AccessToken> accessTokens = new ArrayList<OAuth2AccessToken>();
        for(AuthAccessTokenDO accToken : accTokens) {
            accessTokens.add(accToken.getAccessToken());
        }
        return accessTokens;
    }

}
