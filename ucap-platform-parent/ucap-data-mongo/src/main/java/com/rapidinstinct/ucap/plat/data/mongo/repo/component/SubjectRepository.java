package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.SubjectDO;

/**
 * 
 * @author Gaurav Upreti
 * 
 */

@Repository
public interface SubjectRepository extends MongoRepository<SubjectDO, String>{

	@Query("{'studyOID':?0}")
	public List<SubjectDO> findSubjectStatusByStudyOID(String studyName);
	
}
