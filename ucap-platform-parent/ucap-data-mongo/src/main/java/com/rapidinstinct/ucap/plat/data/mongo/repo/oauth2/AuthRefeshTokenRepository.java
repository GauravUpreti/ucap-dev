package com.rapidinstinct.ucap.plat.data.mongo.repo.oauth2;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.oauth2.AuthRefreshTokenDO;

/**
 * @author Gaurav Upreti
 *
 */

@Repository
public interface AuthRefeshTokenRepository extends MongoRepository<AuthRefreshTokenDO, String>{

    public AuthRefreshTokenDO findByTokenValue(String tokenValue);

}
