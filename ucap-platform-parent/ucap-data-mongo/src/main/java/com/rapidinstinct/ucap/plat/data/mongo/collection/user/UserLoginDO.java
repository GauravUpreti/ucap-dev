package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "userLogin")
@TypeAlias("userLogin")
public class UserLoginDO {

	@Id protected String id;
	private int loginSource = LoginSource.LOCAL.getValue();
	private UserLoginLocalDO local;
	private UserLoginSocialDO linkedin;
	private UserLoginSocialDO google;
	private UserLoginSocialDO facebook;
	private Date createdDT;
	private Date lastLoginDT;
	private int status;
	private String firstName;
	private String lastName;
	private String job;
	private String function;
	private int phone;
	private String company;
	private String email;
	private String country;
	private String comments;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public UserLoginDO() { }
	
	public UserLoginDO(int loginSource){
		this.loginSource = loginSource;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(int loginSource) {
		this.loginSource = loginSource;
	}

	public UserLoginLocalDO getLocal() {
		return local;
	}

	public void setLocal(UserLoginLocalDO local) {
		this.local = local;
	}

	public UserLoginSocialDO getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(UserLoginSocialDO linkedin) {
		this.linkedin = linkedin;
	}

	public UserLoginSocialDO getGoogle() {
		return google;
	}

	public void setGoogle(UserLoginSocialDO google) {
		this.google = google;
	}

	public UserLoginSocialDO getFacebook() {
		return facebook;
	}

	public void setFacebook(UserLoginSocialDO facebook) {
		this.facebook = facebook;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public void setCreatedDT(Date createdDT) {
		this.createdDT = createdDT;
	}

	public Date getLastLoginDT() {
		return lastLoginDT;
	}

	public void setLastLoginDT(Date lastLoginDT) {
		this.lastLoginDT = lastLoginDT;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
		
	public boolean isActiveUser(){
		return LoginStatus.ACTIVE.getValue() == status;
	}
	
	public boolean isVerificationPending(){
		return LoginStatus.VERIFICATION_PENDING.getValue() == status;
	}

	public boolean isInactive(){
		return LoginStatus.INACTIVE.getValue() == status;
	}

	public boolean isActiveEmail(String emailId){
		if(this.getLocal()!=null){
			return this.getLocal().isActiveEmail(emailId);
		}
		return false;
	}

	public boolean isEmailExists(String emailId){
		if(this.getLocal()!=null){
			return this.getLocal().isEmailExists(emailId);
		}
		return false;
	}
	
	public static enum LoginSource {
		
		LOCAL(1), SOCIAL_LINKEDIN(2), SOCIAL_FACEBOOOK(3), SOCIAL_GOOGLE(4);

		private LoginSource(final int text) {
			this.source = text;
		}

		private final int source;

		public int getValue() {
			return source;
		}
		
		public static LoginSource getType(int value){
			LoginSource types [] = LoginSource.values();
			for(LoginSource type:types){
				if(value == type.getValue()){
					return type;
				}
			}
			return null;
		}
	}

	
	public static enum LoginStatus {
		
		CREATED(1), VERIFICATION_PENDING(10), ACTIVE(20), INACTIVE(30);

		private LoginStatus(final int text) {
			this.text = text;
		}

		private final int text;

		public int getValue() {
			return text;
		}
	}

}
