package com.rapidinstinct.ucap.plat.data.mongo.repo.user;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;

/**
 * @author Shiva Kalgudi
 *
 */
@Repository
public interface UserMembershipRequestRepository extends MongoRepository<UserMembershipRequestDO, String> {

	public List<UserMembershipRequestDO> findByUserIdAndStatus(String userId, int status);

	public List<UserMembershipRequestDO> findByUserIdAndOrgIdAndStatus(String userId, String orgId, int status);
	
	public List<UserMembershipRequestDO> findByUserIdAndOrgId(String userId, String orgId);

	public UserMembershipRequestDO findByUserIdAndVtoken(String userId, String vtoken);

}
