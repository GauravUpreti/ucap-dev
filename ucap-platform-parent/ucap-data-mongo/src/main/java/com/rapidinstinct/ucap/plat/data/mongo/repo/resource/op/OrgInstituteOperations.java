package com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgInstituteDO;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public interface OrgInstituteOperations {

	public OrgInstituteDO add(String orgId, String name);

	public OrgInstituteDO add(String orgId, String name, String creator);

	public List<OrgInstituteDO> getInstitutes(String orgId);

	public List<OrgInstituteDO> searchInstitutes(String term, String orgId);

	public OrgInstituteDO findInstitute(String name, String orgId);

}
