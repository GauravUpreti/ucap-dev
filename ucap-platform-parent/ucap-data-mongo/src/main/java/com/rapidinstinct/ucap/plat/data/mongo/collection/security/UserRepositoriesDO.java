package com.rapidinstinct.ucap.plat.data.mongo.collection.security;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "userRepositories")
@TypeAlias("userRepositories")
public class UserRepositoriesDO {

	@Id private String id;
	private String userId;
	private String orgId;
	private String gradId;
	List<String> repoIds;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getOrgId() {
		return orgId;
	}
	
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	public String getGradId() {
		return gradId;
	}
	
	public void setGradId(String gradId) {
		this.gradId = gradId;
	}
	
	public List<String> getRepoIds() {
		return repoIds;
	}
	
	public void setRepoIds(List<String> repoIds) {
		this.repoIds = repoIds;
	}
}
