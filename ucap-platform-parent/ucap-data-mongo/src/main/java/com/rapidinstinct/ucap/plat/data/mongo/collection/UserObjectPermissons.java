package com.rapidinstinct.ucap.plat.data.mongo.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class UserObjectPermissons extends CommonProperties {

	protected String userId;
	protected String objectId;
	protected int objectType;
	protected List<String> roles = new ArrayList<String>();
	private int mask; //not in use
	
	public UserObjectPermissons(){
		super();
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getObjectId() {
		return objectId;
	}
	
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public int getObjectType() {
		return objectType;
	}
	
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	
	public List<String> getRoles() {
		return roles;
	}
	
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public int getMask() {
		return mask;
	}

	public void setMask(int mask) {
		this.mask = mask;
	}
	
	public boolean hasRole(String role){
		return roles.contains(role);
	}
	
	public boolean addRole(String addRole){
		boolean result = false;
		if(!hasRole(addRole)){
			roles.add(addRole);
			result = true;
		}
		return result;
	}
	
	public boolean removeRole(String removeRole){
		boolean result = false;
		if(hasRole(removeRole)){
			roles.remove(removeRole);
			result = true;
		}
		return result;
	}

}