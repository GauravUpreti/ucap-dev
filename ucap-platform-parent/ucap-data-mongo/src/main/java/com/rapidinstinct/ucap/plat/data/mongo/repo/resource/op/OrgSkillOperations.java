package com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgSkillDO;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public interface OrgSkillOperations {

	public OrgSkillDO add(String orgId, String skill);

	public OrgSkillDO add(String orgId, String skill, String creator);

	public List<OrgSkillDO> getSkills(String orgId);

	public List<OrgSkillDO> searchSkills(String term, String orgId);

	public OrgSkillDO findSkill(String skill, String orgId);

}
