package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.GroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.RBMComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.SubjectDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.GroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.NotificationsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.RBMRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.StudyRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.SubjectRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.UserAccessRepository;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Component
public class RBMCommonOperationsImpl implements RBMCommonOperations{

	@Autowired private RBMRepository repo;
	@Autowired private SubjectRepository subRepo;
	@Autowired private UserAccessRepository uRepo;
	@Autowired private StudyRepository sRepo;
	@Autowired private GroupsRepository groupsRepo;
	@Autowired private NotificationsRepository notificationsRepo;
	
	
	@Override
	public RBMComponentDO add(RBMComponentDO thresholdsDO) {
		return repo.save(thresholdsDO);
	}

	@Override
	public List<RBMComponentDO> read(String study) {
		return repo.findByStudy(study);
	}

	@Override
	public RBMComponentDO update(RBMComponentDO thresholdsDO) {
		return repo.save(thresholdsDO);
	}

	@Override
	public RBMComponentDO findById(String id) {
		return repo.findOne(id);
	}

	@Override
	public UserAccessDO findByUserId(String userId) {
		return uRepo.findByUserId(userId);
	}

	@Override
	public List<StudyDO> findByStudyId(String studyName) {
		return sRepo.findStudy(studyName);
	}
	
	@Override
	public RBMComponentDO findbygrpKRIandkri(String grpKRI,String kri) {
		return repo.findbygrpKRIandkri(grpKRI, kri);
	}

	@Override
	public List<GroupsDO> findAllGroups() {
		return groupsRepo.findAll();
	}

	@Override
	public Integer findPendingNotification(String studyName, int status) {
		return notificationsRepo.findByStudyAndStatus(studyName, status).size();
	}

	@Override
	public Integer countKriByStudyName(String studyName) {
		List<RBMComponentDO> rbmDO= repo.findKriByStudyName(studyName);
		HashSet<String> kriCount =  new HashSet<String>();
		for(int i=0;i<rbmDO.size();i++){
			kriCount.add(rbmDO.get(i).getKri());
		}
		return kriCount.size();
	}

	@Override
	public Integer countEnrolledByStudyName(String studyName) {
		List<SubjectDO> subDO = subRepo.findSubjectStatusByStudyOID(studyName);
		Integer enrolledCount = 0;
		String subjectStatus = null;
		for(int i=0;i<subDO.size();i++){
			subjectStatus = subDO.get(i).getSubjectStatus();
			if(subjectStatus.equals("Enrolled") || subjectStatus.equals("Off Study")){
				enrolledCount++;
			}
		}
		return enrolledCount;
	}

	@Override
	public Integer countSiteByStudyName(String studyName) {
		List<SubjectDO> subDO = subRepo.findSubjectStatusByStudyOID(studyName);
		HashSet<String> siteCount =  new HashSet<String>();
		for(int i=0;i<subDO.size();i++){
			siteCount.add(subDO.get(i).getSiteName());
		}
		return siteCount.size();
	}
}