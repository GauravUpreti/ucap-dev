package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.RegistrationDO;

/**
 * 
 * @author Gaurav Upreti
 * 
 */

@Repository
public interface RegisterRepository extends MongoRepository<RegistrationDO, String>{

}
