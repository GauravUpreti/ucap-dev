package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.GroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.RBMComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Component
public interface RBMCommonOperations{
	
	public RBMComponentDO add(RBMComponentDO thresholdsDO);
	
	public List<RBMComponentDO> read(String study);
	
	public UserAccessDO findByUserId(String userId);
	
	public RBMComponentDO update(RBMComponentDO thresholdsDO);
	
	public RBMComponentDO findById(String id);
	
	public List<StudyDO> findByStudyId(String studyName);

	public RBMComponentDO findbygrpKRIandkri(String group, String kri);

	public List<GroupsDO> findAllGroups();
	
	public Integer findPendingNotification (String studyName, int status);
	
	public Integer countKriByStudyName(String studyName);
	
	public Integer countEnrolledByStudyName (String studyName);
	
	public Integer countSiteByStudyName (String studyName);

}