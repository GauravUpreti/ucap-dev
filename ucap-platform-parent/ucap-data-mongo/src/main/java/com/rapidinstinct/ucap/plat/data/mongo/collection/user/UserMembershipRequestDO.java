package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "userMembershipRequest")
@TypeAlias("userMembershipRequest")
public class UserMembershipRequestDO {

	@Id	private String id;
	private String orgId; //organization id
	private String userId; //user who is invited to join.
	private String membershipType; //access group code
	private int status; //status of the request
	private String creator; //request creator id
	protected Date createdDT;
	protected Date modifiedDT;
	
	//verification token
	protected String vtoken;
	
	public UserMembershipRequestDO(){
		this.status = Status.NEW.getValue();
		this.createdDT = new Date();
		this.modifiedDT = new Date();
	}
	
	public UserMembershipRequestDO(String orgId, String userId, String membershipType, String creator){
		this();
		this.orgId = orgId;
		this.userId = userId;
		this.creator = creator;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOrgId() {
		return orgId;
	}
	
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getMembershipType() {
		return membershipType;
	}
	
	public void setMembershipType(String membershipType) {
		this.membershipType = membershipType;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public Date getModifiedDT() {
		return modifiedDT;
	}

	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}

	public String getCreator() {
		return creator;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public String getVtoken() {
		return vtoken;
	}

	public void setVtoken(String vtoken) {
		this.vtoken = vtoken;
	}

	//Request status values
	public static enum Status {
		NEW(10), //newly created request
		ACCEPTED(20), //request is accepted
		REJECTED(30), //request is rejected
		;

		private Status(final int text) {
			this.status = text;
		}
		
		private final int status;
		
		public int getValue() {
			return status;
		}
	}
}
