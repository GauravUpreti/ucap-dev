package com.rapidinstinct.ucap.plat.data.mongo.converter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import com.mongodb.DBObject;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;

/**
 * @author Shiva Kalgudi
 *
 */

@ReadingConverter
public class AuthReadConverter implements Converter<DBObject, OAuth2Authentication>{
	
	public static enum AuthField {
		storedRequest, requestParameters, clientId, scope, userAuthentication, principal, 
		credentials, authorities, details, role, userId, email, roles,
	}

	@Override
	public OAuth2Authentication convert(DBObject source) {
        OAuth2Request oAuth2Request = extractOAuth2Request(source);
        Authentication userAuthentication = extractUserAuth(source);
        return new OAuth2Authentication(oAuth2Request, userAuthentication );
    }

	@SuppressWarnings({ "unchecked", "rawtypes"})
	private Authentication extractUserAuth(DBObject source) {
		DBObject userAuthorization = (DBObject) source.get(AuthField.userAuthentication.toString());
		Object details = extractPrincipal(userAuthorization);
		String principal = (String) userAuthorization.get(AuthField.principal.toString());
		Collection<GrantedAuthority> authorities = getAuthorities((List) userAuthorization.get(AuthField.authorities.toString()));
		UsernamePasswordAuthenticationToken userAuthentication = new UsernamePasswordAuthenticationToken(principal,null, authorities);
        userAuthentication.setDetails(details);
		return userAuthentication;
	}

	
	@SuppressWarnings({ "unchecked"})
    private Object extractPrincipal(DBObject userAuthorization) {
		DBObject userDetails = (DBObject) userAuthorization.get(AuthField.details.toString());
		String userId = (String) userDetails.get(AuthField.userId.toString());
		String email = (String) userDetails.get(AuthField.email.toString());
		List<String> roles = (List<String>) userDetails.get(AuthField.roles.toString());
		UserDetailsBO details = new UserDetailsBO(userId);
		details.setEmail(email);
		for(String role : roles){
			Role roleEn = Role.getType(role);
			if(roleEn != null){
				details.addRole(roleEn);
			}
		}
		return details;
    }

	
	@SuppressWarnings("unchecked")
	private OAuth2Request extractOAuth2Request(DBObject source) {
		DBObject storedRequest = (DBObject)source.get(AuthField.storedRequest.toString());
        String clientId = (String) storedRequest.get(AuthField.clientId.toString());
        Map<String, String> reqParams = (Map<String, String>) storedRequest.get(AuthField.requestParameters.toString());
        List<String> scopes = (List<String>)storedRequest.get(AuthField.scope.toString());
        Set<String> uniqScopes = new HashSet<String>(scopes);
        return new OAuth2Request(reqParams,clientId, null, true, uniqScopes, null, null, null, null);
	}


    private Collection<GrantedAuthority> getAuthorities(List<Map<String, String>> authorities) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>(authorities.size());
        for(Map<String, String> authority : authorities) {
            grantedAuthorities.add(new SimpleGrantedAuthority(authority.get(AuthField.role.toString())));
        }
        return grantedAuthorities;
    }

}