package com.rapidinstinct.ucap.plat.data.mongo.collection.tools;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "attachmentInfo")
@TypeAlias("attachmentInfo")
public class AttachmentInfoDO {
	
	@Id private String fid;
	private String fname;
	private String ftype; //mime-type
	private long flength; 
	private String cid; //component-id
	private int ctype; //component-type
	private int cSubType; //component-sub-type
	private String orgId;
	private String creator;
	private Date createdDT;
	private int status;
	private int version;
	protected Map<String, Object> extData = new HashMap<String, Object>();

	public AttachmentInfoDO(){
		this.createdDT = new Date();
	}

	public AttachmentInfoDO(String fid, String cid, int ctype, String orgId, String creator){
		super();
		this.fid = fid;
		this.cid = cid;
		this.ctype = ctype;
		this.orgId = orgId;
		this.creator = creator;
	}
	
	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getFtype() {
		return ftype;
	}

	public void setFtype(String ftype) {
		this.ftype = ftype;
	}

	public long getFlength() {
		return flength;
	}

	public void setFlength(long flength) {
		this.flength = flength;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public int getCtype() {
		return ctype;
	}

	public void setCtype(int ctype) {
		this.ctype = ctype;
	}

	public int getcSubType() {
		return cSubType;
	}

	public void setcSubType(int cSubType) {
		this.cSubType = cSubType;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public void setCreatedDT(Date createdDT) {
		this.createdDT = createdDT;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Map<String, Object> getExtData() {
		return extData;
	}

	public void setExtData(Map<String, Object> extData) {
		this.extData = extData;
	}
}