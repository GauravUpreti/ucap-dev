package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "userDetails")
@TypeAlias("userDetails")
public class UserDetailsDO {
	@Id protected String userId;
	protected String firstName;
	protected String lastName;
	protected String middleName;
	protected String profileImgURL;
	private Set<String> orgIds = new HashSet<String>();
	private Set<String> emails = new HashSet<String>();
	private String email;
	private Set<ContactNumber> workCNS = new HashSet<ContactNumber>();
	private Set<ContactNumber> personalCNS = new HashSet<ContactNumber>();
	private Date dob;
	private Address address;
	private Date createdDT;
	private Date modifiedDT;
	private Map<String, Object> metadata;
	private List<Role> roles = new ArrayList<Role>();

	//pending email ids
	private Set<String> vpEmails = new HashSet<String>();
	
	@Transient private List<OrgRoles> orgInfos = new ArrayList<OrgRoles>();
	
	public UserDetailsDO(){ 
		this.createdDT = new Date();
		this.modifiedDT = new Date();
	}
	
	public UserDetailsDO(String userId){
		this();
		this.userId = userId;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}	
	
	public String getUserId() {
		return userId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public String getProfileImgURL() {
		return profileImgURL;
	}
	
	public void setProfileImgURL(String profileImgURL) {
		this.profileImgURL = profileImgURL;
	}
	
	public Set<String> getOrgIds() {
		return orgIds;
	}
	
	public void setOrgIds(Set<String> orgIds) {
		this.orgIds = orgIds;
	}
	
	public boolean addOrgId(String orgId){
		return this.orgIds.add(orgId);
	}

	public boolean removeOrgId(String orgId){
		return this.orgIds.remove(orgId);
	}

	public boolean isMemberOfOrg(String orgId){
		return this.orgIds.contains(orgId);
	}
	
	public Set<String> getEmails() {
		return emails;
	}

	public void setEmails(Set<String> emails) {
		this.emails = emails;
	}

	public boolean addEmail(String emailId) {
		return this.emails.add(emailId);
	}

	public boolean addEmails(List<String> emailId) {
		return this.emails.addAll(emailId);
	}

	public boolean removeEmail(String emailId) {
		return this.emails.remove(emailId);
	}

	public boolean removeEmails(List<String> emailIds) {
		return this.emails.removeAll(emailIds);
	}
	
	//total emails count
	public int getActiveEmailsCount(){
		return this.emails.size();
	}
	
	public Set<ContactNumber> getWorkCNS() {
		return workCNS;
	}
	public void addWorkCN(ContactNumber workCN) {
		this.personalCNS.add(workCN);
	}

	public void setWorkCNS(Set<ContactNumber> workCNS) {
		this.workCNS = workCNS;
	}

	public Set<ContactNumber> getPersonalCNS() {
		return personalCNS;
	}

	public void setPersonalCNS(Set<ContactNumber> personalCNS) {
		this.personalCNS = personalCNS;
	}

	public void addPersonalCN(ContactNumber personalCN) {
		this.personalCNS.add(personalCN);
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Map<String, Object> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public void setCreatedDT(Date createdDT) {
		this.createdDT = createdDT;
	}

	public Date getModifiedDT() {
		return modifiedDT;
	}

	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}

	public Set<String> getVpEmails() {
		return vpEmails;
	}

	public void setVpEmails(Set<String> vpemails) {
		this.vpEmails = vpemails;
	}

	public boolean addVpEmail(String emailId) {
		return this.vpEmails.add(emailId);
	}

	public boolean addVpEmails(List<String> emailId) {
		return this.vpEmails.addAll(emailId);
	}

	public boolean removeVpEmail(String emailId) {
		return this.vpEmails.remove(emailId);
	}

	public boolean removeVpEmails(List<String> emailIds) {
		return this.vpEmails.removeAll(emailIds);
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	//check existence of email
	public boolean isExistingEmail(String emailId){
		return this.vpEmails.contains(emailId) || this.emails.contains(emailId);
	}
	
	public boolean isActiveEmail(String emailId){
		return this.emails.contains(emailId);
	}

	//check existence of verification pending email
	public boolean isVpEmail(String emailId){
		return this.vpEmails.contains(emailId);
	}
	
	public List<OrgRoles> getOrgInfos() {
		return orgInfos;
	}

	public void setOrgInfos(List<OrgRoles> orgInfos) {
		this.orgInfos = orgInfos;
	}

	public void addOrgInfo(OrgRoles info){
		this.orgInfos.add(info);
	}

	public static class ContactNumber {
		private String number;
		private String type;
		
		public ContactNumber(){ }
		
		public ContactNumber(String number, String type){ 
			this.number = number;
			this.type = type;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}

	public static class OrgRoles {

		private String orgId;
		private String name;
	    private List<Role> roles = new ArrayList<Role>();

	    public OrgRoles(String orgId, String name){
			this.orgId = orgId;
			this.name = name;
		}
		
		public String getOrgId() {
			return orgId;
		}
		
		public String getName() {
			return name;
		}

		public List<Role> getGrpIds() {
			return roles;
		}

		public void setGrpIds(List<Role> roles) {
			this.roles = roles;
		}
		
	    public boolean hasRole(Role role) {
	        return (this.roles.contains(role));
	    }
		
	    public void addRole(Role role){
			if(role!=null){
				if(!hasRole(role)){
					this.roles.add(role);	
				}
			}
		}
	    
	}
}
