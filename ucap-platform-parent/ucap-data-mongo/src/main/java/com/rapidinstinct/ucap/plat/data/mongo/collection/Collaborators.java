package com.rapidinstinct.ucap.plat.data.mongo.collection;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Gaurav Upreti
 *
 */
public class Collaborators {

	private String owner;
	private Set<String> members;
	
	public Collaborators(){
		this.members = new HashSet<String>();
	}

	public Collaborators(String owner){
		this();
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setMembers(Set<String> members) {
		this.members = members;
	}

	public Set<String> getMembers() {
		return members;
	}

	public boolean addMember(String memberId){
		return this.members.add(memberId);
	}

	public boolean addMembers(Set<String> memberIds){
		for(String member : memberIds){
			if(this.members.contains(member)){
				continue;
			}
			this.members.add(member);
		}
		return true;
	}
	
	public boolean removeMember(String memberId){
		return this.members.remove(memberId);
	}

	public boolean containsMember(String memberId){
		return this.members.contains(memberId);
	}
}
