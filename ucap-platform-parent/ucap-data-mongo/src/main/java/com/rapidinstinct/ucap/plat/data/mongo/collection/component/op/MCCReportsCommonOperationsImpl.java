package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.MCCReportsComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.MCCReportsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.UserAccessRepository;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Component
public class MCCReportsCommonOperationsImpl implements MCCReportsCommonOperations{
	
	@Autowired private MCCReportsRepository mccRepo;
	@Autowired private UserAccessRepository uar;
		
	@Override
	public List<MCCReportsComponentDO> read() {
		return mccRepo.findAll();
	}

	@Override
	public Integer userStudyCount(String userId) {
		return uar.findByUserId(userId).getStudyName().size();
	}

    @Override
	public MCCReportsComponentDO readByModule(String moduleId, String userId) {
		return mccRepo.findByModule(moduleId, userId);
	}
	
	@Override
	public MCCReportsComponentDO readByModuleId(String moduleId) {
		return mccRepo.findByModuleId(moduleId);
	}

	@Override
	public MCCReportsComponentDO readById(String moduleId) {
		return mccRepo.findOne(moduleId);
	}

	@Override
	public MCCReportsComponentDO update(MCCReportsComponentDO mccBO) {
		return mccRepo.save(mccBO);
	}

}