package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.MCCReportsComponentDO;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Component
public interface MCCReportsCommonOperations{
	
	public List<MCCReportsComponentDO> read();
	
	public Integer userStudyCount(String userId);
	
	public MCCReportsComponentDO readByModuleId(String moduleId);
	
	//public List<MCCReportsComponentDO> readByModuleId(String moduleId, String userId);
	
	public MCCReportsComponentDO readById(String mccId);
	
	public MCCReportsComponentDO update(MCCReportsComponentDO mccBO);

	public MCCReportsComponentDO readByModule(String moduleId, String userId);


}