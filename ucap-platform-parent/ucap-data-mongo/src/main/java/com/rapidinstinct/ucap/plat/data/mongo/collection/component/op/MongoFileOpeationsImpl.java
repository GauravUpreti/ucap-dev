package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import com.mongodb.gridfs.GridFSDBFile;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Service
public class MongoFileOpeationsImpl implements FileOperations {
	@Autowired GridFsTemplate operations;
	@Autowired FileInfoMapper mapper;
	
	//Read Study Image
	@Override
	public FileInfo retriveFileByTherapeuticArea(String therapeuticArea) {
		GridFSDBFile dbFile = operations.findOne(idFilterQuery(therapeuticArea));
		return mapper.mapToFileInfo(dbFile);
	}
	
	private Query idFilterQuery(String therapeuticArea){
		return new Query(Criteria.where("therapeuticArea").is(therapeuticArea));
	}
	
	//Read Profile Image
	@Override
	public FileInfo retriveByUserId(String userId){
		GridFSDBFile dbFile = operations.findOne(userImgQuery(userId));
		return mapper.mapToFileInfo(dbFile);
	}
	
	private Query userImgQuery(String userId){
		return new Query(Criteria.where("userId").is(userId));
	}

	//Read Module Image
	@Override
	public FileInfo retriveByModuleId(String moduleId) {
		GridFSDBFile dbFile = operations.findOne(moduleImgQuery(moduleId));
		return mapper.mapToFileInfo(dbFile);
	}
	
	private Query moduleImgQuery(String moduleId){
		return new Query(Criteria.where("moduleId").is(moduleId));
	}
		
}
