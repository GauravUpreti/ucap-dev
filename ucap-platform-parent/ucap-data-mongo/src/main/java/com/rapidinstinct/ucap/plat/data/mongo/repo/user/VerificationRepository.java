package com.rapidinstinct.ucap.plat.data.mongo.repo.user;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO;

/**
 * @author Shiva Kalgudi
 *
 */
@Repository
public interface VerificationRepository extends MongoRepository<VerificationDO, String>  {

	public List<VerificationDO> findByToken(String token);
	
	public VerificationDO findByCreatedForAndContextRefId(String createdFor, String contextRefId);
	
	public VerificationDO findByContextRefIdAndContext(String contextRefId, int context);

}
