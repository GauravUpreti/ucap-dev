package com.rapidinstinct.ucap.plat.data.mongo.repo.op;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentStatus;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class BaseCOImpl<T extends BaseDocumentDO> implements ComponentOperations<T>{
	private static Logger logger = LoggerFactory.getLogger(BaseCOImpl.class);    
	protected Class<T> documentType;
	@Autowired 	protected MongoOperations operations;
	@Value("${component.list.size.limit}") private int listSize;
	
	public MongoOperations getOperations() {
		return operations;
	}

	public void setOperations(MongoOperations operations) {
		this.operations = operations;
	}

	public Class<T> getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Class<T> documentType) {
		this.documentType = documentType;
	}

	/*@Override
	public T lockDocument(T document) {
		Update updateFields = Update.update(DocumentField.UNLOCKED.getValue(), true);
		updateFields.set(DocumentField.UNLOCKER.getValue(), document.getUnlocker());		
		T  result = operations.findAndModify(
				createDocumentUnLockStatusQuery(document, false), 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	
				documentType);
		return result;
	}*/

	private Query createDocumentUnLockStatusQuery(T document, boolean isLocked) {
		return new Query(Criteria.where(DocumentField.ID.getValue()).is(document.getId())
							     .and(DocumentField.UNLOCKED.getValue()).is(isLocked));
	}

	//TODO add index on locked and locker combination - find best way
/*	private Query createDocumentLockStatusQuery(T document, boolean isLocked) {
		Query query = new Query(Criteria.where(DocumentField.ID.getValue()).is(document.getId())
										.and(DocumentField.UNLOCKED.getValue()).is(isLocked)
										.and(DocumentField.UNLOCKED.getValue()).is(document.getUnlocker()));
		return query;
	}*/

	/*@Override
	public T unlockDocument(T document) {
		Update updateFields = Update.update(DocumentField.UNLOCKED.getValue(), false);
		updateFields.set(DocumentField.UNLOCKED.getValue(), null);		
		T  result = operations.findAndModify(
				createDocumentLockStatusQuery(document, true), 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}
*/
	@Override
	public List<T> searchByTitle(String title, String orgId, String userId) {
		int status = DocumentStatus.PUBLISHED_ACTIVATED.getValue();
		String queryStr = "{$text:{$search:'" + title + "'}, "
							+ "orgId:'" + orgId + "', "
									+ "$or :[{status:" + status + "}, "
											+ "{'collaborators.members':'" + userId + "', "
													+ "status:{$lt:" + status + "}}]}";
		Sort sortOrder = new Sort(Direction.DESC, DocumentField.MODIFIEDDT.toString());
		Query query = new BasicQuery(queryStr);
		query.limit(listSize);	
		query.with(sortOrder);
		List<T> targetList  = operations.find(query, getDocumentType());
		return targetList;
	}


	@Override
	public List<T> searchByTitle(String title, String filters, String orgId, String userId) {
		int status = DocumentStatus.PUBLISHED_ACTIVATED.getValue();
		List<T> targetList = null;
		if(StringUtils.isNotBlank(filters)){
			String txtFltr = StringUtils.EMPTY;
			if(StringUtils.isNotBlank(title)){
				txtFltr = "$text:{$search:'" + title + "'}, ";
			}
			String queryStr = "{"
								+ txtFltr
								+ "orgId:'" + orgId 
								+ "', $or :[{status:" + status + "}, {'collaborators.members':'" + userId + "', status:{$lt:" + status + "}}],"
								+ filters
								+ "}";
			logger.info("Constructed Search Criteria:{}", queryStr);
			Sort sortOrder = new Sort(Direction.DESC, DocumentField.MODIFIEDDT.toString());
			Query query = new BasicQuery(queryStr);
			query.limit(listSize);	
			query.with(sortOrder);
			targetList  = operations.find(query, getDocumentType());
		}
		return targetList;
	}

	@Override
	public List<T> getDocumentIdTitleList(List<String> docIds) {
		Query query = new Query(Criteria.where(DocumentField.ID.getValue()).in(docIds));
		query.fields().include(DocumentField.ID.getValue()).include(DocumentField.TITLE.getValue());
		List<T> targetList = operations.find(query, getDocumentType());
		return targetList;
	}

	@Override
	public T likeDocument(T document) {
		return incrPropertyValue(document, DocumentField.LIKES.getValue());
	}

	@Override
	public T dislikeDocument(T document) {
		return incrPropertyValue(document, DocumentField.DISLIKES.getValue());
	}


	protected T incrPropertyValue(T document, String property){
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.inc(property, 1);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(document.getId()));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

	protected T decrPropertyValue(T document, String property){
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.inc(property, -1);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(document.getId()));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

	protected T decrAndIncPropertyValue(T document, String decProperty, String incrPropery){
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.inc(decProperty, -1);
		updateFields.inc(incrPropery, 1);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(document.getId()));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

	
	@Override
	public T addTag(String docId, String tag, String userId) {
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.addToSet(DocumentField.TAGS.getValue(), tag);
		updateFields.set(DocumentField.MODIFIER.getValue(), userId);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(docId));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

	@Override
	public T removeTag(String docId, String tag, String userId) {
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.pull(DocumentField.TAGS.getValue(), tag);
		updateFields.set(DocumentField.MODIFIER.getValue(), userId);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(docId));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

	@Override
	public T addCollaborator(String docId, String userId, String collabId) {
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.addToSet(DocumentField.COLLABORATORS_MEMBERS.getValue(), collabId);
		updateFields.set(DocumentField.MODIFIER.getValue(), userId);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(docId));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

	@Override
	public T removeCollaborator(String docId, String userId, String collabId) {
		Update updateFields = Update.update(DocumentField.MODIFIEDDT.getValue(), new Date());
		updateFields.pull(DocumentField.COLLABORATORS_MEMBERS.getValue(), collabId);
		updateFields.set(DocumentField.MODIFIER.getValue(), userId);
		Query criteria = new Query(Criteria.where(DocumentField.ID.getValue()).is(docId));
		T  result = operations.findAndModify(
				criteria, 
				updateFields, 
				FindAndModifyOptions.options().returnNew(true),	documentType);
		return result;
	}

}