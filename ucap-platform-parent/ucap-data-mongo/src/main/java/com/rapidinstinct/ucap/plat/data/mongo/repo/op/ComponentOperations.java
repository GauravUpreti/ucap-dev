package com.rapidinstinct.ucap.plat.data.mongo.repo.op;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.Contributor;
import com.rapidinstinct.ucap.plat.data.mongo.collection.tools.AttachmentInfoDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */
@Service
public interface ComponentOperations<T extends BaseDocumentDO> {

	public T lockDocument(T document);

	public T unlockDocument(T document);
	
	public List<T> searchByTitle(String title, String orgId, String userId);

	public List<T> searchByTitle(String title, String filters, String orgId, String userId);
	
	public Class<T> getDocumentType();

	public List<T> getDocumentIdTitleList(List<String> docIds);
	
	public T likeDocument(T document);
	
	public T dislikeDocument(T document);
	
	public T addTag(String docId, String tag, String userId);
	
	public T removeTag(String docId, String tag, String userId);

	public T addCollaborator(String docId, String userId, String collabId);
	
	public T removeCollaborator(String docId, String userId, String collabId);
	
	public ContributorOperations<? extends Contributor> getContribOperator();

	public boolean deleteAttachment(String fid);
	
	public List<AttachmentInfoDO> findComponentAttachments(String orgId, String cid, int cType);

}