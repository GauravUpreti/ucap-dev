package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;

/**
 * 
 * @author Shamim Ahmad
 * 
 */
@Document(collection = "Reports")
@TypeAlias("Reports")
public class MCCReportsComponentDO extends BaseDocumentDO {

	private String moduleName;
	private String moduleId;
	private int activeCount;
	private List<Reports> reports;
	private String bgColorCode;
	private int studyCount;
	private String description;
	private int sid;
	

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public int getActiveCount() {
		return activeCount;
	}

	public void setActiveCount(int activeCount) {
		this.activeCount = activeCount;
	}

	public void setReports(List<Reports> reports) {
		this.reports = reports;
	}

	public List<Reports> getReports() {
		return reports;
	}

	public String getBgColorCode() {
		return bgColorCode;
	}

	public void setBgColorCode(String bgColorCode) {
		this.bgColorCode = bgColorCode;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public int getStudyCount() {
		return studyCount;
	}

	public void setStudyCount(int studyCount) {
		this.studyCount = studyCount;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public static class Reports {
		private String reportName;
		private String status;
		private List<ActiveStudies> activeStudies;
		private String userName;
		private String iframeURL;	
		private String bgColorCode;
		private String description;	

		public Reports() {
			super();
		}	
		
		public String getBgColorCode() {
			return bgColorCode;
		}

		public void setBgColorCode(String bgColorCode) {
			this.bgColorCode = bgColorCode;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getIframeURL() {
			return iframeURL;
		}

		public void setIframeURL(String iframeURL) {
			this.iframeURL = iframeURL;
		}
		public Reports(String reportName, String status, List<ActiveStudies> activeStudies) {
			super();
			this.reportName = reportName;
			this.status = status;
			this.activeStudies = activeStudies;
		}

		public String getReportName() {
			return reportName;
		}

		public void setReportName(String reportName) {
			this.reportName = reportName;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
		
		public List<ActiveStudies> getActiveStudies() {
			return activeStudies;
		}

		public void setActiveStudies(List<ActiveStudies> activeStudies) {
			this.activeStudies = activeStudies;
		}
		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}
		
	}

	public static class ActiveStudies {
	
		private String studyName;
		private String therapeuticArea;
		
		public String getStudyName() {
			return studyName;
		}
		public void setStudyName(String studyName) {
			this.studyName = studyName;
		}
		public String getTherapeuticArea() {
			return therapeuticArea;
		}
		public void setTherapeuticArea(String therapeuticArea) {
			this.therapeuticArea = therapeuticArea;
		}
	}
}