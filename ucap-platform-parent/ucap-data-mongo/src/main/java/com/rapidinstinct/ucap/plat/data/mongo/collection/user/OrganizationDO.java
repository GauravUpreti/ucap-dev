package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "organization")
@TypeAlias("organization")
public class OrganizationDO {

	@Id protected String id;
	private String code;
	protected String creatorId;
	protected String ownerId;
	private String name;
	private String description;
	private String email;
	private String website;
	private Address address;
	private int status;
	private Date createdDT;
	private Date modifiedDT;
	private Set<String> alternateEmailIds = new HashSet<String>();
	private Set<String> phones = new HashSet<String>();

	@Transient private PaymentInfoDO paymentInfo;
	
	public OrganizationDO(){
		this.createdDT = new Date();
		this.modifiedDT = new Date();
		this.status = OrgStatus.CREATED.getValue();
	}
	
	public OrganizationDO(String ownerId, String name){
		this();
		this.ownerId = ownerId;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getWebsite() {
		return website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public String getOwnerId() {
		return ownerId;
	}
	
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public boolean isOwner(String ownerId){
		return StringUtils.equals(this.ownerId, ownerId);
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public void setCreatedDT(Date createdDT) {
		this.createdDT = createdDT;
	}

	public Date getModifiedDT() {
		return modifiedDT;
	}

	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Set<String> getAlternateEmailIds() {
		return alternateEmailIds;
	}

	public void setAlternateEmailIds(Set<String> alternateEmailIds) {
		this.alternateEmailIds = alternateEmailIds;
	}

	public boolean addAlternateEmail(String emailId) {
		return this.alternateEmailIds.add(emailId);
	}

	public boolean addAlternateEmail(List<String> emailId) {
		return this.alternateEmailIds.addAll(emailId);
	}

	public Set<String> getPhones() {
		return phones;
	}

	public void setPhones(Set<String> phones) {
		this.phones = phones;
	}

	public boolean addPhone(String phone) {
		return this.phones.add(phone);
	}

	public boolean addPhone(List<String> phones) {
		return this.phones.addAll(phones);
	}
	
	public PaymentInfoDO getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfoDO paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public static enum OrgStatus {
		CREATED(1), VERIFICATION_PENDING(10), ACTIVE(20), INACTIVE(30);

		private OrgStatus(final int text) {
			this.text = text;
		}

		private final int text;
		public int getValue() {
			return text;
		}
	}
}
