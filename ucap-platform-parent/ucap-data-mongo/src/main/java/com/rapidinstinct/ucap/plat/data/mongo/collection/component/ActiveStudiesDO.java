package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import java.util.List;

/**
 *
 * @author Shamim Ahmad
 *
 */
public class ActiveStudiesDO{
	
	private List<Object> activeStudies;
	
	public ActiveStudiesDO() {
		super();
	}

	public ActiveStudiesDO(List<Object> activeStudies) {
		super();
		this.activeStudies = activeStudies;
	}

	public List<Object> getActiveStudies() {
		return activeStudies;
	}

	public void setActiveStudies(List<Object> activeStudies) {
		this.activeStudies = activeStudies;
	}

	
}