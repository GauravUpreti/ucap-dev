package com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgCompanyDO;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public interface OrgCompanyOperations {

	public OrgCompanyDO add(String orgId, String name);

	public OrgCompanyDO add(String orgId, String name, String creator);

	public List<OrgCompanyDO> getCompanies(String orgId);

	public List<OrgCompanyDO> searchCompanies(String term, String orgId);

	public OrgCompanyDO findCompany(String name, String orgId);

}
