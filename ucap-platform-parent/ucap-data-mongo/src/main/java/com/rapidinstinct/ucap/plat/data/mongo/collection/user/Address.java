package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import com.mysema.query.annotations.QueryEmbeddable;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@QueryEmbeddable
public class Address {
	
	private String line1;
	private String line2;
	private String line3;
	private String city;
	private String state;
	private String zip;
	private String country;

	public String getLine1() {
		return line1;
	}
	
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	
	public String getLine2() {
		return line2;
	}
	
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	
	public String getLine3() {
		return line3;
	}
	
	public void setLine3(String line3) {
		this.line3 = line3;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
}
