package com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgSkillDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.resource.OrgSkillRepository;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public class OrgSkillOperationsImpl implements OrgSkillOperations {

	@Autowired 	protected OrgSkillRepository repo;
	@Autowired 	protected MongoOperations operations;

	@Value("${component.list.size.limit}")
	private int listSize;
	
	@Override
	public OrgSkillDO add(String orgId, String name) {
		OrgSkillDO newDoc = new OrgSkillDO(orgId, name);
		return repo.save(newDoc);
	}

	@Override
	public OrgSkillDO add(String orgId, String name, String creator) {
		OrgSkillDO newDoc = new OrgSkillDO(orgId, name);
		newDoc.setCreator(creator);
		return repo.save(newDoc);
	}

	@Override
	public List<OrgSkillDO> getSkills(String orgId) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.limit(listSize);	
		return operations.find(query, OrgSkillDO.class);
	}

	@Override
	public List<OrgSkillDO> searchSkills(String term, String orgId) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.NAME.getValue()).regex(term));
		query.limit(listSize);	
		return operations.find(query, OrgSkillDO.class);

	}

	@Override
	public OrgSkillDO findSkill(String name, String orgId) {
		OrgSkillDO result = null;
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.NAME.getValue()).is(name));
		query.limit(1);	
		List<OrgSkillDO> resList = operations.find(query, OrgSkillDO.class);
		if(resList.size()==1){
			result = resList.get(0);
		}
		return result;
	}

}
