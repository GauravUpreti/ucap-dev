package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import org.springframework.stereotype.Component;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Component
public interface FileInfoMapper {

	public FileInfo mapToFileInfo(Object source);

}
