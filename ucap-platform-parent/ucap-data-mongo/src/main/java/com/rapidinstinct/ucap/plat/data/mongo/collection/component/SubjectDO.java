package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Gaurav Upreti
 *
 */

@Document(collection="SubjectViews")
@TypeAlias ("SubjectViews")
public class SubjectDO  {
 
	private String studyOID;
	private String LocationOID;
	private String SubjectStatus;
	private String Gender;
	private String _id;
	private String SubjectName;
	private int Age;
	private String siteName;
	
	public String getStudyOID() {
		return studyOID;
	}
	public void setStudyOID(String studyOID) {
		this.studyOID = studyOID;
	}	
	public String getLocationOID() {
		return LocationOID;
	}
	public void setLocationOID(String locationOID) {
		LocationOID = locationOID;
	}
	public String getSubjectStatus() {
		return SubjectStatus;
	}
	public void setSubjectStatus(String subjectStatus) {
		SubjectStatus = subjectStatus;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getSubjectName() {
		return SubjectName;
	}
	public void setSubjectName(String subjectName) {
		SubjectName = subjectName;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		Age = age;
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
}
