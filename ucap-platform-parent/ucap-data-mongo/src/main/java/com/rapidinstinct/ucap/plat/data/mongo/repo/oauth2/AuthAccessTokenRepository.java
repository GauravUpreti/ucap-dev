package com.rapidinstinct.ucap.plat.data.mongo.repo.oauth2;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.oauth2.AuthAccessTokenDO;

/**
 * @author Gaurav Upreti
 * 
 */

@Repository
public interface AuthAccessTokenRepository extends MongoRepository<AuthAccessTokenDO, String> {

	public AuthAccessTokenDO findByTokenValue(String tokenValue);

	public AuthAccessTokenDO findByRefreshToken(String refreshToken);

	public AuthAccessTokenDO findByAuthenticationId(String authenticationId);

	public List<AuthAccessTokenDO> findByClientIdAndUserName(String clientId, String userName);

	public List<AuthAccessTokenDO> findByClientId(String clientId);

}
