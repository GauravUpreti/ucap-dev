package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import org.apache.commons.lang3.StringUtils;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO.OrgStatus;



/**
 * 
 * @author Gaurav Upreti
 *
 */

public class OrganizationDOHelper {
	
	public static final String SANDBOX_ORG = "Sandbox";
	public static final String SANDBOX_ORG_CODE = "SB";
	
	public static OrganizationDO createNew(String ownerId, String name){
		OrganizationDO orgDO = new OrganizationDO(ownerId, name);
		orgDO.setCode(name);
		return orgDO;
	}
	
	public static OrganizationDO createSandboxOrg(String ownerId, String email){
		String orgName = StringUtils.split(email, "@")[0] + "'s " + SANDBOX_ORG;
		OrganizationDO orgDO = new OrganizationDO(ownerId, orgName);
		orgDO.setCode(SANDBOX_ORG_CODE);
		orgDO.setStatus(OrgStatus.ACTIVE.getValue());
		orgDO.setEmail(email);
		return orgDO;
	}
}
