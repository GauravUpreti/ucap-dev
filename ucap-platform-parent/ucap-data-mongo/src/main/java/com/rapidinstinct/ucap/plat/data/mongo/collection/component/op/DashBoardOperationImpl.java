package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudiesDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.SubjectDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.RegisterRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.StudiesRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.StudyRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.SubjectRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.UserAccessRepository;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public class DashBoardOperationImpl implements DashBoardOperations {

	@Autowired private SubjectRepository repo;
	@Autowired private StudiesRepository studyRepo;
	@Autowired private StudyRepository sRepo;
	@Autowired private UserAccessRepository uRepo;
	@Autowired private RegisterRepository rRepo;
	
	@Override
	public List<SubjectDO> read() {
		return repo.findAll();
	}
	
	@Override
	public List<StudiesDO> readStudies() {
		return studyRepo.findAll();
	}
			
	@Override
	public List<StudyDO> readStudy() {
		return sRepo.findAll();
	}

	@Override
	public UserAccessDO findByUserId(String userId) {
		return uRepo.findByUserId(userId);
	}

	@Override
	public List<StudyDO> findByStudyId(String studyName) {
		return sRepo.findStudy(studyName);
	}


}
