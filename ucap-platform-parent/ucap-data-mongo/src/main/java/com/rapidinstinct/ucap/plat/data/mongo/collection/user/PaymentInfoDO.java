package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.data.mongo.collection.CommonProperties;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "paymentInfo")
@TypeAlias("paymentInfo")
public class PaymentInfoDO extends CommonProperties{

	public static final String MASK_PREFIX = "************";
	
	private String creator;
	protected String modifier;
	private int cardType;
	private int paymentMode;
	private String cardNumber; //TODO decide if one way hash encryption is needed?
	private String cardVisibleDigits; //visible card last digits - usually 4
	private String holderName;
	private String expiryDate; //TODO decide data format.. Format is same or varies based on the card type 
	private Address billAddress;
	private String mobileNumber;
	
	public PaymentInfoDO(){
		super();
	}
	
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public int getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(int paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Address getBillAddress() {
		return billAddress;
	}

	public void setBillAddress(Address billAddress) {
		this.billAddress = billAddress;
	}

	public String getCardVisibleDigits() {
		return cardVisibleDigits;
	}

	public void setCardVisibleDigits(String cardVisibleDigits) {
		this.cardVisibleDigits = cardVisibleDigits;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public static enum CardType {
		VISA(1), MASTER(2), AMERICAN_EXPRESS(3);
		private CardType(final int type) {
			this.type = type;
		}
		private final int type;
		public int getValue() {
			return type;
		}
	}

	public static enum PaymentMode {
		CREDIT_CARD(1), DEBIT_CARD(2), BANK_ACCOUNT(3);
		private PaymentMode(final int type) {
			this.type = type;
		}
		private final int type;
		public int getValue() {
			return type;
		}
	}
}
