package com.rapidinstinct.ucap.plat.data.mongo.collection.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "groupResPermissions")
@TypeAlias("groupResPermissions")
public class GroupResourcesPermissionsDO {
	
	@Id private String id;
	private String code;
	private String titile;
	private String description;	
	private List<String> moduleIds;
	private List<ResourcePermissionsDO> rpermissions = new ArrayList<ResourcePermissionsDO>();
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getTitile() {
		return titile;
	}
	
	public void setTitile(String titile) {
		this.titile = titile;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getModuleIds() {
		return moduleIds;
	}

	public void setModuleIds(List<String> modules) {
		this.moduleIds = modules;
	}

	public List<ResourcePermissionsDO> getRpermissions() {
		return rpermissions;
	}

	public void setRpermissions(List<ResourcePermissionsDO> ractions) {
		this.rpermissions = ractions;
	}


	public static class ResourcePermissionsDO {
		private String resourceId;
		private int permissions;

		public String getResourceId() {
			return resourceId;
		}
		
		public void setResourceId(String id) {
			this.resourceId = id;
		}
		
		public int getPermissions() {
			return permissions;
		}
		
		public void setPermissions(int actions) {
			this.permissions = actions;
		}
	}
}
