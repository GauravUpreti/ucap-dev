package com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgInstituteDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.resource.OrgInstituteRepository;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public class OrgInstituteOperationsImpl implements OrgInstituteOperations {

	@Autowired 	protected OrgInstituteRepository repo;
	@Autowired 	protected MongoOperations operations;

	@Value("${component.list.size.limit}")
	private int listSize;

	@Override
	public OrgInstituteDO add(String orgId, String name) {
		OrgInstituteDO newDoc = new OrgInstituteDO(orgId, name);
		return repo.save(newDoc);
	}

	@Override
	public OrgInstituteDO add(String orgId, String name, String creator) {
		OrgInstituteDO newDoc = new OrgInstituteDO(orgId, name);
		newDoc.setCreator(creator);
		return repo.save(newDoc);
	}

	@Override
	public List<OrgInstituteDO> getInstitutes(String orgId) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.limit(listSize);	
		return operations.find(query, OrgInstituteDO.class);
	}

	@Override
	public List<OrgInstituteDO> searchInstitutes(String term, String orgId) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.NAME.getValue()).regex(term));
		query.limit(listSize);	
		return operations.find(query, OrgInstituteDO.class);
	}

	@Override
	public OrgInstituteDO findInstitute(String name, String orgId) {
		OrgInstituteDO result = null;
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.NAME.getValue()).is(name));
		query.limit(1);	
		List<OrgInstituteDO> resList = operations.find(query, OrgInstituteDO.class);
		if(resList.size()==1){
			result = resList.get(0);
		}
		return result;
	}

}
