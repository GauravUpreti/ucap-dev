package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.RBMComponentDO;

/**
 * 
 * @author Shamim Ahmad
 * 
 */
@Repository
public interface RBMRepository extends MongoRepository<RBMComponentDO, String> {

	@Query("{'studyName' :'?0'}")
	public List<RBMComponentDO> findByStudy(String studyName);

	@Query("{grpKRI:?0, kri:?1}}")
	public RBMComponentDO findbygrpKRIandkri(String grpKRI, String kri);

	@Query(value="{'studyName':?0}",fields="{'kri':1}")
	public List<RBMComponentDO> findKriByStudyName(String studyName);
	
}