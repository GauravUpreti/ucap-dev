package com.rapidinstinct.ucap.plat.data.mongo.collection;

public class DocumentConstants {
	//TODO externalize constant value	
	public static final int SEARCH_LIMIT = 10;
	public static final int VERSION_ONE = 1;
	public static final String TITLE_VAL_SEPARATOR = ", ";

	
	public static enum DocumentStatus{
		UNPUBLISHED_AUTHORED(10), //visible to only knowledge ADMIN,
		UNPUBLISHED_SUBMITTED(20), //visible to knowledge ADMIN and MANAGER,
		UNPUBLISHED_REJECTED(30), //review failed
		UNPUBLISHED_APPROVED(40), //reviewer approved
		UNPUBLISHED_DELETED(50), //author deleted, HARD delete
		PUBLISHED_ACTIVATED(60), //published and active can be used in referentiable documents,
		PUBLISHED_INACTIVATED(70), //published and in-active (hidden) for temporary time can be used in existing referentiable documents.
		PUBLISHED_DEPRECATED(80), //future reference is not allowed but existing references are valid 
		PUBLISHED_ARCHIVED(90), //archived and can't be accessed back
		PUBLISHED_DELETED(100), //HARD delete and can't be accessed back
		;

		private DocumentStatus(final int text) {
			this.status = text;
		}
		
		private final int status;
		
		public int getValue() {
			return status;
		}
		
		public DocumentStatus getType(int value){
			DocumentStatus types [] = DocumentStatus.values();
			for(DocumentStatus type:types){
				if(type.getValue() == value){
					return type;
				}
			}
			return null;
		}
	}

	public static enum ConstraintType{
		DEPENDENCY(1);

		private ConstraintType(final int text) {
			this.text = text;
		}
		
		private final int text;
		
		public int getValue() {
			return text;
		}

		public ConstraintType getType(int value){
			ConstraintType types [] = ConstraintType.values();
			for(ConstraintType type:types){
				if(type.getValue() == value){
					return type;
				}
			}
			return null;
		}
		
	}
	
	public static enum DocumentField {
		ID("_id"), UNLOCKED("unlocked"), VERSION("version"), UNLOCKER("unlocker"), 
		TEXT("text"), SEARCH("search"), LIMIT("limit"), RESULTS("results"), 
		OBJECT("obj"), TITLE("title"), ORGID("orgId"), STATUS("status"), CREATOR("creator"),
		LIKES("likes"),  DISLIKES("dislikes"), MODIFIER("modifier"), CREATEDDT("createdDT"), MODIFIEDDT("modifiedDT"),FIRSTNAME("firstName"),
		HITS("hits"), TAG("tag"), TAGS("tags"), SOURCEID("sourceId"), NAME("name"), COLLABORATORS_MEMBERS("collaborators.members"), TYPE("type"),
		USERID("userId"), DISPLAYNAME("displayName"),CONCEPTS("concepts"), INITIATES("initiates"), PLANS("plans"), EXECUTIONS("executions"),
		CLOSURES("closures"), OBJECTID("objectId"), OBJECT_TYPE("objectType"), STAGE("stage"), 
		FAVORITE_DOCS("favoriteDocs"), LIKED_DOCS("likedDocs"), DISLIKED_DOCS("dislikedDocs"), 
		POOL_ID("poolId"), RESOURCE_ID("resourceId"), ROLES("roles"), ORG_GROUPS("orgGroups"), GRP_IDS("grpIds"),
		LASTNAME("lastName"), INFORMATIONS("informations"), METADATA("metadata"), QUESTIONS("questions"), PLANID("planId"),
		PORTFOLIO_ID("portfolioId"), WBS("wbs")
		;

		private DocumentField(final String text) {
			this.text = text;
		}

		private final String text;

		public String getValue() {
			return text;
		}
	}



	public static enum DocumentType {
		QUESTION(1), 
		FORM(2), 
		ELEMENT(3), 
		DISCIPLINE(4), 
		RESPONSIBILITY(5), 
		PLAN(6), 
		PROJECT_TYPE(7), 
		SKILL_PROFILE(8),
		PORTFOLIO(9),
		POOL(10),
		PROJECT(11),
		TODO(12),
		PROJECT_FORM(13),
		PROJECT_PLAN(14),
		PROJECT_PLAN_WBS(15),
		PROJECT_PLAN_WBS_TASK(16),
		;

		private DocumentType(final int text) {
			this.text = text;
		}

		private final int text;

		public int getValue() {
			return text;
		}
		
		public DocumentType getType(int value){
			DocumentType types [] = DocumentType.values();
			for(DocumentType type:types){
				if(type.getValue() == value){
					return type;
				}
			}
			return null;
		}
	}

	public static enum VoteType {
		LIKE(1), DISLIKE(2);

		private VoteType(final int type) {
			this.type = type;
		}
		
		private final int type;
		
		public int getValue() {
			return type;
		}
		
		public static VoteType getType(int value){
			VoteType types [] = VoteType.values();
			for(VoteType type:types){
				if(type.getValue() == value){
					return type;
				}
			}
			return null;
		}
	}
	
	public static enum ContactType {
		MOBILE("mobile"), PHONE("phone");

		private ContactType(final String text) {
			this.text = text;
		}
		
		private final String text;
		
		public String getValue() {
			return text;
		}
	}

}
