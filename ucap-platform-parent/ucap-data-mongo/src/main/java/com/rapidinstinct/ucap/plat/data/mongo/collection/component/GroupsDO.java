package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="Groups")
@TypeAlias ("Groups")
public class GroupsDO  {
    
	@Id private int id;
	private List<String> kri;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGrpKRI() {
		return grpKRI;
	}
	public void setGrpKRI(String grpKRI) {
		this.grpKRI = grpKRI;
	}
	public List<String> getKri() {
		return kri;
	}
	public void setKri(List<String> kri) {
		this.kri = kri;
	}
	private String grpKRI;
}
