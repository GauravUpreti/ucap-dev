package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import org.springframework.stereotype.Service;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Service
public interface FileOperations {

	public FileInfo retriveFileByTherapeuticArea(String therapeuticArea);

	public FileInfo retriveByUserId(String userId);
	
	public FileInfo retriveByModuleId(String moduleId);
	
}
