package com.rapidinstinct.ucap.plat.data.mongo.collection.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Gaurav Upreti
 *
 */

@Document(collection = "peopleSkillProfile")
@TypeAlias("peopleSkillProfile")
public class PeopleSkillProfileDO {

	@Id private String userId;
	private String summary;
	private List<Experience> experience = new ArrayList<Experience>();
	private List<Education> education = new ArrayList<Education>();
	private Set<Skill> skills = new HashSet<Skill>();
	private Set<String> skillProfiles = new HashSet<String>();
	private Set<String> profileUrls = new HashSet<String>();
	private Date createdDT;
	private Date modifiedDT;
	
	//transient fields required while adding new user
	@Transient private String firstName;
	@Transient private String lastName;
	@Transient private String email;
	
	public PeopleSkillProfileDO() { 
		this.createdDT = new Date();
		this.modifiedDT = new Date();
	} 
	
	public PeopleSkillProfileDO(String userId){
		this();
		this.userId = userId;
	}
	
	public String getSummary() {
		return summary;
	}

	public List<Experience> getExperience() {
		return experience;
	}

	public void setExperience(List<Experience> experience) {
		this.experience = experience;
	}

	public Set<Skill> getSkills() {
		return skills;
	}

	public void setSkills(Set<Skill> skills) {
		this.skills = skills;
	}

	public Set<String> getSkillProfiles() {
		return skillProfiles;
	}

	public void setSkillProfiles(Set<String> skillProfiles) {
		this.skillProfiles = skillProfiles;
	}

	public void addExperience(Experience experience){
		if(experience!=null){
			this.experience.add(experience);	
		}
	}

	public List<Education> getEducation() {
		return education;
	}

	public void setEducation(List<Education> education) {
		this.education = education;
	}

	public void addEducation(Education education){
		if(education!=null){
			this.education.add(education);	
		}
	}

	public void addSkill(Skill skill){
		if(skill!=null && StringUtils.isNotBlank(skill.getName())){
			this.skills.add(skill);	
		}
	}
	
	public void addSkillProfile(String skillProfileId){
		if(StringUtils.isNotBlank(skillProfileId)){
			this.skillProfiles.add(skillProfileId);	
		}
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getModifiedDT() {
		return modifiedDT;
	}

	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getProfileUrls() {
		return profileUrls;
	}

	public void setProfileUrls(Set<String> profileUrls) {
		this.profileUrls = profileUrls;
	}

	public void addProfileUrl(String profileUrl){
		if(StringUtils.isNotBlank(profileUrl)){
			this.profileUrls.add(profileUrl);	
		}
	}

	public static class Experience {
		private String orgName;
		private String title;
		private String fromDate;
		private String toDate;
		private String summary;

		public Experience(){ }
		
		public Experience(String orgName, String title){
			this.orgName = orgName;
			this.title = title;
		}
		
		public String getOrgName() {
			return orgName;
		}
		
		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getFromDate() {
			return fromDate;
		}
		
		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}
		
		public String getToDate() {
			return toDate;
		}
		
		public void setToDate(String toDate) {
			this.toDate = toDate;
		}
		
		public String getSummary() {
			return summary;
		}
		
		public void setSummary(String summary) {
			this.summary = summary;
		}
	}
	
	public static class Education {
		private String college;
		private String title;
		private String fromDate;
		private String toDate;
		private String summary;

		public Education(){ }
		
		public Education(String college, String title){
			this.college = college;
			this.title = title;
		}

		public String getCollege() {
			return college;
		}

		public void setCollege(String college) {
			this.college = college;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getFromDate() {
			return fromDate;
		}
		
		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}
		
		public String getToDate() {
			return toDate;
		}
		
		public void setToDate(String toDate) {
			this.toDate = toDate;
		}
		
		public String getSummary() {
			return summary;
		}
		
		public void setSummary(String summary) {
			this.summary = summary;
		}
	}

	public static class Skill {
		private String name;
		private int selfRating;
		
		public Skill(){ }
		
		public Skill(String name, int selfRating){ 
			this.name = name;
			this.selfRating = selfRating;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getSelfRating() {
			return selfRating;
		}

		public void setSelfRating(int selfRating) {
			this.selfRating = selfRating;
		}
	}
}
