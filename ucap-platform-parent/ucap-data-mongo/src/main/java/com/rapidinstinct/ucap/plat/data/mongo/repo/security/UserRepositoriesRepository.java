package com.rapidinstinct.ucap.plat.data.mongo.repo.security;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserRepositoriesDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Repository
public interface UserRepositoriesRepository extends MongoRepository<UserRepositoriesDO, String>{

}
