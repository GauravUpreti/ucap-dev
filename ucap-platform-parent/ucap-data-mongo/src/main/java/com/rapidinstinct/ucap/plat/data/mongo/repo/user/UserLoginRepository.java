package com.rapidinstinct.ucap.plat.data.mongo.repo.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Repository
public interface UserLoginRepository extends MongoRepository<UserLoginDO, String> {

	UserLoginDO findByLocalEmailIdsEmailId(String emailId);
	
	UserLoginDO findByLinkedinSocialId(String socialId);

	UserLoginDO findByGoogleSocialId(String socialId);
	
	UserLoginDO findByFacebookSocialId(String socialId);
}
