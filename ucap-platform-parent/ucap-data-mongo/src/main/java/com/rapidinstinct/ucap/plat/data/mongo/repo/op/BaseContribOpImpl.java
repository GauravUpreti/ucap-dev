package com.rapidinstinct.ucap.plat.data.mongo.repo.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.Contributor;

/**
 * @author Gaurav Upreti
 *
 */
public abstract class BaseContribOpImpl<T extends Contributor> implements ContributorOperations<T>{
	protected Class<T> documentType;
	@Autowired 	protected MongoOperations operations;
	@Value("${component.list.size.limit}") private int listSize;

	@Override
	public List<T> getContributors(String orgId, int type) {
		Sort sortOrder = new Sort(Direction.ASC, DocumentField.DISPLAYNAME.getValue());
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.TYPE.getValue()).is(type));
		query.fields().include(DocumentField.USERID.getValue()).include(DocumentField.DISPLAYNAME.getValue());
		query.with(sortOrder);
		query.limit(listSize);
		return operations.find(query, getDocumentType());
	}

	@Override
	public T addContributor(String orgId, String userId, String displayName, int type) {
		T cdo = findContributor(orgId, userId, type);
		if(cdo == null){
			cdo = createContributor(orgId, userId);
			cdo.setType(type);
			cdo.setDisplayName(displayName);
			cdo = getRepository().save(cdo);
		}
		return cdo;
	}

	@Override
	public List<T> searchContributors(String orgId, String term, int type) {
		Sort sortOrder = new Sort(Direction.ASC, DocumentField.DISPLAYNAME.getValue());
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.DISPLAYNAME.getValue()).regex(term, "i"));
		query.fields().include(DocumentField.USERID.getValue()).include(DocumentField.DISPLAYNAME.getValue());
		query.limit(listSize);	
		query.with(sortOrder);
		return operations.find(query, getDocumentType());
	}

	@Override
	public T findContributor(String orgId, String userId, int type) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.USERID.getValue()).is(userId));
		query.addCriteria(Criteria.where(DocumentField.TYPE.getValue()).is(type));
		query.fields().include(DocumentField.USERID.getValue()).include(DocumentField.DISPLAYNAME.getValue());
		return operations.findOne(query, getDocumentType());
	}
}