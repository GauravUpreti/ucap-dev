package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import org.springframework.stereotype.Component;

import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSFile;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Component
public class MongoFileInfoMapper implements FileInfoMapper {

	private static String[] VALID_FIELDS = {"_id" , "filename" , "contentType" , "length" , "uploadDate" , "aliases" , "chunkSize" , "md5"};
	
	@Override
	public FileInfo mapToFileInfo(Object source) {
		FileInfo target = null;
		if(source != null){
			if(source instanceof GridFSFile){
				GridFSFile internalObj = (GridFSFile) source;
				target = new MongoFileInfoImpl(internalObj);
				target.setId(internalObj.getId());
				target.setFileName(internalObj.getFilename());
				target.setContentType(internalObj.getContentType());
				target.setContentLength(internalObj.getLength());
				target.setCreateDT(internalObj.getUploadDate());
				if(internalObj.getAliases()!=null){
					for(String alias:internalObj.getAliases()){
						target.getAliases().add(alias);	
					}
				}
				if(internalObj.getMetaData()!=null){
					DBObject dbObj = internalObj.getMetaData();
					target.setOrgId( (String) dbObj.get(FileInfo.ExtraFields.ORGID.toString()));
					target.setCreator( (String) dbObj.get(FileInfo.ExtraFields.CREATOR.toString()));
					target.setReferrerId( (String) dbObj.get(FileInfo.ExtraFields.REFERRER_ID.toString()));
					target.setContentType((String) dbObj.get(VALID_FIELDS[2]));
				}
			}
		}
		return target;
	}
}
