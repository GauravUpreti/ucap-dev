package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Document(collection="Thresholds")
@TypeAlias("Thresholds")
public class RBMComponentDO extends BaseDocumentDO {

	private String grpKRI;
	private String kri;
	private String kRICode;	
	private Float sModRisk;
	private Float sSevRisk;
	private Float sLB;
	private Float uModRisk;
	private Float uSevRisk;
	private Float uLB;
	private Float wgtORI;
	private String studyName;
	
	public String getGrpKRI() {
		return grpKRI;
	}
	public void setGrpKRI(String grpKRI) {
		this.grpKRI = grpKRI;
	}
	public String getKri() {
		return kri;
	}
	public void setKri(String kri) {
		this.kri = kri;
	}
	public String getkRICode() {
		return kRICode;
	}
	public void setkRICode(String kRICode) {
		this.kRICode = kRICode;
	}
	public Float getsModRisk() {
		return sModRisk;
	}
	public void setsModRisk(Float sModRisk) {
		this.sModRisk = sModRisk;
	}
	public Float getsSevRisk() {
		return sSevRisk;
	}
	public void setsSevRisk(Float sSevRisk) {
		this.sSevRisk = sSevRisk;
	}
	public Float getsLB() {
		return sLB;
	}
	public void setsLB(Float sLB) {
		this.sLB = sLB;
	}
	public Float getuModRisk() {
		return uModRisk;
	}
	public void setuModRisk(Float uModRisk) {
		this.uModRisk = uModRisk;
	}
	public Float getuSevRisk() {
		return uSevRisk;
	}
	public void setuSevRisk(Float uSevRisk) {
		this.uSevRisk = uSevRisk;
	}
	public Float getuLB() {
		return uLB;
	}
	public void setuLB(Float uLB) {
		this.uLB = uLB;
	}
	public Float getWgtORI() {
		return wgtORI;
	}
	public void setWgtORI(Float wgtORI) {
		this.wgtORI = wgtORI;
	}
	public String getStudyName() {
		return studyName;
	}
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}
}