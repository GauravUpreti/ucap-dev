package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.commons.util.DateHelper;


/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "verification")
@TypeAlias("verification")
public class VerificationDO {
	public static final short TOKEN_EXPIRY_DAYS = 30;
	@Id private String id;
	private String createdFor;
	private int context;
	private String contextRefId;
	private String token;
	private Date createdDT;
	private int status;
	private Map<String, Object> contextData;
	
	public VerificationDO() { 
		this.contextData = new HashMap<String, Object>();
		this.createdDT = new Date();
		this.status = Status.PENDING.getValue();
	}
	
	public VerificationDO(String userId, String token, int context, String contextRefId){
		this();
		this.createdFor = userId;
		this.token = token;
		this.context = context;
		this.contextRefId = contextRefId;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCreatedFor() {
		return createdFor;
	}
	
	public void setCreatedFor(String createdFor) {
		this.createdFor = createdFor;
	}
	
	public int getContext() {
		return context;
	}
	
	public void setContext(int context) {
		this.context = context;
	}
	
	public String getContextRefId() {
		return contextRefId;
	}
	
	public void setContextRefId(String contextRefId) {
		this.contextRefId = contextRefId;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String verificationURL) {
		this.token = verificationURL;
	}
	
	public Date getCreatedDT() {
		return createdDT;
	}
	
	public void setCreatedDT(Date createdDT) {
		this.createdDT = createdDT;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Map<String, Object> getContextData() {
		return contextData;
	}

	public void setContextData(Map<String, Object> contextData) {
		this.contextData = contextData;
	}

	public void addContextData(String key, Object value) {
		this.contextData.put(key, value);
	}
	
	public void removeContextData(String key) {
		this.contextData.remove(key);
	}

	public Object getContextData(String key) {
		return this.contextData.get(key);
	}

	public String getStringContextData(String key) {
		Object value = this.getContextData(key);
		if(value instanceof String){
			return (String) value;	
		}
		
		return StringUtils.EMPTY;
	}

	public boolean isValidToken(){
		Date expiryDate = DateHelper.addDays(this.getCreatedDT(), TOKEN_EXPIRY_DAYS);		
		Date currentDate = DateHelper.getTodayDate(DateHelper.FORMAT_MMDDYYYY);
		return expiryDate.after(currentDate);
	}

	public static enum ContextDataKey {
		email, orgId, grp, memReqId, orgUserId
	}
	
	public static enum Context {
		USER_REGISTRATION(1), ORG_REGISTRATION(2), ORG_MEMBERSHIP_REQUEST(3), REGISTRATION_INVITE(4), ADD_EMAIL(5), INVITED_USER_ACTIVATE(6);

		private Context(final int context) { this.context = context; }

		private final int context;

		public int getValue() { return context;}
		
	}

	public static enum Status {
		PENDING(10), VERIFIED(20) ;

		private Status(final int context) {
			this.context = context;
		}

		private final int context;

		public int getValue() {
			return context;
		}
	}
}
