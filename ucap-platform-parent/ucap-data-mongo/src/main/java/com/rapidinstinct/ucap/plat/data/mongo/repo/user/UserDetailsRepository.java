package com.rapidinstinct.ucap.plat.data.mongo.repo.user;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Repository
public interface UserDetailsRepository extends MongoRepository<UserDetailsDO, String> {

	List<UserDetailsDO> findByOrgIds(String orgId,  Pageable page);	

	UserDetailsDO findByUserIdAndOrgIds(String userId, String orgId);	
}
