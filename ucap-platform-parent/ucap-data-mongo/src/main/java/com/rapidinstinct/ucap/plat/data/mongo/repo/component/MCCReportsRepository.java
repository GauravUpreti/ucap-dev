package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.MCCReportsComponentDO;

/**
 * 
 * @author Shamim Ahmad
 * 
 */
@Repository
public interface MCCReportsRepository extends MongoRepository<MCCReportsComponentDO, String> {

	@Query("{'moduleId':?0}")
	public MCCReportsComponentDO findByModuleId(String moduleId);
	
	@Query("{'moduleId':?0}")
	public MCCReportsComponentDO findByModule(String moduleId,
			String userId);
	
	
	
	
	
	//@Query("{'$or':[{parentId:?2, owner:?0, orgId:?1},{parentId:?2, orgId:?1, membersDetails:{'$elemMatch':{memberId:?0}}}]}")
	//db.Reports.find({'$and':[{moduleId:"SUBJECT-ENROLLMENT"},{reports:{$elemMatch:{user:{$elemMatch:{userName:'sbhat'}}}}}]})
	//@Query("{'$and':[{moduleId:?0},{reports:{'$elemMatch':{user:{'$elemMatch':{userName:?1}}}}}]}")
	
	//@Query(value="{'$and':[{moduleId:?0},{reports:{$elemMatch:{user:{$elemMatch:{userName:?1}}}}}]}",fields="{moduleId:1}")
	//public List<MCCReportsComponentDO> findByModuleId(String moduleId, String userId);
}