package com.rapidinstinct.ucap.plat.data.mongo.repo.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UnlockedDO;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Repository
public interface UnlockedDocumentsRepository extends MongoRepository<UnlockedDO, String>{

	UnlockedDO findByUserIdAndDocType(String userId, int docType);
}
