package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseUserDocument;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Document(collection = "unlockedDocs")
@TypeAlias("unlockedDocs")
public class UnlockedDO extends BaseUserDocument {

	private int docType;
	private List<String> unlockedDocIds = new ArrayList<String>();

	public UnlockedDO(String userId, String orgId){
		super(userId, orgId);
	}
	
	public int getDocType() {
		return docType;
	}
	
	public void setDocType(int docType) {
		this.docType = docType;
	}
	
	public List<String> getUnlockedDocIds() {
		return unlockedDocIds;
	}
	
	public void setUnlockedDocIds(List<String> documentIds) {
		this.unlockedDocIds = documentIds;
	}
	
	public boolean add(String docId){
		boolean result = false;
		if(!unlockedDocIds.contains(docId)){
			unlockedDocIds.add(docId);
			result = true;
		}
		return result;
	}
	
	public boolean remove(String docId){
		return unlockedDocIds.remove(docId);
	}

}
