package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.NotificationsDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.component.NotificationsRepository;

/**
*
* @author Gaurav Upreti
*
*/

@Component
public class NotificationOperationsImpl implements NotificationOperations{

	@Autowired private NotificationsRepository notificationsRepo;
	
	@Override
	public List<NotificationsDO> findNotificationsbyStudyName(String studyName) {
		return notificationsRepo.findbystudyName(studyName);
	}

	@Override
	public List<NotificationsDO> findAllNotifications() {
		return notificationsRepo.findAll();
	}

	@Override
	public void updateNotification(NotificationsDO notificationDO) {
		notificationsRepo.save(notificationDO);
	}

	@Override
	public NotificationsDO findByNotificationId(String notificationID) {
		return notificationsRepo.findById(notificationID);
	}

	@Override
	public List<NotificationsDO> findPendingNotificationsbyStudyName(String study, int status) {
		return notificationsRepo.findByStudyAndStatus(study, status);
	}

}
