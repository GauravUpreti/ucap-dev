package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseUserDocument;

/**
 * @author Gaurav Upreti
 *
 */
public abstract class Contributor extends BaseUserDocument{

	protected int type;
	
	public Contributor(){
		super();
	}
	
	public Contributor(String userId, String orgId){
		super(userId, orgId);
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public static enum ContributorType {
		OWNER(1), COLLABORATOR(2);

		private ContributorType(final int text) {
			this.status = text;
		}
		
		private final int status;
		
		public int getValue() {
			return status;
		}
	}
}
