package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserGroupsDOHelper {
	
	public static UserGroupsDO createNew(String userId, String orgId, String grpId){
		UserGroupsDO userGroup = new UserGroupsDO(userId);
		OrganizationGroupsDO orgGroups = new OrganizationGroupsDO();
		orgGroups.setOrgId(orgId);
		orgGroups.addGroupId(grpId);
		userGroup.addOrganizationGroups(orgGroups);
		return userGroup;
	}

	public static UserGroupsDO createNewOrgGroup(UserGroupsDO userGroup,String orgId, String grpId){
		OrganizationGroupsDO orgGroups = new OrganizationGroupsDO();
		orgGroups.setOrgId(orgId);
		userGroup.addOrganizationGroups(orgGroups);
		return userGroup;
	}
}
