package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.Date;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginSource;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginStatus;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginLocalDO.LoginEmail;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserLoginDOHelper {

	public static UserLoginDO createNewLocalLogin(String emailId, String password){
		UserLoginDO userLogin = new UserLoginDO(LoginSource.LOCAL.getValue());
		LoginEmail loginEmail = new LoginEmail(emailId, LoginStatus.INACTIVE.getValue());
		loginEmail.emailToLower();
		UserLoginLocalDO loginLocal = new UserLoginLocalDO(loginEmail, password);
		userLogin.setLocal(loginLocal);
		userLogin.getLocal().encryptPassword();
		userLogin.setCreatedDT(new Date());
		return userLogin;
	}
}
