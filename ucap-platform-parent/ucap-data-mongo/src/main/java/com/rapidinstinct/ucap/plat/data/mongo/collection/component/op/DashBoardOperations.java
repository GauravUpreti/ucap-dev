package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudiesDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.SubjectDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public interface DashBoardOperations {

	public List<SubjectDO> read();

	public List<StudiesDO> readStudies();

	public List<StudyDO> readStudy();

	public UserAccessDO findByUserId(String userId);

	public Collection<? extends StudyDO> findByStudyId(String string);


}
