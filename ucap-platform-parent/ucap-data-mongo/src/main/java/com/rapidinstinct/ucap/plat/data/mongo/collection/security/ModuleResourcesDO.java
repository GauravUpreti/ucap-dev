package com.rapidinstinct.ucap.plat.data.mongo.collection.security;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "moduleResources")
@TypeAlias("moduleResources")
public class ModuleResourcesDO {
	
	@Id private String id;
	private String code;
	private String titile;
	private String description;	
	List<String> resources;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getTitile() {
		return titile;
	}
	
	public void setTitile(String titile) {
		this.titile = titile;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getResources() {
		return resources;
	}

	public void setResources(List<String> resources) {
		this.resources = resources;
	}

}
