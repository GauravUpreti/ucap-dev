package com.rapidinstinct.ucap.plat.data.mongo.collection.security;

import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "resourcePermissions")
@TypeAlias("resourcePermissions")
public class ResourcePermissionsDO {

	@Id private String id;
	private String code;
	private String titile;
	private String description;	
	private int permissions;
	private Map<String, Object> metadata;
	private Date createdDT;
	private Date modifiedDT;
	private int status;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public int getPermissions() {
		return permissions;
	}
	
	public void setPermissions(int actions) {
		this.permissions = actions;
	}
	
	public Map<String, Object> getMetadata() {
		return metadata;
	}
	
	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}
	
	public Date getCreatedDT() {
		return createdDT;
	}
	
	public void setCreatedDT(Date createdDT) {
		this.createdDT = createdDT;
	}
	
	public Date getModifiedDT() {
		return modifiedDT;
	}
	
	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitile() {
		return titile;
	}

	public void setTitile(String titile) {
		this.titile = titile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
