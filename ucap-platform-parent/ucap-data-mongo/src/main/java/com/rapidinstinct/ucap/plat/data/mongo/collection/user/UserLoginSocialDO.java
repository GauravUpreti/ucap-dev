package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import com.mysema.query.annotations.QueryEmbeddable;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@QueryEmbeddable
public class UserLoginSocialDO {
	
	private String socialId;
	private String token;
	private String emailId;
	private String firstName;
	private String lastName;	
	
	public String getSocialId() {
		return socialId;
	}
	
	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getEmailId() {
		return emailId;
	}
	
	public void setEmailId(String email) {
		this.emailId = email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
