package com.rapidinstinct.ucap.plat.data.mongo.collection.component;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;
/**
 *
 * @author Shamim Ahmad
 *
 */
@Document(collection="StudyList")
@TypeAlias("StudyList")
public class StudyDO extends BaseDocumentDO {
	
	private String therapeuticArea;
	private String description;
	private int siteCount;
	private int enrolledCount;
	private String studyName;
	private int kRICount;
	private int pendingNotifCount;


	public String getTherapeuticArea() {
		return therapeuticArea;
	}
	public void setTherapeuticArea(String therapeuticArea) {
		this.therapeuticArea = therapeuticArea;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getStudyName() {
		return studyName;
	}
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}
	public int getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(int siteCount) {
		this.siteCount = siteCount;
	}
	public int getEnrolledCount() {
		return enrolledCount;
	}
	public void setEnrolledCount(int enrolledCount) {
		this.enrolledCount = enrolledCount;
	}
	public int getkRICount() {
		return kRICount;
	}
	public void setkRICount(int kRICount) {
		this.kRICount = kRICount;
	}
	public int getPendingNotifCount() {
		return pendingNotifCount;
	}
	public void setPendingNotifCount(int pendingNotifCount) {
		this.pendingNotifCount = pendingNotifCount;
	}
	
	
}
