package com.rapidinstinct.ucap.plat.data.mongo.repo.resource;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgSkillDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Repository
public interface OrgSkillRepository extends MongoRepository<OrgSkillDO, String> {

	public List<OrgSkillDO> findByOrgId(String orgId, Pageable page);
}
