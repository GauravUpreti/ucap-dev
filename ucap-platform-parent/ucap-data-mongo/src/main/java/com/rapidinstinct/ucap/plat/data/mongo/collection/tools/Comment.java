package com.rapidinstinct.ucap.plat.data.mongo.collection.tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.mongodb.core.index.Indexed;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseUserDocument;

/**
 * @author Shiva Kalgudi
 *
 */

public abstract class Comment extends BaseUserDocument {
	
	protected String parentId;
	@Indexed protected String discussionId;
	protected int discussionType;
	protected String slug;
	protected String fullSlug;
	protected String commentText;

	public Comment() {
		super();
	}
	
	public Comment(String userId, String orgId) {
		super(userId, orgId);
	}

	public Comment(String userId, String orgId, String commentText, String discussionId, int discussionType) {
		this(userId, orgId);
		this.commentText = commentText;
		this.discussionId = discussionId;
		this.discussionType = discussionType;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getDiscussionId() {
		return discussionId;
	}

	public void setDiscussionId(String referrerId) {
		this.discussionId = referrerId;
	}

	public int getDiscussionType() {
		return discussionType;
	}

	public void setDiscussionType(int referrerType) {
		this.discussionType = referrerType;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getFullSlug() {
		return fullSlug;
	}

	public void setFullSlug(String fullSlug) {
		this.fullSlug = fullSlug;
	}

	public String getCommentText() {
		return commentText;
	}
	
	public String createSlug(){
		this.slug = RandomStringUtils.randomAlphanumeric(5); 
		return this.slug;
	}
	
	public String createFullSlug(){
		this.fullSlug  = System.currentTimeMillis() + ":" + this.slug;
		return fullSlug;
	}

	public void createSlugAndFullSlug(){
		createSlug(); 
		createFullSlug();
	}

}
