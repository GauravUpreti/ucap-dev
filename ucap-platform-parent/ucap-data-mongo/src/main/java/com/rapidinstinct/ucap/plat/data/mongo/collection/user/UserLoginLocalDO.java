package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.index.Indexed;

import com.mysema.query.annotations.QueryEmbeddable;
import com.rapidinstinct.ucap.plat.commons.util.Encryptor;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginStatus;

/**
 * 
 * @author Gaurav Upreti
 *
 */
@QueryEmbeddable
public class UserLoginLocalDO {
	
	private Set<LoginEmail> emailIds = null;
	private String password;
	private String firstName;
	private String lastName;

	public UserLoginLocalDO(){
		emailIds = new HashSet<UserLoginLocalDO.LoginEmail>();
	}

	public UserLoginLocalDO(LoginEmail lemail, String password){
		this();
		this.emailIds.add(lemail);
		this.password = password;
	}
	
	public Set<LoginEmail> getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(Set<LoginEmail> emailIds) {
		this.emailIds = emailIds;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean addEmail(LoginEmail email){
		return emailIds.add(email);
	}

	public int emailCount(){
		return this.emailIds.size();
	}
	
	public boolean addEmail(String email){
		LoginEmail le = new LoginEmail(email, LoginStatus.ACTIVE.getValue());
		return emailIds.add(le);
	}

	public boolean removeEmail(String email){
		LoginEmail le = new LoginEmail(email);
		return emailIds.remove(le);
	}

	public boolean comparePassword(String password){
		return Encryptor.getInstance().checkPassword(password, this.password);
	}
	
	public void encryptPassword(){
		this.password = Encryptor.getInstance().encryptPassword(this.password);
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public boolean isEmailExists(String emailId){
		boolean result = false;
		Set<LoginEmail> loginMails = this.getEmailIds();
		Iterator<LoginEmail> itrt = loginMails.iterator();
		while(itrt.hasNext()){
			LoginEmail le = itrt.next();
			result = le.getEmailId().equals(emailId);
			if(result){
				break;
			}
		}
		return result;
	}

	public boolean isActiveEmail(String emailId){
		boolean result = false;
		Set<LoginEmail> loginMails = this.getEmailIds();
		Iterator<LoginEmail> itrt = loginMails.iterator();
		while(itrt.hasNext()){
			LoginEmail le = itrt.next();
			if(le.getEmailId().equals(emailId)){
				result = LoginStatus.ACTIVE.getValue() == le.getStatus();
				break;
			}
		}
		return result;
	}

	
	//activate given email
	public boolean activateEmail(String emailId){
		boolean result = false;
		Set<LoginEmail> loginMails = this.getEmailIds();
		Iterator<LoginEmail> itrt = loginMails.iterator();
		while(itrt.hasNext()){
			LoginEmail le = itrt.next();
			if(le.getEmailId().equals(emailId)){
				le.setStatus(LoginStatus.ACTIVE.getValue());
				result = true;
				break;
			}
		}
		return result;
	}


	public boolean activateEmail(){
		boolean result = false;
		Set<LoginEmail> loginMails = this.getEmailIds();
		if(loginMails.size() == 1){
			Iterator<LoginEmail> itrt = loginMails.iterator();
			while(itrt.hasNext()){
				LoginEmail le = itrt.next();
				le.setStatus(LoginStatus.ACTIVE.getValue());
				result = true;
				break;
			}
		}
		return result;
	}

	public String getActiveEmail(){
		String activeEmail = null;
		Set<LoginEmail> loginMails = this.getEmailIds();
		Iterator<LoginEmail> itrt = loginMails.iterator();
		while(itrt.hasNext()){
			LoginEmail le = itrt.next();
			if(le.getStatus() == LoginStatus.ACTIVE.getValue()){
				activeEmail = le.getEmailId();
				break;
			}
		}
		return activeEmail;
	}

	public static class LoginEmail {
		
		@Indexed(unique=true) private String emailId;
		private int status;
		
		protected LoginEmail(){ }
		
		public LoginEmail(String emailId){
			this.emailId = emailId;
		}

		public LoginEmail(String emailId, int status){
			this.emailId = emailId;
			this.status = status;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public void emailToLower(){
			this.emailId = StringUtils.lowerCase(this.emailId);
		}
		
		 @Override public int hashCode() {
		     return Objects.hash(this.getEmailId());
		 }

		@Override public boolean equals(Object obj) {
			if(obj == this) return true;
			return obj instanceof LoginEmail &&
			        Objects.equals(this.getEmailId(), ((LoginEmail) obj).getEmailId());
		}
	}
	
}
