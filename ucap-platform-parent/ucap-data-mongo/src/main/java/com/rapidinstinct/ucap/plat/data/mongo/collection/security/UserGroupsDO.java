package com.rapidinstinct.ucap.plat.data.mongo.collection.security;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "userGroups")
@TypeAlias("userGroups")
public class UserGroupsDO {

	public static final String GRP_ADMIN = "admin";
	public static final String GRP_KNOWLEDGE_MANAGER = "knowlege-manager";
	public static final String GRP_RESOURCE_MANAGER = "resource-manager";
	public static final String GRP_MEMBER = "member";
	
	@Id protected String userId;
	protected Date createdDT;
	protected Date modifiedDT;
	private Set<OrganizationGroupsDO> orgGroups = new HashSet<OrganizationGroupsDO>();
	
	public UserGroupsDO(){ 
		this.createdDT = new Date();
		this.modifiedDT = new Date();
	}
	
	public UserGroupsDO(String userId){ 
		this();
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public Date getModifiedDT() {
		return modifiedDT;
	}

	public void setModifiedDT(Date modifyDT) {
		this.modifiedDT = modifyDT;
	}

	public Set<OrganizationGroupsDO> getOrgGroups() {
		return orgGroups;
	}

	public void setOrgGroups(Set<OrganizationGroupsDO> orgGroups) {
		this.orgGroups = orgGroups;
	}
	
	public OrganizationGroupsDO getOrganizationGroups(String orgId){
		OrganizationGroupsDO orgGrps = null;
		for(OrganizationGroupsDO group : this.orgGroups){
			if(group.getOrgId().equals(orgId)){
				orgGrps = group;
				break;
			}
		}
		return orgGrps;
	}
	
	public void addOrganizationGroups(OrganizationGroupsDO orgGroupsReq){
		if(orgGroupsReq == null){
			return;
		}
		boolean found = false;
		OrganizationGroupsDO groupToUpdate = null;
		for(OrganizationGroupsDO group : this.orgGroups){
			if(group.getOrgId().equals(orgGroupsReq.getOrgId())){
				found = true;
				groupToUpdate = group;
				break;
			}
		}

		if(found){
			for(String groupId : orgGroupsReq.getGrpIds()){
				groupToUpdate.addGroupId(groupId);
			}
		}else{
			this.orgGroups.add(orgGroupsReq);
		}
	}
	

	public boolean removeOrganizationGroups(OrganizationGroupsDO orgGroupsReq){
		boolean found = false;
		if(orgGroupsReq == null){
			return found;
		}
		
		OrganizationGroupsDO groupToUpdate = null;
		for(OrganizationGroupsDO group : this.orgGroups){
			if(group.getOrgId().equals(orgGroupsReq.getOrgId())){
				found = true;
				groupToUpdate = group;
				break;
			}
		}

		if(found){
			for(String groupId : orgGroupsReq.getGrpIds()){
				groupToUpdate.removeGroupId(groupId);
			}
		}
		
		return found;
	}

	public boolean removeOrganizationGroups(String orgId){
		boolean found = false;
		if(StringUtils.isBlank(orgId)){
			return found;
		}
		
		OrganizationGroupsDO toRemove = null;
		for(OrganizationGroupsDO group : this.orgGroups){
			if(group.getOrgId().equals(orgId)){
				found = true;
				toRemove = group;
				break;
			}
		}

		if(found){
			this.orgGroups.remove(toRemove);
		}
		
		return found; 
	}
	
	public void addOrganizationGroups(String orgId, String groupId){

		if(StringUtils.isBlank(orgId) || StringUtils.isBlank(groupId)){
			return;
		}
		
		boolean found = false;
		OrganizationGroupsDO groupToUpdate = null;
		for(OrganizationGroupsDO group : this.orgGroups){
			if(group.getOrgId().equals(orgId)){
				found = true;
				groupToUpdate = group;
				break;
			}
		}

		if(found){
			groupToUpdate.addGroupId(groupId);
		}else{
			OrganizationGroupsDO orgGroupsReq = new OrganizationGroupsDO();
			orgGroupsReq.setOrgId(orgId);
			orgGroupsReq.addGroupId(groupId);
			this.orgGroups.add(orgGroupsReq);
		}
	}

	public static class OrganizationGroupsDO {
		private String orgId;		
		private Set<String> grpIds = new HashSet<String>();
		
		public String getOrgId() {
			return orgId;
		}
		
		public void setOrgId(String orgId) {
			this.orgId = orgId;
		}
		
		public Set<String> getGrpIds() {
			return grpIds;
		}

		public void setGrpIds(Set<String> groupIds) {
			this.grpIds = groupIds;
		}
		
		public boolean addGroupId(String groupId){
			if(StringUtils.isNotBlank(groupId)){
				return this.grpIds.add(groupId);	
			}
			return false;
		}

		public boolean removeGroupId(String groupId){
			if(StringUtils.isNotBlank(groupId)){
				return this.grpIds.remove(groupId);	
			}
			return false;
		}
		
		public boolean isAdmin(){
			return this.grpIds.contains(GRP_ADMIN);
		}

		public boolean isKnowlegeManager(){
			return this.grpIds.contains(GRP_KNOWLEDGE_MANAGER);
		}
		
		public boolean isResourceManager(){
			return this.grpIds.contains(GRP_RESOURCE_MANAGER);
		}

		public boolean isMember(){
			return this.grpIds.contains(GRP_MEMBER);
		}
	}
}
