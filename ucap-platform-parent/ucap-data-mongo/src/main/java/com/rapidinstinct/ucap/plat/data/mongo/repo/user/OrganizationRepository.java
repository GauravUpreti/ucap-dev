package com.rapidinstinct.ucap.plat.data.mongo.repo.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Repository
public interface OrganizationRepository extends MongoRepository<OrganizationDO, String> {

}
