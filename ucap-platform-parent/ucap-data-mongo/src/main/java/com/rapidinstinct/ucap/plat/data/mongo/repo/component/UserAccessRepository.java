package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;

@Repository
public interface UserAccessRepository extends MongoRepository<UserAccessDO, String> {
	
	@Query("{'userId':'?0'}")
	public UserAccessDO findByUserId(String userId);
	
}
