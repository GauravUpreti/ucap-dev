package com.rapidinstinct.ucap.plat.data.mongo.collection.oauth2;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * @author Shiva Kalgudi
 *
 */

@Document(collection = "authAccessToken")
@TypeAlias("authAccessToken")
public class AuthAccessTokenDO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id	protected String id;
	private String tokenValue;
    private OAuth2AccessToken accessToken;
    private String authenticationId;
    private String userName;
    private String clientId;
    private OAuth2Authentication authentication;
    private String refreshToken;
	protected Date createdDT;
	protected Date modifiedDT;
    private int version;
    
    public AuthAccessTokenDO() {
    	this.createdDT = new Date();
    	this.modifiedDT = new Date();
    }
	
    public AuthAccessTokenDO(final OAuth2AccessToken oAuth2AccessToken, final OAuth2Authentication authentication, final String authenticationId) {
    	this();
        this.tokenValue = oAuth2AccessToken.getValue();
        this.accessToken = oAuth2AccessToken;
        this.authenticationId = authenticationId;
        this.userName = authentication.getName();
        this.clientId = authentication.getOAuth2Request().getClientId();
        this.authentication = authentication;
        this.refreshToken = oAuth2AccessToken.getRefreshToken().getValue();
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public int getVersion() {
		return version;
	}

	public String getAuthenticationId() {
        return authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2Authentication getAuthentication() {
        return authentication;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

	public String getId() {
		return id;
	}

	public Date getCreatedDT() {
		return createdDT;
	}

	public Date getModifiedDT() {
		return modifiedDT;
	}

	public void setModifiedDT(Date modifiedDT) {
		this.modifiedDT = modifiedDT;
	}

}
