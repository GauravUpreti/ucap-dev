package com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgCompanyDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.resource.OrgCompanyRepository;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public class OrgCompanyOperationsImpl implements OrgCompanyOperations {

	@Autowired 	protected OrgCompanyRepository repo;
	@Autowired 	protected MongoOperations operations;

	@Value("${component.list.size.limit}")
	private int listSize;

	@Override
	public OrgCompanyDO add(String orgId, String name) {
		OrgCompanyDO newDoc = new OrgCompanyDO(orgId, name);
		return repo.save(newDoc);
	}

	@Override
	public OrgCompanyDO add(String orgId, String name, String creator) {
		OrgCompanyDO newDoc = new OrgCompanyDO(orgId, name);
		newDoc.setCreator(creator);
		return repo.save(newDoc);
	}

	@Override
	public List<OrgCompanyDO> getCompanies(String orgId) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.limit(listSize);	
		return operations.find(query, OrgCompanyDO.class);
	}

	@Override
	public List<OrgCompanyDO> searchCompanies(String term, String orgId) {
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.NAME.getValue()).regex(term));
		query.limit(listSize);	
		return operations.find(query, OrgCompanyDO.class);
	}

	@Override
	public OrgCompanyDO findCompany(String name, String orgId) {
		OrgCompanyDO result = null;
		Query query = new Query(Criteria.where(DocumentField.ORGID.getValue()).is(orgId));
		query.addCriteria(Criteria.where(DocumentField.NAME.getValue()).is(name));
		query.limit(1);	
		List<OrgCompanyDO> resList = operations.find(query, OrgCompanyDO.class);
		if(resList.size()==1){
			result = resList.get(0);
		}
		return result;
	}

}
