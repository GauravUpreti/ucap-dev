package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.NotificationsDO;

/**
 * 
 * @author Gaurav Upreti
 * 
 */

@Repository
public interface NotificationsRepository extends MongoRepository<NotificationsDO, String> {

	@Query("{'studyName' :'?0'}")
	public List<NotificationsDO> findbystudyName(String studyName);
	
	@Query("{'_id' :'?0'}")
	public NotificationsDO findById(String id);

	@Query("{'$and':[{'studyName':?0},{'status':?1}]}")
	public List<NotificationsDO> findByStudyAndStatus(String studyName, int status );
		
}