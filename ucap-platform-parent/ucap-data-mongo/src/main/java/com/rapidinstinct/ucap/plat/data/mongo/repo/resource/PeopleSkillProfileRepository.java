package com.rapidinstinct.ucap.plat.data.mongo.repo.resource;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.PeopleSkillProfileDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Repository
public interface PeopleSkillProfileRepository extends MongoRepository<PeopleSkillProfileDO, String> {


}
