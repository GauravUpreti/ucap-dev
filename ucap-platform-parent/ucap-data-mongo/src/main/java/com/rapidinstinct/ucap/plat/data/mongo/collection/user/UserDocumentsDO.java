package com.rapidinstinct.ucap.plat.data.mongo.collection.user;

import java.util.HashSet;
import java.util.Set;

import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseUserDocument;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class UserDocumentsDO extends BaseUserDocument {

	protected Set<String> favoriteDocs = new HashSet<String>();
	protected Set<String> likedDocs = new HashSet<String>();
	protected Set<String> dislikedDocs = new HashSet<String>();

	public UserDocumentsDO(String userId, String orgId){
		super(userId, orgId);
	}
	
	public Set<String> getFavoriteDocs() {
		return favoriteDocs;
	}

	public void setFavoriteDocs(Set<String> favoriteQuestionIds) {
		this.favoriteDocs = favoriteQuestionIds;
	}

	public Set<String> getLikedDocs() {
		return likedDocs;
	}

	public void setLikedDocs(Set<String> likedDocs) {
		this.likedDocs = likedDocs;
	}

	public Set<String> getDislikedDocs() {
		return dislikedDocs;
	}

	public void setDislikedDocs(Set<String> dislikedDocs) {
		this.dislikedDocs = dislikedDocs;
	}

	public boolean addFavoriteDoc(String docId){
		boolean result = false;
		if(!favoriteDocs.contains(docId)){
			favoriteDocs.add(docId);
			result = true;
		}
		return result;
	}
	
	public boolean removeFavoriteDoc(String docId){
		return favoriteDocs.remove(docId);
	}
	
	public boolean containsFavoriteDoc(String docId){
		return favoriteDocs.contains(docId);
	}
	
	public boolean addLikedDoc(String docId){
		boolean result = false;
		if(!dislikedDocs.contains(docId) && !likedDocs.contains(docId)){
			likedDocs.add(docId);
			result = true;
		}
		return result;
	}
	
	public boolean containsLikedDoc(String docId){
		return likedDocs.contains(docId);
	}

	public boolean addDislikedDoc(String docId){
		boolean result = false;
		if(!dislikedDocs.contains(docId) && !likedDocs.contains(docId)){
			dislikedDocs.add(docId);
			result = true;
		}
		return result;
	}
	
	public boolean containsDislikedDoc(String docId){
		return dislikedDocs.contains(docId);
	}

}
