package com.rapidinstinct.ucap.plat.data.mongo.repo.component;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Repository
public interface StudyRepository extends MongoRepository<StudyDO, String> {
	
	@Query("{'studyName' : '?0'}")
	public List<StudyDO> findStudy(String studyName);

}
