package com.rapidinstinct.ucap.plat.data.mongo.repo.user.op;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;

/**
 * @author Shiva Kalgudi
 *
 */

@Component
public interface OrganizationOperations {

	public OrganizationDO findById(String id);

	public OrganizationDO add(OrganizationDO odo);

	public OrganizationDO update(OrganizationDO odo);

	public void delete(OrganizationDO odo);
	
	public List<OrganizationDO> getOrgInfo(Set<String> ids);
}
