package com.rapidinstinct.ucap.plat.data.mongo.repo.resource;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgCompanyDO;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Repository
public interface OrgCompanyRepository extends MongoRepository<OrgCompanyDO, String> {

}
