package com.rapidinstinct.ucap.plat.data.mongo.collection;

import org.springframework.data.annotation.Id;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class CommonProperties {
	
	@Id protected String id;

	public CommonProperties() {
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
