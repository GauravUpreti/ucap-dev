package com.rapidinstinct.ucap.plat.data.mongo.repo.user.op;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.OrganizationRepository;

/**
 * @author Shiva Kalgudi
 *
 */
@Component
public class OrganizationOpImpl implements OrganizationOperations {
	
	@Autowired OrganizationRepository repo;

	@Override
	public OrganizationDO findById(String id) {
		if(StringUtils.isNotBlank(id)){
			return repo.findOne(id);
		}
		return null;
	}

	@Override
	public OrganizationDO add(OrganizationDO odo) {
		if(odo != null){
			return repo.save(odo); 
		}
		return null;
	}

	@Override
	public OrganizationDO update(OrganizationDO odo) {
		if(odo != null){
			return repo.save(odo); 
		}
		return null;
	}

	@Override
	public void delete(OrganizationDO odo) {
		if(odo != null){
			repo.delete(odo); 
		}
	}

	@Override
	public List<OrganizationDO> getOrgInfo(Set<String> ids) {
		return (List<OrganizationDO>) repo.findAll(ids);
	}
}