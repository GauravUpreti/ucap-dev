package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Shamim Ahmad
 *
 */
public interface FileInfo {

	public Object getId(); 
	public void setId(Object id) ;
	public String idToString(); 
	
	public String getFileName();
	public void setFileName(String fileName);
	
	public String getContentType();
	public void setContentType(String contentType);
	
	public long getContentLength() ;
	public void setContentLength(long contentLength) ;
	
	public Date getCreateDT();
	public void setCreateDT(Date createDT);
	
	public Set<String> getAliases(); 
	public void setAliases(Set<String> aliases);
	
	public String getOrgId();
	public void setOrgId(String orgId);

	public String getCreator();
	public void setCreator(String creator);
	
	public String getReferrerId();
	public void setReferrerId(String referrerId);

	public String getReferrerSubId();
	public void setReferrerSubId(String referrerSubId);

	public String getReferrerType();
	public void setReferrerType(String referrerType);

	public String getReferrerSubType();
	public void setReferrerSubType(String referrerSubType);

	public Map<String, Object> getExtData() ;
	public void setExtData(Map<String, Object> extData);
	
	public InputStream getInputStream();
	
	public Object getInternalObject();
	public void setInternalObject(Object internalObject);
	
	public static enum ExtraFields {
		
		REFERRER_ID("referrerId"),  ORGID("orgId"), TITLE("title"), CREATOR("creator"), 
		STATUS("status"),  LIKES("likes"),  DISLIKES("dislikes"), TAGS("tags"),
		REFERRER_TYPE("referrerType"), REFERRER_SUB_TYPE("referrerSubType"), REFERRER_SUBID("referrerSubId"),
		;

		private ExtraFields(final String text) {
			this.text = text;
		}

		private final String text;

		public String getValue() {
			return text;
		}
	}
	
}
