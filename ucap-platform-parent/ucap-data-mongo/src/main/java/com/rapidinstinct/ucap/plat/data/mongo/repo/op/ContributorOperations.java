package com.rapidinstinct.ucap.plat.data.mongo.repo.op;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public interface ContributorOperations<T> {

	public List<T> getContributors(String orgId, int type);
	
	public T addContributor(String orgId, String userId, String displayName, int type);

	public T findContributor(String orgId, String userId, int type);
	
	public List<T> searchContributors(String orgId, String term, int type);
	
	public Class<T> getDocumentType();

	public MongoRepository<T, String> getRepository();
	
	public T createContributor(String orgId, String userId);

}
