package com.rapidinstinct.ucap.plat.data.mongo.collection.component.op;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Shamim Ahmad
 *
 */
public class FileMetadata {

	protected Object id;
	protected String fileName;
	protected String contentType;
	protected long contentLength;
	protected Date createDT;
	protected Set<String> aliases = new HashSet<String>();
	protected Map<String, Object> extData = new HashMap<String, Object>();
	
	public FileMetadata(Object id){
		this.id = id;
	}
	
	public Object getId() {
		return id;
	}
	
	public void setId(Object id) {
		this.id = id;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public long getContentLength() {
		return contentLength;
	}
	
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	
	public Date getCreateDT() {
		return createDT;
	}
	
	public void setCreateDT(Date createDT) {
		this.createDT = createDT;
	}
	
	public Set<String> getAliases() {
		return aliases;
	}
	
	public void setAliases(Set<String> aliases) {
		this.aliases = aliases;
	}
	
	public Map<String, Object> getExtData() {
		return extData;
	}

	public void setExtData(Map<String, Object> extData) {
		this.extData = extData;
	}

	public String getOrgId() {
		return (String) extData.get(FileInfo.ExtraFields.ORGID.toString());
	}

	public void setOrgId(String orgId) {
		extData.put(FileInfo.ExtraFields.ORGID.toString(), orgId);
	}


	public String getCreator() {
		return (String) extData.get(FileInfo.ExtraFields.CREATOR.toString());
	}

	public void setCreator(String creator) {
		extData.put(FileInfo.ExtraFields.CREATOR.toString(), creator);
	}

	public String getReferrerId() {
		return (String) extData.get(FileInfo.ExtraFields.REFERRER_ID.toString());
	}

	public void setReferrerId(String referrerId) {
		extData.put(FileInfo.ExtraFields.REFERRER_ID.toString(), referrerId);
	}

	public String getReferrerType() {
		return (String) extData.get(FileInfo.ExtraFields.REFERRER_TYPE.toString());
	}

	public void setReferrerType(String referrerType) {
		extData.put(FileInfo.ExtraFields.REFERRER_TYPE.toString(), referrerType);
	}

	public String getReferrerSubType() {
		return (String) extData.get(FileInfo.ExtraFields.REFERRER_SUB_TYPE.toString());
	}

	public void setReferrerSubType(String referrerSubType) {
		extData.put(FileInfo.ExtraFields.REFERRER_SUB_TYPE.toString(), referrerSubType);
	}
	
	public String getReferrerSubId() {
		return (String) extData.get(FileInfo.ExtraFields.REFERRER_SUBID.toString());
	}

	public void setReferrerSubId(String referrerSubId) {
		extData.put(FileInfo.ExtraFields.REFERRER_SUBID.toString(), referrerSubId);
	}

}
