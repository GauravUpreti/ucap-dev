package com.rapidinstinct.ucap.plat.security.authentication;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.service.user.UserAuthService;

/**
 * @author Gaurav Upreti
 *
 */
public class UserAuthenticationProvider implements AuthenticationProvider {

	@Autowired private UserAuthService userAuthService;
	
 	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getPrincipal() != null ? authentication.getPrincipal().toString() : null;
        String password = authentication.getCredentials() != null ? authentication.getCredentials().toString() : null;
 		ServiceRequest request = new ServiceRequest();
		LoginBO localLogin = new LoginBO(email, password);
		request.addRequestData(localLogin);

		ServiceResponse response = userAuthService.autheticate(request);
        
		UsernamePasswordAuthenticationToken token = null;
        if(response.getServiceResult().isResult()){
        	UserDetailsBO user = response.getResponseData(UserDetailsBO.class);
            String roleName = Role.ROLE_USER.toString();
            token = new UsernamePasswordAuthenticationToken(
            		email, password, Arrays.<GrantedAuthority>asList(new SimpleGrantedAuthority(roleName)));
            token.setDetails(user);
        }else{
        	if(USResultCodes.INVALID_CREDENTIAL.getValue().equals(response.getServiceResult().getResultCode())){
        		throw new BadCredentialsException(response.getServiceResult().getResultCode());	
        	}else {
        		throw new DisabledException(response.getServiceResult().getResultCode());
        	}
        }
        return token;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	
}
