package com.rapidinstinct.ucap.plat.service.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.GroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.RBMComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.service.component.RBMService;

/**
 *
 * @author Shamim Ahmad
 *
 */

@RestController
@RequestMapping ("/rbm")
public class RBMController extends BaseController{
	
	@Autowired  RBMService rbm;
	
	@SuppressWarnings("unchecked")
	@RequestMapping (method=RequestMethod.GET, value="/read/study")
	public List<StudyDO> getUserStudy(	@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		ServiceResponse response = rbm.readUserStudy(sreq);
		return (List<StudyDO>) response.getResponseData(StudyDO.class);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping (method=RequestMethod.GET, value="/read/{studyName}")	//Read thresholds by studyName.
	public List<StudyDO> getThresholds(	@PathVariable String studyName,
										@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		sreq.addRequestData(studyName);
		ServiceResponse response = rbm.readThresholds(sreq);
		return (List<StudyDO>) response.getResponseData(StudyDO.class);
	}
	
	@RequestMapping (method=RequestMethod.POST, value="/createRisk")
	public RBMComponentDO create(	@RequestBody @Valid RBMComponentDO thresholdsDO, 
									@ModelAttribute ("serviceRequest") ServiceRequest sreq ){
		sreq.addRequestData(thresholdsDO);
		ServiceResponse response = rbm.addRisk(sreq);
		return response.getResponseData(RBMComponentDO.class); 
	}	
	
	@RequestMapping(method=RequestMethod.PUT,value = "/update/{id}")
	public RBMComponentDO update(	@RequestBody @Valid RBMComponentDO thresholdsBO, 
									@PathVariable String id,
									@ModelAttribute("serviceRequest") ServiceRequest sreq){
		thresholdsBO.setId(id);
		sreq.addRequestData(thresholdsBO);
		ServiceResponse response = rbm.updateRisk(sreq);
		return response.getResponseData(RBMComponentDO.class);
	}
		
	@RequestMapping(method = RequestMethod.PUT, value="/search")
	public RBMComponentDO  search(@RequestBody @Valid RBMComponentDO thresholdsDO, 
									 @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	
		sreq.addRequestData(thresholdsDO);
    	ServiceResponse response = rbm.search(sreq);
    	return response.getResponseData(RBMComponentDO.class);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping (method=RequestMethod.GET, value="/readGroups")
	public List<GroupsDO> getGroups(	@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		ServiceResponse response = rbm.readGroups(sreq);
		return  (List<GroupsDO>) response.getResponseData(GroupsDO.class);
	}
	
	@RequestMapping (method=RequestMethod.GET, value="/countKRIs/{studyName}")
	public Integer getKRICount(	@PathVariable String studyName,
								@ModelAttribute ("serviceRequest") ServiceRequest sreq){
	sreq.addRequestData(studyName);
	ServiceResponse response = rbm.getKRICountByStudyName(sreq);
	return  response.getResponseData(Integer.class);
    }
	
	@RequestMapping (method=RequestMethod.GET, value="/countEnrolled/{studyName}")
	public Integer getEnrolledCount(@PathVariable String studyName,
									@ModelAttribute ("serviceRequest") ServiceRequest sreq){
	sreq.addRequestData(studyName);
	ServiceResponse response = rbm.getEnrolledCountByStudyName(sreq);
	return  response.getResponseData(Integer.class);
    }
	
	@RequestMapping (method=RequestMethod.GET, value="/countSites/{studyName}")
	public Integer getSiteCount(@PathVariable String studyName,
								@ModelAttribute ("serviceRequest") ServiceRequest sreq){
	sreq.addRequestData(studyName);
	ServiceResponse response = rbm.getSiteCountByStudyName(sreq);
	return  response.getResponseData(Integer.class);
    }
	
}