package com.rapidinstinct.ucap.plat.service.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.NotificationsDO;
import com.rapidinstinct.ucap.plat.service.component.NotificationService;

/**
 * 
 * @author Gaurav Upreti
 *
 */
@RestController
@RequestMapping ("/notification")
public class NotificationController extends BaseController{

	@Autowired  NotificationService ns;
	
	@SuppressWarnings("unchecked")
	@RequestMapping (method=RequestMethod.GET, value="/readNotifications/{studyName}")
	public List<NotificationsDO> getNotification(@PathVariable String studyName,@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		sreq.addRequestData(studyName);
		ServiceResponse response = ns.readNotifications(sreq);
		return  (List<NotificationsDO>) response.getResponseData(NotificationsDO.class);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping (method=RequestMethod.GET, value="/readAllNotifications")
		public List<NotificationsDO> getNotifications(	@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		ServiceResponse response = ns.readAllNotifications(sreq);
		return  (List<NotificationsDO>) response.getResponseData(NotificationsDO.class);
		}
	
	@RequestMapping(method=RequestMethod.PUT,value = "/updateNotification/{id}")
	public NotificationsDO updateNotification(	@RequestBody @Valid NotificationsDO notification, 
									@PathVariable String id,
									@ModelAttribute("serviceRequest") ServiceRequest sreq){
		notification.setId(id);
		sreq.addRequestData(notification);
		ServiceResponse response = ns.updateNotification(sreq);
		return response.getResponseData(NotificationsDO.class);
	}
	
	@RequestMapping (method=RequestMethod.GET, value="/countPendingNotifications/{studyName}")
		public Integer getPendingNotificationCount(	@PathVariable String studyName,
													@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		sreq.addRequestData(studyName);
		ServiceResponse response = ns.countPendingNotifications(sreq);
		return  response.getResponseData(Integer.class);
	}
			
}
