package com.rapidinstinct.ucap.plat.service.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rapidinstinct.ucap.plat.bo.ServiceResult;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.service.exception.DataServiceException;

/**
 * @author Gaurav Upreti
 *
 */

@ControllerAdvice
public class ServiceExceptionHandler {

	public static final String SERVICE_RESULT_ATTR = "_sericeResult";
	private static Logger _LOG = LoggerFactory.getLogger(ServiceExceptionHandler.class);    

	
    @ExceptionHandler(value = DataServiceException.class)
    public ResponseEntity<ServiceResult> defaultErrorHandler(DataServiceException e) throws Exception {
    	
    	ServiceResult result = e.getResult();
       	HttpStatus statusCode = HttpStatus.BAD_REQUEST;   

       	if(CSResultCodes.USER_ATTRIBUTES_NF.getValue().equals(result.getResultCode())){
    		statusCode = HttpStatus.UNAUTHORIZED;
    	
       	}else if(CSResultCodes.DOCUMENT_OPERATION_DENIED.getValue().equals(result.getResultCode())){
    		statusCode = HttpStatus.CONFLICT;
    	
       	}else if(CSResultCodes.REQUEST_DATA_INVALID.getValue().equals(result.getResultCode())){
    		statusCode = HttpStatus.BAD_REQUEST;
    	
       	}else if(CSResultCodes.DOCUMENT_NOT_FOUND.getValue().equals(result.getResultCode())){
    		statusCode = HttpStatus.NOT_FOUND;

       	}else if(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue().equals(result.getResultCode())){
    		statusCode = HttpStatus.FORBIDDEN;
    	}
        
       	return new ResponseEntity<ServiceResult>(result, statusCode);
    }

    @ExceptionHandler(Exception.class)
    public void exception(Exception e) throws Exception{
    	_LOG.info("Exception occurred while handling request :{}", e);
        throw e;
    }
}
