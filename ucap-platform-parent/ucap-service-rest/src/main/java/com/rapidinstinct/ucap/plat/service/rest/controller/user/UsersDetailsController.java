package com.rapidinstinct.ucap.plat.service.rest.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.ServiceResult;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserOrgGroupsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserPassChangeRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.UserProfileRequestBO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.service.resource.OrganizationService;
import com.rapidinstinct.ucap.plat.service.rest.controller.BaseController;
import com.rapidinstinct.ucap.plat.service.user.UserProfileService;

/**
 * @author Gaurav Upreti
 *
 */

@RestController
@RequestMapping("/me")
public class UsersDetailsController extends BaseController {
	@Autowired UserProfileService userService;
	@Autowired OrganizationService orgService;
	
    @RequestMapping(method = RequestMethod.GET)    
    public UserDetailsDO getUserDetails(@ModelAttribute("serviceRequest") ServiceRequest sreq){
    	ServiceResponse sres = userService.getUserProfileDetails(sreq);
    	return getResultBO(sres, UserDetailsDO.class);
    }
    
    @RequestMapping(method = RequestMethod.GET, value="/orgs")    
    public UserOrgGroupsBO getUserOrgsDetails(@ModelAttribute("serviceRequest") ServiceRequest sreq){
    	ServiceResponse sres = userService.getUserOrgDetails(sreq);
    	return getResultBO(sres, UserOrgGroupsBO.class);
    }

    @RequestMapping(method = RequestMethod.GET, value="/orgs/{id}")    
    public OrganizationDO getOrgDetails(@PathVariable String id, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(id);
    	ServiceResponse sres = orgService.read(sreq);
    	return getResultBO(sres, OrganizationDO.class);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/orgs/{id}")    
    public OrganizationDO updateOrgDetails(@PathVariable String id, @RequestBody OrganizationDO orgDetails, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	orgDetails.setId(id);
    	sreq.addRequestData(orgDetails);
    	ServiceResponse sres = orgService.update(sreq);
    	return getResultBO(sres, OrganizationDO.class);
    }

    
    @RequestMapping(method = RequestMethod.POST, value="/orgs")    
    public OrganizationDO createNewUserOrg(@RequestBody OrganizationDO orgDetails, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(orgDetails);
    	ServiceResponse sres = orgService.add(sreq);
    	return getResultBO(sres, OrganizationDO.class);
    }

    @SuppressWarnings("unchecked")
 	@RequestMapping(method = RequestMethod.GET, value="/user/query")
 	public List<UserDetailsDO>  searchByName(@RequestParam("q") String searchTerm,
 									 @ModelAttribute("serviceRequest") ServiceRequest sreq) {
     	sreq.addRequestData(searchTerm);
     	ServiceResponse response = userService.searchOrgUsers(sreq);
     	return response.getResponseData(List.class);
 	}

    @RequestMapping(method = RequestMethod.PUT)    
    public UserDetailsDO updateUserDetails(@RequestBody UserProfileRequestBO profile, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(profile);
    	ServiceResponse sres = userService.updateUserProfileDetails(sreq);
    	return getResultBO(sres, UserDetailsDO.class);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/change-password")    
    public UserDetailsBO changePassword(@RequestBody UserPassChangeRequestBO chgPass, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(chgPass);
    	ServiceResponse sres = userService.changePassword(sreq);
    	return getResultBO(sres, UserDetailsBO.class);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/accept-org-invite")    
    public ServiceResult acceptOrgInvite(@RequestBody String token, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(token);
    	ServiceResponse resp = userService.acceptOrgInvite(sreq);
    	return resp.getServiceResult();
    }	
    
    @RequestMapping(method = RequestMethod.PUT, value="/reject-org-invite")    
    public ServiceResult rejectOrgInvite(@RequestBody String token, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(token);
    	ServiceResponse resp = userService.rejectOrgInvite(sreq);
    	return resp.getServiceResult();
    }	

    @RequestMapping(method = RequestMethod.PUT, value="/add-email")    
    public ServiceResult addEmail(@RequestBody String email, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(email);
    	ServiceResponse resp = userService.addEmail(sreq);
    	return resp.getServiceResult();
    }

    @RequestMapping(method = RequestMethod.PUT, value="/remove-email")    
    public ServiceResult removeEmail(@RequestBody String email, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(email);
    	ServiceResponse resp = userService.removeEmail(sreq);
    	return resp.getServiceResult();
    }

    @RequestMapping(method = RequestMethod.PUT, value="/verify-email")    
    public ServiceResult verifyEmail(@RequestBody String token, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(token);
    	ServiceResponse resp = userService.verifyEmail(sreq);
    	return resp.getServiceResult();
    }	

    @RequestMapping(method = RequestMethod.PUT, value="/resend-verify-email")    
    public ServiceResult resendVerifyEmail(@RequestBody String email, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(email);
    	ServiceResponse resp = userService.resendVerifyEmail(sreq);
    	return resp.getServiceResult();
    }	
}