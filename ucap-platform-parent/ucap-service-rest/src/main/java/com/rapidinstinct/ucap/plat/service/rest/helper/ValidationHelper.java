package com.rapidinstinct.ucap.plat.service.rest.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;

/**
 * @author Gaurav Upreti
 *
 */

public class ValidationHelper {

	private ValidationHelper() { }
	
	
	public static List<ValidationError> createErrorList(List<FieldError> errors){
		List<ValidationError> errorList = new ArrayList<ValidationError>();
		for(FieldError error : errors){
			ValidationError verror = new ValidationError(error.getField(), error.getCode(), error.getDefaultMessage());
			errorList.add(verror);
		}
		return errorList;
	}
}
