package com.rapidinstinct.ucap.plat.service.rest.helper;

import java.io.Serializable;

/**
 * @author Gaurav Upreti
 *
 */
public class ValidationError implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String field;
	private String code;
	private String defMessage;
	
	public ValidationError(String field, String code, String defMessage){
		this.field = field;
		this.code = code;
		this.defMessage = defMessage;
	}

	public String getCode() {
		return code;
	}

	public String getDefMessage() {
		return defMessage;
	}

	public String getField() {
		return field;
	}
}
