package com.rapidinstinct.ucap.plat.service.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.MCCReportsComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.service.component.MCCReportsService;

/**
 *
 * @author Shamim Ahmad
 *
 */
@RestController
@RequestMapping("/reports")
public class MCCReportsController extends BaseController {

	@Autowired private MCCReportsService mrs;
	
	@SuppressWarnings("unchecked")
	@RequestMapping (method=RequestMethod.GET, value="/readAll")
	public List<MCCReportsComponentDO> getAllReports(	@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		ServiceResponse response = mrs.readAll(sreq);
		return (List<MCCReportsComponentDO>) response.getResponseData(MCCReportsComponentDO.class);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method=RequestMethod.GET, value="/readMCCReports/{moduleId}")
	public List<MCCReportsComponentDO> getMCCReports(	@PathVariable String moduleId, 
														@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		sreq.addRequestData(moduleId);
		ServiceResponse response = mrs.readByModuleId(sreq);
		return (List<MCCReportsComponentDO>) response.getResponseData(MCCReportsComponentDO.class);
	}
	
	@SuppressWarnings("unchecked") 
	@RequestMapping (method=RequestMethod.GET, value="/studies&therapeuticArea")
	public List<StudyDO> getUserStudy(@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		ServiceResponse response = mrs.readUserStudy(sreq);
		return (List<StudyDO>) response.getResponseData(StudyDO.class);
	}
	
  @RequestMapping(method=RequestMethod.PUT,value="/updateActiveStudies/{moduleId}")
	public MCCReportsComponentDO updateActiveStudies(
									@RequestBody @Valid MCCReportsComponentDO mccReportsComponentDO,
									@PathVariable String moduleId,
									@ModelAttribute ("serviceRequest") ServiceRequest sreq){
		mccReportsComponentDO.setModuleId(moduleId);
		sreq.addRequestData(mccReportsComponentDO);
		ServiceResponse response = mrs.updateActiveStudies(sreq);
		return response.getResponseData(MCCReportsComponentDO.class);
	}

}
