package com.rapidinstinct.ucap.plat.service.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.ServiceResult;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.service.exception.DataServiceException;
import com.rapidinstinct.ucap.plat.service.rest.filter.ApplicationFilter;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class BaseController {

	private static Logger _LOG = LoggerFactory.getLogger(BaseController.class);
	
	public static final int PAGE_SIZE = 10;

	@ModelAttribute("user")
	public UserDetailsBO getUser(HttpServletRequest request) {
		UserDetailsBO userDetails = (UserDetailsBO) request.getAttribute(ApplicationFilter.USER_IDENTITY);
		_LOG.info("Requestor loggedin id : {}", userDetails!=null ? userDetails.getUserId() : "UN_DEFINED");
		return userDetails;
	}
	
	@ModelAttribute("serviceRequest")
	public ServiceRequest createServiceRequest(HttpServletRequest request) {
		ServiceRequest sreq = new ServiceRequest();
		sreq.addUser((UserDetailsBO)request.getAttribute(ApplicationFilter.USER_IDENTITY));
		request.setAttribute(ApplicationFilter.SERVICE_REQUEST, sreq);
		return sreq;
	}
	
	
	public <T> T getResultBO(ServiceResponse response, Class<T> type) {
		T bo = null;
		if(response.getServiceResult().isResult()){
    		bo = response.getResponseData(type);
    	}else{
    		throw new DataServiceException(response.getServiceResult());
    	}
		return bo;
	}


	public ServiceResult getServiceResult(ServiceResponse response) {
		if(response.getServiceResult().isResult()){
    		return response.getServiceResult();
    	}else{
    		throw new DataServiceException(response.getServiceResult());
    	}
	}

	public void handleResult(ServiceResponse response){
    	if(!response.getServiceResult().isResult()){
    		throw new DataServiceException(response.getServiceResult());
    	}
	}
	
}
