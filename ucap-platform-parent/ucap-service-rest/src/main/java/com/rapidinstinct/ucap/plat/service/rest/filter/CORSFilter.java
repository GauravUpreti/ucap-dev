package com.rapidinstinct.ucap.plat.service.rest.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;


public class CORSFilter extends OncePerRequestFilter {
	private static Logger _LOG = LoggerFactory.getLogger(CORSFilter.class);

	@Value("${rest.cors.allowed.origins}")
	private String allowedOrigins;
	private static final Set<String> EMPTY = new HashSet<String>();

    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    	_LOG.info("Configured allowed origins {}", allowedOrigins);
        String origin = request.getHeader("Origin");
        if (getAllowedOrigins(allowedOrigins).contains(origin)) {
            response.setHeader("Access-Control-Allow-Origin", origin);
            if ("OPTIONS".equals(request.getMethod())) {
    	        response.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
    	        response.setHeader("Access-Control-Max-Age", "3600");
    	        response.setHeader("Access-Control-Allow-Headers", "X-UCAP-ORG-ID, authorization, content-type, expires_in, refresh_token, name, signedin, scope, if-modified-since, accept, token_type, access_token, x-requested-with");
            }
        }
        filterChain.doFilter(request, response);
    }

    private Set<String> parseAllowedOrigins(String allowedOriginsString) {
        if(StringUtils.isNotEmpty(allowedOriginsString)) {
            return new HashSet<String>(Arrays.asList(allowedOriginsString.split(",")));
        } else {
            return EMPTY;
        }

    }

	private Set<String> getAllowedOrigins(String allowedOriginsString) {
        return parseAllowedOrigins(allowedOriginsString);
    }

	public String getAllowedOrigins() {
		return allowedOrigins;
	}

	public void setAllowedOrigins(String allowedOrigins) {
		this.allowedOrigins = allowedOrigins;
	}
}