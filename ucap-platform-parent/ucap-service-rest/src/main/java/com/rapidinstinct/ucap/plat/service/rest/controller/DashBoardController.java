package com.rapidinstinct.ucap.plat.service.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudiesDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.SubjectDO;
import com.rapidinstinct.ucap.plat.service.component.DashBoardService;

/**
 * 
 * @author Gaurav Upreti
 *
 */
@RestController
@RequestMapping("/dashboard")
public class DashBoardController extends BaseController {

	@Autowired	DashBoardService dbs;

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value = "/readSubjects")
	public List<SubjectDO> getSubjects(@ModelAttribute("serviceRequest") ServiceRequest sreq) {
		ServiceResponse response = dbs.readSubjects(sreq);
		return (List<SubjectDO>) response.getResponseData(SubjectDO.class);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value = "/readStudies")
	public List<StudiesDO> getStudies(@ModelAttribute("serviceRequest") ServiceRequest sreq) {
		ServiceResponse response = dbs.readStudies(sreq);
		return (List<StudiesDO>) response.getResponseData(StudiesDO.class);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value = "/read/study")
	public List<StudyDO> getUserStudy(@ModelAttribute("serviceRequest") ServiceRequest sreq) {
		ServiceResponse response = dbs.readUserStudy(sreq);
		return (List<StudyDO>) response.getResponseData(StudyDO.class);
	}
	
}
