package com.rapidinstinct.ucap.plat.service.rest.controller.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.ServiceResult;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.UserLocalLoginBO;
import com.rapidinstinct.ucap.plat.service.rest.controller.BaseController;
import com.rapidinstinct.ucap.plat.service.user.UserRegistrationService;

/**
 * @author Gaurav Upreti
 *
 */

@RestController
@RequestMapping("/users")
public class UsersController extends BaseController {

	private static Logger _LOG = LoggerFactory.getLogger(UsersController.class);
	@Autowired UserRegistrationService userService;

    @RequestMapping(method = RequestMethod.POST, value="/recover-password")    
    public ServiceResult passwordRecovery(@RequestBody String email){
    	ServiceRequest request = new ServiceRequest();
    	request.addRequestData(email);
    	ServiceResponse resp = userService.recoverPassword(request);
    	return resp.getServiceResult();
    }	

    @RequestMapping(method = RequestMethod.POST, value="/verify-registration")    
    public UserLocalLoginBO verifyRegistration(@RequestBody String token){
    	ServiceRequest request = new ServiceRequest();
    	request.addRequestData(token);
    	ServiceResponse resp = userService.verifyRegistration(request);
    	return getResultBO(resp, UserLocalLoginBO.class);
    }	

    @RequestMapping(method = RequestMethod.PUT, value="/activate-login")    
    public ServiceResult activateLogin(@RequestBody String vtoken){
    	_LOG.debug("Newly created user account activation");
    	ServiceRequest sreq = new ServiceRequest();
    	sreq.addRequestData(vtoken);
    	ServiceResponse response = userService.activateLogin(sreq);
        return response.getServiceResult();
    }

    @RequestMapping(method = RequestMethod.PUT, value="/verify-invite")    
    public ServiceResult verifyOrgInvite(@RequestBody LoginBO login){
    	_LOG.debug("Organization Invite Token Validation");
    	ServiceRequest sreq = new ServiceRequest();
    	sreq.addRequestData(login);
    	ServiceResponse response = userService.verifyOrgInvite(sreq);
        return response.getServiceResult();
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/register")    
    public ResponseEntity<LoginBO> registerUser(@RequestBody LoginBO loginReq, UriComponentsBuilder builder){
    	_LOG.debug("New registration request received for user" + loginReq.getEmailId());
    	ServiceRequest sreq = new ServiceRequest();
    	sreq.addRequestData(loginReq);
    	ServiceResponse response = userService.registerUser(sreq);
    	HttpHeaders headers = null;
    	HttpStatus status = HttpStatus.BAD_REQUEST;
    	if(response.getServiceResult().isResult()){
    		loginReq = response.getResponseData(LoginBO.class);
        	_LOG.debug("Newly created user Id : {}", loginReq.getUserId());
            headers = new HttpHeaders();
            headers.setLocation(builder.path("/users/register/{id}").buildAndExpand(loginReq.getUserId()).toUri());
            status = HttpStatus.CREATED;
    	}	
        return new ResponseEntity<LoginBO>(getResultBO(response, LoginBO.class), headers, status);
    }
    
}