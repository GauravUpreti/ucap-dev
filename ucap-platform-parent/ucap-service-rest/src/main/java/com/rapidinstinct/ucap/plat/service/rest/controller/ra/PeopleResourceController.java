package com.rapidinstinct.ucap.plat.service.rest.controller.ra;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.UserGroupRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.UserGroupsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserSummaryBO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.service.resource.PeopleRS;
import com.rapidinstinct.ucap.plat.service.rest.controller.BaseController;

/**
 * @author Gaurav Upreti
 *
 */

@RestController
@RequestMapping("/resource/peoples")
public class PeopleResourceController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(PeopleResourceController.class);    
	@Autowired PeopleRS service;

/*    @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET)
	public List<PeopleSkillProfileDO> readAll(@ModelAttribute("serviceRequest") ServiceRequest sreq){
    	ServiceResponse response = service.readAll(sreq);
		return (List<PeopleSkillProfileDO>) response.getResponseData();
	}*/

    @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET,value = "/query/group")
	public List<UserDetailsDO> searchByGroup(@RequestParam("q") String groups, @ModelAttribute("serviceRequest") ServiceRequest sreq){
    	sreq.addRequestData(groups);
    	ServiceResponse response = service.searchOrgPeopleByGroup(sreq);
		return (List<UserDetailsDO>) response.getResponseData();
	}
    
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public UserSummaryBO read(@PathVariable String id, @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for profile read: Profile Key {}", id);   
    	sreq.addRequestData(id);
    	ServiceResponse response = service.readProfile(sreq);
    	return getResultBO(response, UserSummaryBO.class);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/groups")
    public OrganizationGroupsDO readGroups(@PathVariable String id, @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	sreq.addRequestData(id);
    	ServiceResponse response = service.readGroups(sreq);
    	return getResultBO(response, OrganizationGroupsDO.class);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/groups")
    public OrganizationGroupsDO updateGroups(@PathVariable String id, @RequestBody UserGroupRequestBO reqGroups, @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	reqGroups.setUserId(id);
    	sreq.addRequestData(reqGroups);
    	ServiceResponse response = service.updateGroups(sreq);
    	return getResultBO(response, OrganizationGroupsDO.class);
    }

/*    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public PeopleSkillProfileDO update(@RequestBody PeopleSkillProfileDO profile, 
    											@PathVariable String id, 
    											@ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for profile update received: Profile Key {}", id);
    	sreq.addRequestData(profile);
    	ServiceResponse response = service.updateProfile(sreq);
    	return getResultBO(response, PeopleSkillProfileDO.class);
    }*/

    @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value="/all/query")
	public List<UserDetailsDO>  searchAllPeopleByName(@RequestParam("q") String searchTerm,
									 @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	sreq.addRequestData(searchTerm);
    	ServiceResponse response = service.searchAllPeople(sreq);
    	return response.getResponseData(List.class);
	}

    @SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value="/org-non-members/query")
	public List<UserDetailsDO>  searchOrgNonMembers(@RequestParam("q") String searchTerm,
									 @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	sreq.addRequestData(searchTerm);
    	ServiceResponse response = service.searchOrgNonMembers(sreq);
    	return response.getResponseData(List.class);
	}

    
    @SuppressWarnings("unchecked")
 	@RequestMapping(method = RequestMethod.GET, value="/query")
 	public List<UserDetailsDO>  searchByOrgName(@RequestParam("q") String searchTerm,
 									 @ModelAttribute("serviceRequest") ServiceRequest sreq) {
     	sreq.addRequestData(searchTerm);
     	ServiceResponse response = service.searchOrgPeople(sreq);
     	return response.getResponseData(List.class);
 	}

    
/*    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PeopleSkillProfileDO> create(@RequestBody @Valid PeopleSkillProfileDO profile, 
    											BindingResult result,
    											UriComponentsBuilder builder, 
    											@ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for new profile creation received. Request Details : {}", profile);
    	sreq.addRequestData(profile);
    	ServiceResponse response = service.add(sreq);
    	PeopleSkillProfileDO updtProfile = getResultBO(response, PeopleSkillProfileDO.class);
    	HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/resource/people/{id}").buildAndExpand(updtProfile.getUserId()).toUri());
        return new ResponseEntity<PeopleSkillProfileDO>(updtProfile, headers, HttpStatus.CREATED);
    }*/

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/grant-access")
    public UserGroupsDO grantAccess(@RequestBody UserGroupsBO groups, 
    											@PathVariable String id, 
    											@ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for grant access received: Profile Key {}", id);
    	groups.setUserId(id);
    	sreq.addRequestData(groups);
    	ServiceResponse response = service.grantAccess(sreq);
    	return getResultBO(response, UserGroupsDO.class);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/revoke-access")
    public UserGroupsDO removeAccess(@RequestBody UserGroupsBO groups, 
    											@PathVariable String id, 
    											@ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for remove access received: Profile Key {}", id);
       	groups.setUserId(id);
        sreq.addRequestData(groups);
    	ServiceResponse response = service.revokeAccess(sreq);
    	return getResultBO(response, UserGroupsDO.class);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/remove")
    public UserGroupsDO removeUserFromOrg(@PathVariable String id, @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for user remove from org is received: Profile Key {}", id);
        sreq.addRequestData(id);
    	ServiceResponse response = service.removeUserFromOrg(sreq);
    	return getResultBO(response, UserGroupsDO.class);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/invite")
    public void inviteUser(@RequestBody UserGroupsBO groups, 
    											@PathVariable String id, 
    											@ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	logger.debug("Request for invite user received: Profile Key {}", id);
    	groups.setUserId(id);
    	sreq.addRequestData(groups);
    	ServiceResponse response = service.inviteUserToOrg(sreq);
    	handleResult(response);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/invite-new-user")
    public void inviteNewUser(@RequestBody LoginBO login,  @ModelAttribute("serviceRequest") ServiceRequest sreq) {
    	sreq.addRequestData(login);
    	ServiceResponse response = service.inviteNewUserToOrg(sreq);
    	handleResult(response);
    }
}
