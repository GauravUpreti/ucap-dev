package com.rapidinstinct.ucap.plat.service.rest.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.filter.OncePerRequestFilter;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.service.user.UserAuthService;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class ApplicationFilter extends OncePerRequestFilter {
	private static Logger _LOG = LoggerFactory.getLogger(ApplicationFilter.class);
	public static final String ORG_HEADER = "X-UCAP-ORG-ID";
	public static final String USER_IDENTITY = "_USER";
	public static final String SERVICE_REQUEST = "_SREQ";

	@Autowired UserAuthService userAuthService;
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		addUserIdentity(request);
		filterChain.doFilter(request, response);
	}

	private boolean addUserIdentity(HttpServletRequest httpRequest){
		boolean result = false;
		if(httpRequest.getUserPrincipal() == null){
			_LOG.warn("User Identity not found: {}");
			return false;
		}
		OAuth2Authentication auth = (OAuth2Authentication) httpRequest.getUserPrincipal();
		UserDetailsBO userBO = (UserDetailsBO) auth.getUserAuthentication().getDetails();
		String orgId = httpRequest.getHeader(ORG_HEADER);
		_LOG.debug("User Identity found: {}", userBO.getEmail());
		if(userBO != null){
			userBO.setCurrentOrgId(orgId);
			ServiceRequest request = new ServiceRequest();
			request.addRequestData(userBO);
			userAuthService.addCurrentOrgGroups(request);
			httpRequest.setAttribute(USER_IDENTITY, userBO);
		}
		return result;
	}

}
