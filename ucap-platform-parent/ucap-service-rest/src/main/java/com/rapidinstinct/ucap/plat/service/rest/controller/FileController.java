package com.rapidinstinct.ucap.plat.service.rest.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.FileInfo;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.FileOperations;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Controller
@RequestMapping("/files")
public class FileController extends BaseController {
	public final static int  BUFFER_SIZE  = 2048;
	@Autowired FileOperations operations;

	@RequestMapping(value = "/read/{therapeuticArea}", method = RequestMethod.GET)
	public void readStudyImg(@PathVariable String therapeuticArea, HttpServletResponse response) throws IOException {
		try{
			FileInfo fileInfo = operations.retriveFileByTherapeuticArea(therapeuticArea);
			if(fileInfo!=null && fileInfo.getInputStream()!=null){
				if(fileInfo.getContentLength() > 0){
					response.setHeader("Content-length", "" + fileInfo.getContentLength());
				}
				if(StringUtils.isNotBlank(fileInfo.getContentType())){
					response.setContentType(fileInfo.getContentType());
				}
				if(StringUtils.isNotBlank(fileInfo.getFileName())){
					response.setHeader("Content-disposition", "inline; filename=" + fileInfo.getFileName());
				}else{
					response.setHeader("Content-disposition", "inline");
				}
				byte[] buffer = new byte[BUFFER_SIZE];
				OutputStream outStream = response.getOutputStream();
				InputStream inputStream = fileInfo.getInputStream();
				try{
					while(true) {
					    int bytesRead = inputStream.read(buffer);
					    if (bytesRead <= 0)
					      break;
					    outStream.write(buffer, 0, bytesRead);
					}
					inputStream.close();
					outStream.close();
				}catch(IOException e){
					response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Server");
				}finally{
					outStream.close();
					inputStream.close();
				}	
			}else{
				response.sendError(HttpStatus.NOT_FOUND.value(), "File not found");
			}
		}catch(Exception e){
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Server");
		}
	}
	
	
	@RequestMapping(value = "/readUserImg/{userId}", method = RequestMethod.GET)
	public void readUserImg(@PathVariable String userId, HttpServletResponse response) throws IOException {
		try{
			FileInfo fileInfo = operations.retriveByUserId(userId);
			if(fileInfo!=null && fileInfo.getInputStream()!=null){
				if(fileInfo.getContentLength() > 0){
					response.setHeader("Content-length", "" + fileInfo.getContentLength());
				}
				if(StringUtils.isNotBlank(fileInfo.getContentType())){
					response.setContentType(fileInfo.getContentType());
				}
				if(StringUtils.isNotBlank(fileInfo.getFileName())){
					response.setHeader("Content-disposition", "inline; filename=" + fileInfo.getFileName());
				}else{
					response.setHeader("Content-disposition", "inline");
				}
				byte[] buffer = new byte[BUFFER_SIZE];
				OutputStream outStream = response.getOutputStream();
				InputStream inputStream = fileInfo.getInputStream();
				try{
					while(true) {
					    int bytesRead = inputStream.read(buffer);
					    if (bytesRead <= 0)
					      break;
					    outStream.write(buffer, 0, bytesRead);
					}
					inputStream.close();
					outStream.close();
				}catch(IOException e){
					response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Server");
				}finally{
					outStream.close();
					inputStream.close();
				}	
			}else{
				response.sendError(HttpStatus.NOT_FOUND.value(), "File not found");
			}
		}catch(Exception e){
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Server");
		}
	}
	
	
	@RequestMapping(value = "/readMCCModuleImg/{moduleId}", method = RequestMethod.GET)
	public void readMCCModuleImg(@PathVariable String moduleId, HttpServletResponse response) throws IOException {
		try{
			FileInfo fileInfo = operations.retriveByModuleId(moduleId);
			if(fileInfo!=null && fileInfo.getInputStream()!=null){
				if(fileInfo.getContentLength() > 0){
					response.setHeader("Content-length", "" + fileInfo.getContentLength());
				}
				if(StringUtils.isNotBlank(fileInfo.getContentType())){
					response.setContentType(fileInfo.getContentType());
				}
				if(StringUtils.isNotBlank(fileInfo.getFileName())){
					response.setHeader("Content-disposition", "inline; filename=" + fileInfo.getFileName());
				}else{
					response.setHeader("Content-disposition", "inline");
				}
				byte[] buffer = new byte[BUFFER_SIZE];
				OutputStream outStream = response.getOutputStream();
				InputStream inputStream = fileInfo.getInputStream();
				try{
					while(true) {
					    int bytesRead = inputStream.read(buffer);
					    if (bytesRead <= 0)
					      break;
					    outStream.write(buffer, 0, bytesRead);
					}
					inputStream.close();
					outStream.close();
				}catch(IOException e){
					response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Server");
				}finally{
					outStream.close();
					inputStream.close();
				}	
			}else{
				response.sendError(HttpStatus.NOT_FOUND.value(), "File not found");
			}
		}catch(Exception e){
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Server");
		}
	}
	
}