package com.rapidinstinct.ucap.plat.service.rest.controller.test;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Arrays;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/ucap-service-rest.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class AbstractContextControllerTests {
	
	protected MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext wac;
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(this.wac).build();
        System.out.println(Arrays.asList(wac.getBeanDefinitionNames()));
	}

}
