package com.rapidinstinct.ucap.plat.service.rest.controller.test.fixure;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.io.ClassPathResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class DataFixureUtil {

	public static enum FixureType{
		QUESTION(1), FORM(2), ELEMENT(3);

		private FixureType(final int text) {
			this.text = text;
		}
		
		private final int text;
		
		public int getValue() {
			return text;
		}

	}
	
	protected final static Gson gson;

	static { 
		final GsonBuilder builder = new GsonBuilder();  
		builder.setDateFormat("MM-dd-yyyy");
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
		    final DateFormat df = new SimpleDateFormat("MM-dd-yyyy");  
		    @Override  
		    public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {  
		        try {  
		            return df.parse(json.getAsString());  
		        } catch (final java.text.ParseException e) {  
		            e.printStackTrace();  
		            return null;  
		        }  
		    }  
		});
		gson = builder.create();
	
}
	
	public static  <T> T convertToListObject(String dataFileName, Type listType){
		ClassPathResource resource = new ClassPathResource("data/" + dataFileName);
		Reader dataReader = null;
		try {
			dataReader = new FileReader(resource.getFile()); 
			return fromJsonAll(dataReader, listType);		
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}finally{
			if(dataReader!=null){
				try {
					dataReader.close();
				} catch (IOException e) {
				}
			}
		}
		
		return null;
	}
	

	public static  <T> T convertToListObject(String dataFileName, Class<T> type){
		ClassPathResource resource = new ClassPathResource(dataFileName);
		Reader dataReader = null;
		try {
			dataReader = new FileReader(resource.getFile()); 
			return fromJsonAll(dataReader, type);		
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}finally{
			if(dataReader!=null){
				try {
					dataReader.close();
				} catch (IOException e) {
				}
			}
		}
		
		return null;
	}

	protected static <T> T  fromJsonAll(Reader jsonStream, Type listType) {
		return  gson.fromJson(jsonStream, listType);
	}

	
	protected static <T> T  fromJsonAll(Reader jsonStream, Class<T> type) {
		return  gson.fromJson(jsonStream, type);
	}

	
	public static String toJsonAll(Object object){
		if(object!=null){
			return gson.toJson(object);
		}else{
			return null;
		}
	}
}
