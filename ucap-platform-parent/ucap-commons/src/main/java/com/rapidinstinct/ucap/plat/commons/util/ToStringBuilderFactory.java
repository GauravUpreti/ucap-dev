package com.rapidinstinct.ucap.plat.commons.util;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 
 * @author Shiva Kalgudi
 * 
 * Class toString builder factory.. 
 * Classes can utilize this factory to create string representation of objects
 */

public class ToStringBuilderFactory {	
	
	//default display style
	public static ToStringStyle TOSTRING_STYLE = ToStringStyle.SHORT_PREFIX_STYLE;
	
	private ToStringBuilderFactory(){
		super();
	}
	
	//ToStringBuilder factory method
	public static ToStringBuilder build(Object object){
		return new ToStringBuilder(object, TOSTRING_STYLE);
	}
}
