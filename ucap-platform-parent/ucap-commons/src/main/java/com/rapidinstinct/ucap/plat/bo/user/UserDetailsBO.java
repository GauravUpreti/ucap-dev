package com.rapidinstinct.ucap.plat.bo.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;

/**
 * @author Gaurav Upreti
 *
 */
public class UserDetailsBO implements UserDetails {

	private static final long serialVersionUID = 1L;
	private String userId;
    private String email;
    private String hashedPassword;
    private String firstName;
    private String lastName;
    private List<Role> roles = new ArrayList<Role>();
    private boolean enabled;
	private String currentOrgId;

    
    public UserDetailsBO(String userId) {
        this();
        this.userId = userId;
    }

    public UserDetailsBO() {
        super();
    }

    public UserDetailsBO(final String email, final String hashedPassword,final String firstName, final String lastName, Role ... roles) {
        this();
        this.email = email;
        this.hashedPassword = hashedPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        for(Role role:roles){
            this.roles.add(role);
        }
    }

    public String getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (Role role : this.getRoles()) {
			GrantedAuthority authority = new SimpleGrantedAuthority(role.name());
			authorities.add(authority);
		}
		return authorities;
	}

	@Override
	public String getPassword() {
		return hashedPassword;
	}

	@Override
	public String getUsername() {
		return email;
	}
	
	public List<Role> getRoles() {
		return roles;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void addRole(Role role) {
        this.roles.add(role);
    }

    public boolean hasRole(Role role) {
        return (this.roles.contains(role));
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCurrentOrgId() {
		return currentOrgId;
	}

	public void setCurrentOrgId(String currentOrgId) {
		this.currentOrgId = currentOrgId;
	}
	
	public boolean isUserIdAndOrgIdDefined(){
		return (StringUtils.isNotBlank(currentOrgId) && StringUtils.isNotBlank(userId));
	}
	
	public boolean isOrgAdmin(){
		return hasRole(Role.ROLE_ADMIN);
	}
	
	public boolean isOrgKM(){
		return hasRole(Role.ROLE_KNOWLEDGE_MANAGER);	
	}

	public boolean isOrgResourceManager(){
		return hasRole(Role.ROLE_RSOURCE_MANAGER);	
	}

	public boolean isOrgMember(){
		return hasRole(Role.ROLE_MEMBER);	
	}

	
	public String getDisplayName(){
		String dname = null;
		if(StringUtils.isNotBlank(firstName) && StringUtils.isBlank(lastName)){
			dname = firstName;
		}else if(StringUtils.isBlank(firstName) && StringUtils.isNotBlank(lastName)){
			dname = lastName;
		}else if(StringUtils.isNotBlank(firstName) && StringUtils.isNotBlank(lastName)){
			dname = firstName + " " + lastName;
		}else if(StringUtils.isNotBlank(email)){
			dname = email;
		}
		return dname;
	}
}
