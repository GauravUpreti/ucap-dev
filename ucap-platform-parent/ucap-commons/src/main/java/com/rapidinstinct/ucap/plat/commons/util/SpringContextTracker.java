package com.rapidinstinct.ucap.plat.commons.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * 
 * @author Shiva Kalgudi
 *
 * 
 * This class caches the spring contexts to improve the performance of spring bean lookup
 */
public class SpringContextTracker {
	
	private volatile static SpringContextTracker instance = new SpringContextTracker();
	private Map<String, ClassPathXmlApplicationContext> contextHashMap = new HashMap<String, ClassPathXmlApplicationContext>();

	private SpringContextTracker() {
		super();
	}

	public static ClassPathXmlApplicationContext getContext(String packageName, String fileName) throws SpringBeanLocatorException {
		if (StringUtils.isEmpty(fileName)) {
			throw new SpringBeanLocatorException("SpringContextTracker->buildOrGetContext: parameter 'fileName' cannot be empty");
		}
		return instance.retrieveContext(packageName, fileName);
	}

	private ClassPathXmlApplicationContext retrieveContext(String packageName, String fileName) throws BeanDefinitionStoreException {
		String builtFileName = buildConfigFilename(packageName, fileName);
		if (this.contextHashMap.containsKey(builtFileName)) {
			return contextHashMap.get(builtFileName);
		}
		ClassPathXmlApplicationContext cpXmlAppCtx = new ClassPathXmlApplicationContext(builtFileName);		
		this.contextHashMap.put(builtFileName, cpXmlAppCtx);
		return cpXmlAppCtx;
	}

    private static String buildConfigFilename(String packageName, String fileName) {
        return new StringBuffer().append(packageName).append(File.separator).append(fileName).toString();
    }

}