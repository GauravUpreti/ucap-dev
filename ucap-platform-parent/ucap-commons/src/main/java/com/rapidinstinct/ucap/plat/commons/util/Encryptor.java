package com.rapidinstinct.ucap.plat.commons.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

public class Encryptor {

	private static Encryptor encryptor = null;
	private static StrongPasswordEncryptor passwordEncryptor = null;
	private static BasicTextEncryptor textEncryptor = null;	
	private static final String TextEncryptorPassword = "QRWYOPYDSGHBLKMXZCIAV";	//This is a random string
	
	private static final Logger logger = LoggerFactory.getLogger(Encryptor.class.getName());
	
	private Encryptor() { }

	static {
		encryptor = new Encryptor();
		passwordEncryptor = new StrongPasswordEncryptor();
		textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(TextEncryptorPassword);
	}
	
	public static Encryptor getInstance() {
		return encryptor;
	}

	public String encryptString(String text) {
	    String encryptedText=null;
		try {
		    encryptedText = URLEncoder.encode(textEncryptor.encrypt(text),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot encrypt the string " + text + "  Ex->" + e.getMessage());
		}
		return encryptedText;
	}
	
	public String encryptStringForced(String text) {
		String encryptedText = null;
		do {
			encryptedText = encryptString(text);
		} while(StringUtils.contains(encryptedText, '/'));
		
		return encryptedText;
	}
		
	public String decryptString(String encryptedText) {
		if(!StringUtils.isEmpty(encryptedText)) {
			return textEncryptor.decrypt(encryptedText);
		}
		else { 
			return "";
		}
	}
	
	public String encryptPassword(String password) {
		if (password != null) {
			return passwordEncryptor.encryptPassword(password);
		}
		return null;
	}

	public boolean checkPassword(String plainPassword, String encryptedPassword) {
		return passwordEncryptor.checkPassword(plainPassword, encryptedPassword);
	}
	
	
	public String getRandomPassword(int length) {
		return getRandomString(length);
	}
	
	public String getRandomString(int length) {
		Random random = new Random(); 
		char[] symbols = new char[36];
		char[] buf;
		
		for (int idx = 0; idx < 10; ++idx)
			symbols[idx] = (char) ('0' + idx);
	    for (int idx = 10; idx < 36; ++idx)
	    	symbols[idx] = (char) ('a' + idx - 10);
	    
	    buf = new char[length];
	    
	    for (int idx = 0; idx < buf.length; ++idx) 
	    	buf[idx] = symbols[random.nextInt(symbols.length)]; 
	    
	    return new String(buf);
	}
	
	
	public List<String> encryptList(List<?> dataList){
		List<String> encryptList = new ArrayList<String>();
		//Encrypt each data item from the input list and return a new encrypted list
		for(Object id : dataList){
			encryptList.add(encryptString(id.toString()));
		}
		return encryptList;
	}	

}
