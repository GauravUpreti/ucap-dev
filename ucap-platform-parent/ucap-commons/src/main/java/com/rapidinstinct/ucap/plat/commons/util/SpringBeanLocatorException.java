package com.rapidinstinct.ucap.plat.commons.util;

/**
 * @author Shiva Kalgudi
 *
 */
public class SpringBeanLocatorException extends RuntimeException {

	private static final long serialVersionUID = -795625394106806754L;

    public SpringBeanLocatorException() {
        super();
    }

    public SpringBeanLocatorException(String message) {
        super(message);
    }

    public SpringBeanLocatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SpringBeanLocatorException(Throwable cause) {
        super(cause);
    }

}
