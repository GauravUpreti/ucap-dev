package com.rapidinstinct.ucap.plat.bo.user;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class USConstants {

	public static enum USResultCodes {
		
		DUPLICATE_EMAIL("duplicate-email"), 
		EMAIL_NOT_FOUND("email-not-found"), 
		INVALID_CREDENTIAL("invalid-credentials"), 
		USER_INACTIVATED("user-inactivated"), 
		INVALID_REQUEST("invalid-request"),
		VERIFICATION_PENDING("verification-pending"),
		REGISTRATION_SUCCESS("registration-success"),
		REGISTRATION_FAILED("registration-failed"),
		USER_NOT_FOUND("user-not-found"),
		USER_DETAILS_NOT_FOUND("user-not-found"),
		USER_DETAILS_FOUND("user-found"),
		USER_FOUND("user-found"),
		USER_PROFILE_UPDATED("user-profile-updated"),
		LOGIN_SUCCESS("login-success"),
		USER_GROUPS_FOUND("user-groups-found"),
		USER_GROUPS_NOT_FOUND("user-groups-nf"),
		USER_ALREADY_MEMBER("user-already-member"),
		USER_NOT_A_MEMBER("user-not-a-member"),
		DUPLICATE_MEMBERSHIP_REQUEST("duplicate-membership-request"),
		ACTIVATION_PENDING("activation-pending"),
		USER_ACTIVE("user-active"),
		;
		
		private USResultCodes(final String text) {
			this.text = text;
		}

		private final String text;

		public String getValue() {
			return text;
		}
		
		public static USResultCodes getType(String value){
			USResultCodes types [] = USResultCodes.values();
			for(USResultCodes type:types){
				if(StringUtils.equals(value, type.getValue())){
					return type;
				}
			}
			return null;
		}
	}
	
	
	
	public static enum  AccessAttributes {
		EMAIL_ID("emailId");
		
		private final String text;

		private AccessAttributes(final String text) {
			this.text = text;
		}

		public String getValue() {
			return text;
		}

	}

	public static enum Role {
		ROLE_ADMIN("admin"),
		ROLE_KNOWLEDGE_MANAGER("knowlege-manager"),
		ROLE_USER("user"),
		ROLE_RSOURCE_MANAGER("resource-manager"),
		ROLE_MEMBER("member");
		
		private Role(final String text) {
			this.text = text;
		}
		private final String text;
		public String getValue() {
			return text;
		}
		
		public static Role getType(String value){
			Role types [] = Role.values();
			for(Role type:types){
				if(StringUtils.equals(value, type.getValue())){
					return type;
				}
			}
			return null;
		}
		
	}


}
