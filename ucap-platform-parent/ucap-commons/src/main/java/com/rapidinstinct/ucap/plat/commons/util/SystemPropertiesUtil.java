package com.rapidinstinct.ucap.plat.commons.util;


/**
 * 
 * @author Shiva Kalgudi
 *
 * System Property Utility class.. Provides APIs to read the system properties, 
 * defines system constants for application types and deployment environments
 */
public class SystemPropertiesUtil {
	
	//System Environment property name to identify deployment environment, default value is {@link DeploymentEnvironment#DEV}
	public static final String SYS_VARIABLE_ENVIRONMENT = "com.rapidinstinct.ucap.deploy.environment";


	//deployment environment name list
	public static enum DeploymentEnvironment {
		
		DEV("dev"), PRESIT("presit"), SIT("sit"), UAT("uat"), PRD("prd"), ST("st");

		private DeploymentEnvironment(final String text) {
			this.text = text;
		}

		private final String text;

		public String toString() {
			return text;
		}
	}


	//private constructor to stop creating instances 
	private SystemPropertiesUtil() {
		super();
	}

	/**
	 * method returns deployment environment name.
	 * Method looks for environment variable {@link SYS_VARIABLE_ENVIRONMENT}
	 * if environment variable is not defined, default value is {@link DeploymentEnvironment#DEV} returned
	 */
	public static String getDeploymentEnvironment() {
		return System.getProperty(SYS_VARIABLE_ENVIRONMENT) !=null ? System.getProperty(SYS_VARIABLE_ENVIRONMENT):DeploymentEnvironment.DEV.toString(); 
	}


	/**
	 * Method sets deployment environment variable.
	 * Should be strictly in development environment only.
	 * for other environment set via container provided support 
	 */
	public static void setDeploymentEnvironment(DeploymentEnvironment envName){
		System.setProperty(SYS_VARIABLE_ENVIRONMENT, envName.toString());
	}


	public static String getNamespace(){
		return "ucap";
	}

	public static String setNamespace(String namespace){
		return null;
	}

}
