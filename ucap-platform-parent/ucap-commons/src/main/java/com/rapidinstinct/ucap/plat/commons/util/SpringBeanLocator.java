package com.rapidinstinct.ucap.plat.commons.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Shiva Kalgudi
 * 
 * Spring bean locator Utility class
 */
public class SpringBeanLocator {

	private String configFilePath = null;
	private String configFilename = null;
	private String beanName = null;
	private Class<?> beanClass = null;

	public static <T> T getBean(String configFilePath, String configFilename, String beanName, Class<T> beanClass) throws BeansException {
		if (StringUtils.isBlank(beanName)) {
			throw new BeanCreationException("SpringBeanLocator->getBean(String, String, String, Class): 'beanName' value cannot be null or empty string");
		}

		ClassPathXmlApplicationContext ctx = null;
		try {
			ctx = SpringContextTracker.getContext(configFilePath, configFilename);
		} catch (SpringBeanLocatorException e) {
			throw new ApplicationContextException(e.getMessage(), e);
		}
		return (T) ctx.getBean(beanName, beanClass);
		//return null;
	}

	public static Object getBean(String configFilePath, String configFilename, String beanName) throws BeansException {
		return getBean(configFilePath, configFilename, beanName, null);
	}

	@SuppressWarnings("unchecked")
	public <T> T getBean() throws BeansException {
		return (T) getBean(this.getConfigFilePath(), this.getConfigFilename(), this.getBeanName(), this.getBeanClass());
	}

	public String toString() {
		ToStringBuilder out = ToStringBuilderFactory.build(this);
		out.appendSuper(super.toString());
		out.append("configFilePath", this.getConfigFilePath());
		out.append("configFilename", this.getConfigFilename());
		out.append("beanName", this.getBeanName());
		out.append("beanClass", this.getBeanClass());
		return out.toString();
	}

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getConfigFilename() {
		return configFilename;
	}

	public void setConfigFilename(String configFileName) {
		this.configFilename = configFileName;
	}

	public String getConfigFilePath() {
		return configFilePath;
	}

	public void setConfigFilePath(String configFilePath) {
		this.configFilePath = configFilePath;
	}

	public Class<?> getBeanClass() {
		return beanClass;
	}

	public void setBeanClass(Class<?> beanClass) {
		this.beanClass = beanClass;
	}

}
