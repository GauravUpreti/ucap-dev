package com.rapidinstinct.ucap.plat.service.resource;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * @author Gaurav Upreti
 *
 */

@Service
public interface PeopleRS {

	public  ServiceResponse readAll(ServiceRequest request);

	public ServiceResponse searchOrgPeople(ServiceRequest request);

	public ServiceResponse searchOrgPeopleByGroup(ServiceRequest request);

	public ServiceResponse searchAllPeople(ServiceRequest request);

	public ServiceResponse searchOrgNonMembers(ServiceRequest request);

	public ServiceResponse add(ServiceRequest request);

	public ServiceResponse readProfile(ServiceRequest request);

	public ServiceResponse readGroups(ServiceRequest request);

	public ServiceResponse updateGroups(ServiceRequest request);

	public ServiceResponse updateProfile(ServiceRequest request);

	public ServiceResponse grantAccess(ServiceRequest request);
	
	public ServiceResponse revokeAccess(ServiceRequest request);
	
	public ServiceResponse removeUserFromOrg(ServiceRequest request);

	public ServiceResponse inviteUserToOrg(ServiceRequest request);
	
	public ServiceResponse inviteNewUserToOrg(ServiceRequest request);

}
