package com.rapidinstinct.ucap.plat.cbo.mapper;

import org.springframework.stereotype.Component;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public interface BOMapper<C extends BaseDocumentDO, B extends BaseBusinessObject> {

	public B fromCDOToBO(C cdo);
	
	public C createNewDOFromBO(B bo, String userId, String orgId);
	
	public C copyUpdatedBOToDO(B bo, C cdo, String modifier);
	
	public Class<B> getBOType();

	public Class<C> getCDOType();
}
