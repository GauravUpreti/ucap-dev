package com.rapidinstinct.ucap.plat.service.user;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public interface UserRegistrationService {

	public ServiceResponse isEmailInUse(ServiceRequest request);
	
	public ServiceResponse registerUser(ServiceRequest request);
	
	public ServiceResponse verifyRegistration(ServiceRequest request);
	
	public ServiceResponse recoverPassword(ServiceRequest request);
	
	public ServiceResponse activateLogin(ServiceRequest request);

	public ServiceResponse activateInvitedLogin(ServiceRequest request);

	public ServiceResponse verifyOrgInvite(ServiceRequest request);
}
