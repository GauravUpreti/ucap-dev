package com.rapidinstinct.ucap.plat.cbo;


/**
 *
 * @author Shamim Ahmad
 *
 */
public class RBMConstants {

	public static enum StudyResultCodes {
		ACTIVE(11), 
		INACTIVE(21);
		
		private StudyResultCodes(final int text) {
			this.text = text;
		}

		private final int text;

		public int getValue() {
			return text;
		}
	}
	

	public static enum StudyNotificationStatusCodes {
		PENDING(11), 
		ATTENDED(21);
		
		private StudyNotificationStatusCodes(final int text) {
			this.text = text;
		}

		private final int text;

		public int getValue() {
			return text;
		}
	}
}
