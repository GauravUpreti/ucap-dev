package com.rapidinstinct.ucap.plat.service.resource.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserGroupRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.UserGroupsBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.commons.util.Encryptor;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginStatus;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.UserOperations;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.VerificationOperations;
import com.rapidinstinct.ucap.plat.service.MailServiceHelper;
import com.rapidinstinct.ucap.plat.service.helper.AuthorizationHelper;
import com.rapidinstinct.ucap.plat.service.helper.AuthorizationHelper.RAResourceOperation;
import com.rapidinstinct.ucap.plat.service.helper.AuthorizationResult;

/**
 * @author Gaurav Upreti
 *
 */

@Service
public class PeopleRSImpl extends PeopleRSBaseImpl {
	@Autowired private VerificationOperations verOps;
	@Autowired private MailServiceHelper msInvk;
	@Autowired private UserOperations uops;
	
	/**
	 * User should be part of organization to grant access.
	 * If user is not part of organization, user should be invite to become member of organization
	 */
	@Override
	public ServiceResponse grantAccess(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserGroupsBO requestGrps = null;
		UserGroupsDO currentGroups = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.UPDATE);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			UserDetailsBO user = request.getUser();
			String orgId = user.getCurrentOrgId();
			requestGrps = request.getRequestData(UserGroupsBO.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(requestGrps!=null && StringUtils.isNotBlank(requestGrps.getUserId())) {
				//check if user has org-membership
				resultCode = USResultCodes.USER_NOT_A_MEMBER.toString();
				UserDetailsDO usrDTL = userDetailsRepo.findOne(requestGrps.getUserId());
				if(usrDTL!=null && usrDTL.isMemberOfOrg(orgId)) {
					currentGroups = userGroupsRepo.findOne(requestGrps.getUserId());
					OrganizationGroupsDO groups = new OrganizationGroupsDO();
					groups.setOrgId(orgId);
					groups.setGrpIds(requestGrps.getGroups());
					if(currentGroups!=null){
						currentGroups.addOrganizationGroups(groups);
						currentGroups.setModifiedDT(new Date());
					}else{
						currentGroups = new UserGroupsDO(requestGrps.getUserId());
						currentGroups.addOrganizationGroups(groups);
					}
					userGroupsRepo.save(currentGroups);
					result = true;
					resultCode = USResultCodes.USER_PROFILE_UPDATED.toString();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(currentGroups);
		return response;
	}

	@Override
	public ServiceResponse revokeAccess(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserGroupsBO requestGrps = null;
		UserGroupsDO currentGroups = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.UPDATE);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			UserDetailsBO user = request.getUser();
			requestGrps = request.getRequestData(UserGroupsBO.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(requestGrps!=null && StringUtils.isNotBlank(requestGrps.getUserId())) {
				currentGroups = userGroupsRepo.findOne(requestGrps.getUserId());
				String orgId = user.getCurrentOrgId();
				OrganizationGroupsDO groups = new OrganizationGroupsDO();
				groups.setOrgId(orgId);
				groups.setGrpIds(requestGrps.getGroups());
				if(currentGroups!=null){
					currentGroups.removeOrganizationGroups(groups);
					currentGroups.setModifiedDT(new Date());
					userGroupsRepo.save(currentGroups);
					result = true;
					resultCode = USResultCodes.USER_PROFILE_UPDATED.toString();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(currentGroups);
		return response;
	}

	@Override
	public ServiceResponse updateGroups(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		OrganizationGroupsDO orgGroups = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.UPDATE);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			UserDetailsBO user = request.getUser();
			String orgId = user.getCurrentOrgId();
			UserGroupRequestBO  requestGrps = request.getRequestData(UserGroupRequestBO.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(requestGrps!=null && requestGrps.isValidData()){
				//check if user has org-membership
				resultCode = USResultCodes.USER_NOT_A_MEMBER.toString();
				UserDetailsDO usrDTL = userDetailsRepo.findOne(requestGrps.getUserId());
				if(usrDTL!=null && usrDTL.isMemberOfOrg(orgId)) {
					UserGroupsDO curGroups = userGroupsRepo.findOne(requestGrps.getUserId());
					if(curGroups!=null){
						//add newly added groups
						if(requestGrps.hasGrantAccesses()){
							result = true;
							for(String groupId : requestGrps.getGrantAccesses()){
								curGroups.addOrganizationGroups(orgId, groupId);	
							}
						}
						//remove groups
						if(requestGrps.hasRevokeAccesses()){
							result = true;
							OrganizationGroupsDO orgGrp = new OrganizationGroupsDO();
							orgGrp.setOrgId(orgId);
							for(String groupId : requestGrps.getRevokeAccesses()){
								orgGrp.addGroupId(groupId);	
							}
							curGroups.removeOrganizationGroups(orgGrp);
						}
						
						orgGroups = curGroups.getOrganizationGroups(orgId);

						if(result){
							curGroups = userGroupsRepo.save(curGroups);	
							resultCode = USResultCodes.USER_PROFILE_UPDATED.toString();
						}
					}
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(orgGroups);
		return response;
	}
	
	@Override
	public ServiceResponse removeUserFromOrg(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		String userId = null;
		UserGroupsDO currentGroups = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.UPDATE);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			UserDetailsBO user = request.getUser();
			userId = request.getRequestData(String.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(userId)) {
				currentGroups = userGroupsRepo.findOne(userId);
				String orgId = user.getCurrentOrgId();
				if(currentGroups!=null){
					currentGroups.removeOrganizationGroups(orgId);
					currentGroups.setModifiedDT(new Date());
					userGroupsRepo.save(currentGroups);
					result = true;
					resultCode = USResultCodes.USER_PROFILE_UPDATED.toString();
					//remove user-org membership	
					UserDetailsDO usrDTL = userDetailsRepo.findOne(userId);
					if(usrDTL!=null && usrDTL.isMemberOfOrg(orgId)){
						usrDTL.removeOrgId(orgId);
						usrDTL.setModifiedDT(new Date());
						userDetailsRepo.save(usrDTL);
					}
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(currentGroups);
		return response;
	}

	@Override
	public ServiceResponse readGroups(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		OrganizationGroupsDO orgGrps = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.READ);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			UserDetailsBO user = request.getUser();
			String orgId = user.getCurrentOrgId();
			String userId = request.getRequestData(String.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(userId) && StringUtils.isNotBlank(orgId)) {
				resultCode = USResultCodes.USER_NOT_A_MEMBER.toString();
				UserDetailsDO usrDTL = userDetailsRepo.findOne(userId);
				if(usrDTL!=null && usrDTL.isMemberOfOrg(orgId)) {
					resultCode = USResultCodes.USER_GROUPS_NOT_FOUND.getValue();
					UserGroupsDO currentGroups = userGroupsRepo.findOne(userId);
					if(currentGroups!=null){
						orgGrps = currentGroups.getOrganizationGroups(orgId);
						if(orgGrps==null){
							orgGrps = new OrganizationGroupsDO();
							orgGrps.setOrgId(orgId);
						}
						result = true;
						resultCode = USResultCodes.USER_GROUPS_FOUND.getValue();
					}
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(orgGrps);
		return response;
	}

	@Override
	public ServiceResponse inviteUserToOrg(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserGroupsBO reqGrp = request.getRequestData(UserGroupsBO.class);
		if(reqGrp!=null ) {
			String userId = reqGrp.getUserId();
			UserDetailsBO requester = request.getUser();
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(userId)){
				UserLoginDO loginDO = userLoginRepo.findOne(userId);
				UserDetailsDO userDTDO = userDetailsRepo.findOne(userId);
				String emailId = null;
				boolean verfPending = false;
				//in-active user	
				if(loginDO.isInactive()){
					resultCode = USResultCodes.USER_INACTIVATED.getValue();
				  //login verification is pending
				}else if(loginDO.isVerificationPending()){
					emailId = loginDO.getLocal().getEmailIds().iterator().next().getEmailId();
					result = true;
					verfPending = true;
					//requested email is not activated yet
				}else if(loginDO.isActiveUser() && StringUtils.isNotBlank(reqGrp.getEmailId()) && !loginDO.isActiveEmail(reqGrp.getEmailId())){
					emailId = loginDO.getLocal().getActiveEmail();
					result = true;
					//user is not a member and has an email
				}else if(loginDO.isActiveUser() && userDTDO != null  && userDTDO.getEmails().size() > 0){
					emailId = loginDO.getLocal().getActiveEmail();
					result = true;
					//user is already member of organization
				}else if(userDTDO != null || loginDO.isActiveEmail(reqGrp.getEmailId())){
					resultCode = USResultCodes.USER_ALREADY_MEMBER.getValue();
				}

				if(result){
					sendOrgInvite(userId,  emailId, requester.getUserId(), verfPending);	
					resultCode = CSResultCodes.OPERATION_SUCCESS.toString();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}


	/**
	 * User is not found in the system..
	 * - create login record with temporary password
	 * - create user details record
	 * - create user member group details
	 */
	@Override
	public ServiceResponse inviteNewUserToOrg(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		LoginBO lgnBO = request.getRequestData(LoginBO.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.toString();
		if(lgnBO != null && StringUtils.isNotBlank(lgnBO.getEmailId())) {
			String emailId = lgnBO.getEmailId();
			//find if user login exits for given email id
			UserLoginDO login = userLoginRepo.findByLocalEmailIdsEmailId(emailId);
			if(login!=null) {
					UserGroupsBO grpBO = new UserGroupsBO();
					grpBO.setUserId(login.getId());
					grpBO.setEmailId(emailId);
					request.addRequestData(grpBO);
					ServiceResponse sresp = inviteUserToOrg(request);
					result = sresp.getServiceResult().isResult();
					resultCode = sresp.getServiceResult().getResultCode();
			}else{
				//create login record with temporary password
				String tempPass = Encryptor.getInstance().getRandomPassword(10);
				UserLoginDO loginDO = UserLoginDOHelper.createNewLocalLogin(emailId, tempPass);
				loginDO.setStatus(LoginStatus.INACTIVE.getValue());
				loginDO.setPhone(lgnBO.getPhone());
				loginDO.setId(lgnBO.getUserId());
				loginDO.setComments(lgnBO.getComments());
				loginDO.setCountry(lgnBO.getCountry());
				loginDO.setJob(lgnBO.getJob());
				loginDO.setCompany(lgnBO.getCompany());
				loginDO.setFirstName(lgnBO.getFirstName());
				loginDO.setFunction(lgnBO.getFunction());
				loginDO.setLastName(lgnBO.getLastName());
				loginDO = userLoginRepo.save(loginDO);
				result = true;
				resultCode = CSResultCodes.OPERATION_SUCCESS.toString();
				sendOrgInviteNew(loginDO.getId(),  emailId);
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	/**
	 * Create Membership Request and send invitation email
	 */
	private void sendOrgInvite(String userId,  String emailId, String adminId, boolean tokenNeeded) {
		List<UserMembershipRequestDO> memReqList = memReqRepo.findByUserIdAndOrgId(userId, emailId);
		String token = null;
		if(memReqList.size() == 0){
			token = createNewMembershipReq(userId, emailId, adminId);
		}else{
			UserMembershipRequestDO memReq = memReqList.get(0);
			token = memReq.getVtoken();
		}
		if(tokenNeeded){
			msInvk.sendOrgMemReqMail(token, emailId, userId);
		}else{
			msInvk.sendOrgMemReqMail(emailId, userId);
		}
		
	}

	private String createNewMembershipReq(String userId, String orgId, String adminId) {
		String token = Encryptor.getInstance().getRandomString(15);
		UserMembershipRequestDO memReq = new UserMembershipRequestDO(orgId, userId, UserGroupsDO.GRP_MEMBER, adminId);
		memReq.setVtoken(token);
		memReq = memReqRepo.save(memReq);
		return token;
	}


	/**
	 * Create Membership Request and send invitation email
	 */
	private void sendOrgInviteNew(String userId,  String emailId) {
		String token = createNewMembershipReq(userId, "token", emailId);
		msInvk.sendOrgMemReqMail(token, emailId, userId) ;
	}
	
	public ServiceResponse searchOrgPeopleByGroup(ServiceRequest request){
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO> userList = null;
		if(request.getUser()!=null){
			UserDetailsBO user = request.getUser();
			String orgId = user.getCurrentOrgId();
			String groups = request.getRequestData(String.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(groups)){
				String grpNames[] = StringUtils.split(groups, ",");
				List<String> grpNamesLst = new ArrayList<String>();
				for(String grp:grpNames){
					grpNamesLst.add(grp);
				}
				userList = uops.getUsersByRole(orgId, grpNamesLst);
				result = true;
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}
}