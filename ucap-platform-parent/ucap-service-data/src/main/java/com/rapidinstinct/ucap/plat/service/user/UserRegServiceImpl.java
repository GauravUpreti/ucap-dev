package com.rapidinstinct.ucap.plat.service.user;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.UserLocalLoginBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.AccessAttributes;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.commons.util.Encryptor;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserGroupsDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginStatus;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO.Status;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO.Context;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO.ContextDataKey;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.OrganizationRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserLoginRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserMembershipRequestRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.VerificationOperations;
import com.rapidinstinct.ucap.plat.service.MailServiceHelper;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public class UserRegServiceImpl implements UserRegistrationService {

	private static Logger log = LoggerFactory.getLogger(UserRegServiceImpl.class);
	@Autowired private UserLoginRepository userLoginRepo;
	@Autowired private OrganizationRepository organizationRepo;
	@Autowired private UserGroupsRepository userGroupsRepo;
	@Autowired private VerificationOperations verOps;
	@Autowired private MailServiceHelper msInvk;
	@Autowired private UserDetailsRepository userDetailsRepo;
	@Autowired protected UserMembershipRequestRepository membershipRepo;

	protected UserLoginDO saveUser(UserLoginDO document){
		return userLoginRepo.save(document);
	}

	protected UserLoginDO findUserByEmailId(String emailId){
		UserLoginDO login = null;
		if(StringUtils.isNotBlank(emailId)){
			login = userLoginRepo.findByLocalEmailIdsEmailId(StringUtils.lowerCase(emailId));
		}
		return login;
	}

	@Override
	public ServiceResponse isEmailInUse(ServiceRequest request) {
		String emailId = request.getStringAttribute(AccessAttributes.EMAIL_ID.getValue());
		UserLoginDO login = findUserByEmailId(StringUtils.lowerCase(emailId));
		ServiceResponse response = null;
		if(login != null){
			response = new ServiceResponse(true, USResultCodes.DUPLICATE_EMAIL.getValue());
		}else{
			response = new ServiceResponse(false, USResultCodes.EMAIL_NOT_FOUND.getValue());
		}
		return response;
	}

	@Override
	public ServiceResponse registerUser(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		LoginBO localLogin = request.getRequestData(LoginBO.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(localLogin != null){
			UserLoginDO login = findUserByEmailId(localLogin.getEmailId());
			resultCode = USResultCodes.DUPLICATE_EMAIL.getValue();	
			if(login == null){			
				//create a new local login user
				UserLoginDO loginDO = UserLoginDOHelper.createNewLocalLogin(localLogin.getEmailId(), localLogin.getPassword());
				loginDO.setStatus(LoginStatus.VERIFICATION_PENDING.getValue());
				loginDO = saveUser(loginDO);
				localLogin.setUserId(loginDO.getId());
				localLogin.maskPassword();
				resultCode = USResultCodes.REGISTRATION_SUCCESS.getValue();	
				result = true;
				//send verification email
				String token = Encryptor.getInstance().getRandomString(15);
				VerificationDO vdo = new VerificationDO(loginDO.getId(), token, Context.USER_REGISTRATION.getValue(), loginDO.getId());
				vdo.addContextData(ContextDataKey.email.toString(), localLogin.getEmailId());
				verOps.add(vdo);
				msInvk.sendRegVerMail(token, localLogin.getEmailId());
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(localLogin);
		return response;
	}

	@Override
	public ServiceResponse recoverPassword(ServiceRequest request) {
		boolean result = true;
		String emailId = request.getRequestData(String.class);
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(StringUtils.isNotBlank(emailId)){
			UserLoginDO loginDO = findUserByEmailId(emailId);
			if(loginDO!=null && loginDO.isActiveUser()){
				String tempPass = Encryptor.getInstance().getRandomPassword(10);
				log.info("Temporary Password:" + tempPass);
				loginDO.getLocal().setPassword(tempPass);
				loginDO.getLocal().encryptPassword();
				userLoginRepo.save(loginDO);
				msInvk.sendTempPassMail(tempPass, emailId);
				resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	@Override
	public ServiceResponse verifyRegistration(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String verToken = request.getRequestData(String.class);
		UserLocalLoginBO ullb = null;
		if(StringUtils.isNotBlank(verToken)){
			VerificationDO vdo = verOps.findByToken(verToken);
			if(vdo != null && vdo.isValidToken() && StringUtils.isNotBlank(vdo.getContextRefId())){
				String emailId =  (String) vdo.getContextData(ContextDataKey.email.toString());
				UserLoginDO uldo = userLoginRepo.findOne(vdo.getContextRefId());
				resultCode = USResultCodes.USER_NOT_FOUND.getValue();
				if(uldo != null && uldo.isVerificationPending()){
					uldo.setStatus(LoginStatus.ACTIVE.getValue());
					result = uldo.getLocal().activateEmail(emailId);
					if(result){
						resultCode = USResultCodes.REGISTRATION_SUCCESS.getValue();
						userLoginRepo.save(uldo);
						verOps.delete(vdo);
						ullb = new UserLocalLoginBO(uldo.getId());
						ullb.setEmailId(emailId);
						UserDetailsDO ud = checkInvitation(ullb);
						createDefaultOrgAndAdmin(emailId, ud);
					}else{
						resultCode = USResultCodes.REGISTRATION_FAILED.getValue();
					}
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(ullb);
		return response;
	}

	//Check for invitation record and add member as part of organization
	private UserDetailsDO checkInvitation(UserLocalLoginBO ullb) {
		VerificationDO verInvite = verOps.findById(ullb.getEmailId());
		if(verInvite!=null && verInvite.isValidToken()){
			String orgId = verInvite.getContextRefId();
			verOps.delete(verInvite);

			//add user profile details with registered email id/invited org id
			UserDetailsDO ud = new UserDetailsDO(ullb.getUserId());
			ud.addOrgId(orgId);
			ud.addEmail(ullb.getEmailId());
			return ud;
		}
		return null;
	}

	
	private UserLocalLoginBO createDefaultOrgAndAdmin(String emailId, UserDetailsDO ud) {
		UserLocalLoginBO ullb = null;
		//add new default organization
		OrganizationDO orgDO = OrganizationDOHelper.createSandboxOrg(ud.getUserId(), emailId);
		orgDO = organizationRepo.save(orgDO);
		//add newly created user as admin of default organization
		UserGroupsDO userGroup = userGroupsRepo.findOne(ud.getUserId());
		if(userGroup==null){
			userGroup = UserGroupsDOHelper.createNew(ud.getUserId(), orgDO.getId(), UserGroupsDO.GRP_ADMIN);
		}else{
			userGroup.addOrganizationGroups(orgDO.getId(), UserGroupsDO.GRP_ADMIN);
		}
		userGroup.addOrganizationGroups(orgDO.getId(), UserGroupsDO.GRP_MEMBER);
		userGroupsRepo.save(userGroup);
		//add newly created org
		ud.addOrgId(orgDO.getId());
		userDetailsRepo.save(ud);
		return ullb;
	}


	@Override
	public ServiceResponse activateLogin(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String vtoken = request.getRequestData(String.class);
		if(StringUtils.isNotBlank(vtoken)){
			VerificationDO vdo = verOps.findByToken(vtoken);
			int vcontext = Context.INVITED_USER_ACTIVATE.getValue();
			if(vdo != null 
					&&  vdo.getContext() == vcontext 
					&& StringUtils.isNotBlank(vdo.getCreatedFor()) 
					&& StringUtils.isNotBlank(vdo.getContextRefId())) {
				String userId = vdo.getContextRefId();
				String emailId = vdo.getCreatedFor();
				//extract orgId 
				String orgId =  vdo.getStringContextData(ContextDataKey.orgId.toString());
				//activate login record
				UserLoginDO uldo = userLoginRepo.findOne(userId);
				resultCode = USResultCodes.USER_NOT_FOUND.getValue();
				if(uldo != null && uldo.isVerificationPending()){
					uldo.setStatus(LoginStatus.ACTIVE.getValue());
					result = uldo.getLocal().activateEmail(emailId);
					if(result){
						userLoginRepo.save(uldo);
						resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
						UserDetailsDO ud = userDetailsRepo.findOne(userId);
						//if user record does'nt exist create one
						if(ud == null){
							//create user details record
							ud = new UserDetailsDO(userId);
							ud.addOrgId(orgId);
							ud.addEmail(emailId);
							//default first name to user email id
							ud.setFirstName(emailId);
							userDetailsRepo.save(ud);
						}

						createDefaultOrgAndAdmin(emailId, ud);
						verOps.delete(vdo);
					}else{
						resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
					}
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}	

	@Override
	public ServiceResponse activateInvitedLogin(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		LoginBO loginReq = request.getRequestData(LoginBO.class);
		if(loginReq.isValidInviteRequest()){
			String userId = loginReq.getUserId();
			//activate login record
			UserLoginDO uldo = userLoginRepo.findOne(userId);
			resultCode = USResultCodes.USER_NOT_FOUND.getValue();
			if(uldo != null && uldo.isVerificationPending()){
				uldo.setStatus(LoginStatus.ACTIVE.getValue());
				result = uldo.getLocal().activateEmail();
				if(result){
					//first time login set the user selected password
					uldo.getLocal().setPassword(loginReq.getPassword());
					uldo.getLocal().encryptPassword();
					userLoginRepo.save(uldo);
					
					String emailId = uldo.getLocal().getEmailIds().iterator().next().getEmailId();
					UserDetailsDO ud = userDetailsRepo.findOne(userId);
					//if user record does'nt exist create one
					if(ud == null){
						//create user details record
						ud = new UserDetailsDO(userId);
						ud.addEmail(emailId);
						//default first name to user email id
						ud.setFirstName(emailId);
						userDetailsRepo.save(ud);
					}
					createDefaultOrgAndAdmin(emailId, ud);
					resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
				}
			}else{
				resultCode = USResultCodes.EMAIL_NOT_FOUND.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	@Override
	public ServiceResponse verifyOrgInvite(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		LoginBO loginReq = request.getRequestData(LoginBO.class);
		if(loginReq!=null && loginReq.hasEmailId() && loginReq.hasInviteToken()){
			String emailId = loginReq.getEmailId();
			String invToken = loginReq.getInviteToken();
			UserLoginDO loginDO = userLoginRepo.findByLocalEmailIdsEmailId(emailId);
			//activation pending, check the token value
			if(loginDO!=null && loginDO.isVerificationPending()){
				List<UserMembershipRequestDO> memRequests = membershipRepo.findByUserIdAndStatus(loginDO.getId(), Status.NEW.getValue());
				for(UserMembershipRequestDO memReq : memRequests){
					result = memReq.getVtoken().equals(invToken);
					if(result){
						resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
						break;
					}
				}
			//user is active check is not required	
			}else if(loginDO!=null && loginDO.isActiveUser()){
				result = true;	
				resultCode = USResultCodes.USER_ACTIVE.getValue();
			//user is inactive	
			}else if(loginDO!=null && loginDO.isInactive()){
				resultCode = USResultCodes.USER_INACTIVATED.getValue();
			//no login record found	
			}else if(loginDO==null){
				resultCode = USResultCodes.USER_NOT_FOUND.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}	
}