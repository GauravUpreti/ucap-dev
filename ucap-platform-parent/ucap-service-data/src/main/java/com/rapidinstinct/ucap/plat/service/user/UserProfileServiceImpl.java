package com.rapidinstinct.ucap.plat.service.user;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserPassChangeRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.UserProfileRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.commons.util.Encryptor;
import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO.OrgRoles;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO.Context;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserLoginRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserMembershipRequestRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.OrganizationOperations;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.VerificationOperations;
import com.rapidinstinct.ucap.plat.service.MailServiceHelper;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class UserProfileServiceImpl implements UserProfileService {

	@Autowired private UserLoginRepository userLoginRepo;
	@Autowired private MongoOperations operations;
	@Autowired private VerificationOperations verOps;
	@Autowired private MailServiceHelper msInvk;

	@Autowired protected UserDetailsRepository userDetailsRepo;
	@Autowired protected OrganizationOperations orgOps;
	@Autowired protected UserGroupsRepository userGroupsRepo;
	@Autowired protected UserMembershipRequestRepository membershipRepo;

	@Value("${component.list.size.limit}") private int listSize;

	@Override
	public ServiceResponse updateUserProfileDetails(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserDetailsDO updtDetails = null;
		UserDetailsBO curUser = request.getUser();
		UserProfileRequestBO profile = request.getRequestData(UserProfileRequestBO.class);
		if(curUser!=null && profile!=null){
			profile.setUserId(curUser.getUserId());
			UserDetailsDO detailsDO = userDetailsRepo.findOne(curUser.getUserId());
			if (detailsDO != null){
				BeanUtils.copyProperties(profile, detailsDO);
				detailsDO.setModifiedDT(new Date());
			}else{
				detailsDO = new UserDetailsDO(curUser.getUserId());
				BeanUtils.copyProperties(profile, detailsDO);
			}
			updtDetails = userDetailsRepo.save(detailsDO);
			result = true;
			resultCode = USResultCodes.USER_PROFILE_UPDATED.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(updtDetails);
		return response;
	}

	@Override
	public ServiceResponse getUserProfileDetails(ServiceRequest request) {
		UserDetailsBO userDetails = request.getUser();
		UserDetailsDO profile = null;
		boolean result = false;
		String resultCode = USResultCodes.USER_NOT_FOUND.getValue();;
		if(userDetails != null){
			result = true;
			resultCode = USResultCodes.USER_DETAILS_FOUND.getValue();
			profile = userDetailsRepo.findOne(userDetails.getUserId());
			if(profile==null){
				profile = new UserDetailsDO(userDetails.getUserId());
			}	
			//add organization-membership details
			UserGroupsDO userGroups = userGroupsRepo.findOne(userDetails.getUserId());
			if(userGroups != null){
	        	for(OrganizationGroupsDO orgGroup: userGroups.getOrgGroups()){
	        		OrganizationDO org = orgOps.findById(orgGroup.getOrgId());
	        		String orgName = org != null ? org.getName() : StringUtils.EMPTY;
	        		OrgRoles orgRoles = new OrgRoles(orgGroup.getOrgId(), orgName);
	        		for(String groupName : orgGroup.getGrpIds()){
	        			Role role = Role.getType(groupName);
	        			if(role != null){
	        				orgRoles.addRole(role);
	        			}
	        		}
	        		profile.addOrgInfo(orgRoles);
	        	}
			}
		}		

		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(profile);
		return response;
	}

	//TODO - Invalidate Token
	@Override
	public ServiceResponse changePassword(ServiceRequest request) {
		boolean result = false;
		UserDetailsBO curUser = request.getUser();
		UserPassChangeRequestBO profile = request.getRequestData(UserPassChangeRequestBO.class);
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser!=null && profile!=null){
			UserLoginDO login = userLoginRepo.findOne(curUser.getUserId());
			resultCode = USResultCodes.USER_NOT_FOUND.getValue();
			if(login!=null){
				resultCode = USResultCodes.INVALID_CREDENTIAL.getValue();
				if(login.getLocal().comparePassword(profile.getCurrentPassword())){
					login.getLocal().setPassword(profile.getNewPassword());
					login.getLocal().encryptPassword();
					userLoginRepo.save(login);
					result = true;
					resultCode = USResultCodes.USER_PROFILE_UPDATED.getValue();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(curUser);
		return response;
	}

	




	@Override
	public ServiceResponse searchOrgUsers(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO>  userList = null;
		UserDetailsBO user = request.getUser();
		String searchTerm = request.getRequestData(String.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(StringUtils.isNotBlank(searchTerm)){
			String orgId = user.getCurrentOrgId();
			String queryStr = "{"
					+ "$or:["
							+ "{firstName: {$regex: '" + searchTerm +  "', $options:'i'}}, "
							+ "{lastName: {$regex: '" + searchTerm + "', $options:'i'}}"
						+ "], "
					+ "orgIds : '"+ orgId + "'}";
			Sort sortOrder = new Sort(Direction.ASC, DocumentField.FIRSTNAME.toString());
			Query query = new BasicQuery(queryStr);
			query.limit(listSize);	
			query.with(sortOrder);
			userList = operations.find(query, UserDetailsDO.class);
			result = true;
			resultCode = USResultCodes.USER_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}

	
	/**
	 * Check for existing email. 
	 * If email is new add email id and send verification email to added email
	 * 
	 * TODO - send email to primary email and other emails - should be preference driven
	 */
	@Override
	public ServiceResponse addEmail(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		String emailId = request.getRequestData(String.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser!=null && StringUtils.isNotBlank(emailId)){
			emailId = StringUtils.lowerCase(emailId);
			UserLoginDO login = userLoginRepo.findByLocalEmailIdsEmailId(emailId);
			resultCode = USResultCodes.DUPLICATE_EMAIL.getValue();
			VerificationDO pvdo = verOps.findContextRefAndContext(emailId, Context.ADD_EMAIL.getValue());
			if(login == null && pvdo == null){
				UserDetailsDO userDt = userDetailsRepo.findOne(curUser.getUserId());
				if(userDt != null && !userDt.isExistingEmail(emailId)){
					userDt.addVpEmail(emailId);
					userDt.setModifiedDT(new Date());
					userDetailsRepo.save(userDt);
					//send verification email
					String token = Encryptor.getInstance().getRandomString(15);
					VerificationDO vdo = new VerificationDO(curUser.getUserId(), token, Context.ADD_EMAIL.getValue(), emailId);
					verOps.add(vdo);
					msInvk.sendEmailVerification(token, emailId, curUser.getFirstName());
					result = true;
					resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	/**
	 * if email is found remove from login and details record.
	 * if only one email exists, don't remove email
	 */
	@Override
	public ServiceResponse removeEmail(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		String emailId = request.getRequestData(String.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser!=null && StringUtils.isNotBlank(emailId)){
			emailId = StringUtils.lowerCase(emailId);
			UserDetailsDO userDt = userDetailsRepo.findOne(curUser.getUserId());
			//pending email id
			if(userDt != null && userDt.isVpEmail(emailId)){
				userDt.removeVpEmail(emailId);
				userDt.setModifiedDT(new Date());
				userDetailsRepo.save(userDt);
				result = true;
				resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
			//active email id	
			}else if(userDt != null && userDt.isActiveEmail(emailId) &&  userDt.getActiveEmailsCount() > 1){
				userDt.removeEmail(emailId);
				userDt.setModifiedDT(new Date());
				userDetailsRepo.save(userDt);

				UserLoginDO login = userLoginRepo.findOne(userDt.getUserId());
				if(login.getLocal().removeEmail(emailId)){
					userLoginRepo.save(login);
				}
				result = true;
				resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	/**
	 * Verify email is in pending list, 
	 * find verification record and add to user login and user details record, 
	 * remove email from pending list
	 */
	@Override
	public ServiceResponse verifyEmail(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		String vtoken = request.getRequestData(String.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser!=null && StringUtils.isNotBlank(vtoken)) {
			UserDetailsDO userDt = userDetailsRepo.findOne(curUser.getUserId());
			VerificationDO vdo = verOps.findByToken(vtoken);
			if(userDt != null && vdo != null){
				String emailId = vdo.getContextRefId();
				if(userDt.isVpEmail(emailId)){
					//add email to user details record
					userDt.removeVpEmail(emailId);
					userDt.addEmail(emailId);
					userDt.setModifiedDT(new Date());
					userDetailsRepo.save(userDt);
					
					//add email to user login email
					UserLoginDO login = userLoginRepo.findOne(userDt.getUserId());
					login.getLocal().addEmail(emailId);
					userLoginRepo.save(login);
					
					//delete verification record
					verOps.delete(vdo);
					
					result = true;
					resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	/**
	 * find email existence in verification pending list
	 * find corresponding verification record and send email
	 */
	@Override
	public ServiceResponse resendVerifyEmail(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		String emailId = request.getRequestData(String.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser!=null && StringUtils.isNotBlank(emailId)){
			//convert email to lower case
			emailId = StringUtils.lowerCase(emailId);
			UserDetailsDO userDt = userDetailsRepo.findOne(curUser.getUserId());
			if(userDt != null && userDt.isVpEmail(emailId)){
				VerificationDO vdo = verOps.findByUserContextRef(userDt.getUserId(), emailId);
				if(vdo != null){
					msInvk.sendEmailVerification(vdo.getToken(), emailId, curUser.getFirstName());
					result = true;
					resultCode = CSResultCodes.OPERATION_SUCCESS.getValue();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

/*	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponse addOrUpdateOrgEmails(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserPreferenceDO prefData = null;
		UserDetailsBO curUser = request.getUser();
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<OrgPreference> orgPrefs = request.getRequestData(List.class);
		if(curUser!=null && orgPrefs!=null && orgPrefs.size() > 0){
			prefData = userPrefRepo.findOne(curUser.getUserId());	
			if(prefData==null){
				prefData = new UserPreferenceDO();
				prefData.setUserId(curUser.getUserId());
				prefData.setCreatedDT(new Date());
			}
			prefData.setModifiedDT(new Date());
			for(OrgPreference pref : orgPrefs){
				prefData.addOrgPreference(pref.getOrgId(), pref.getEmailId());
			}
			userPrefRepo.save(prefData);
			result = true;
			resultCode = USResultCodes.USER_DETAILS_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(prefData);
		return response;
	}*/

/*	@Override
	public ServiceResponse getUserPreference(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserPreferenceDO prefData = null;
		UserDetailsBO curUser = request.getUser();
		resultCode = CSResultCodes.USER_ATTRIBUTES_NF.getValue();
		if(curUser!=null){
			prefData = userPrefRepo.findOne(curUser.getUserId());	
			if(prefData == null){
				prefData = new UserPreferenceDO();
				prefData.setUserId(curUser.getUserId());
			}
			result = true;
			resultCode = USResultCodes.USER_DETAILS_FOUND.getValue();
		}
		
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(prefData);
		return response;
	}*/
}