package com.rapidinstinct.ucap.plat.service.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.cbo.CSConstants;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.cbo.RBMConstants;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.GroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.RBMComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.RBMCommonOperations;
import com.rapidinstinct.ucap.plat.service.component.NotificationService;
import com.rapidinstinct.ucap.plat.service.component.RBMService;

/**
 * 
 * @author Shamim Ahmad
 * 
 */

@Service
public class RBMServiceImpl implements RBMService {

	@Autowired	private RBMCommonOperations ops;
	@Autowired	private GridFsOperations operations;
	@Autowired	private NotificationService notifCS;
	
	@Override
	public ServiceResponse readUserStudy(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<StudyDO> studyDO = new ArrayList<StudyDO>();
		UserAccessDO userStudy = null;
		List<String> study = null;
		String userId = request.getUser().getUserId();
		try {
			userStudy = (UserAccessDO) ops.findByUserId(userId);
			study = userStudy.getStudyName();
			if (userStudy != null && study != null) {
				for (int i = 0; i < study.size(); i++) {
					studyDO.addAll(ops.findByStudyId(study.get(i)));
				}
				for(int j=0;j<studyDO.size();j++){
					studyDO.get(j).setPendingNotifCount(ops.findPendingNotification(studyDO.get(j).getStudyName(), RBMConstants.StudyNotificationStatusCodes.PENDING.getValue()));
				}
				result = true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(studyDO);
		return response;
	}
		
	@Override
	public ServiceResponse readThresholds(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<RBMComponentDO> thresholdsDO = null;
		String study = request.getRequestData(String.class);
		try {
			if (study != null) {
				thresholdsDO = ops.read(study);
				result = true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		} catch (Exception e) {
			result = false;
			resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(thresholdsDO);
		return response;
	}

	@Override
	public ServiceResponse addRisk(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.DOCUMENT_SAVE_FAILED.getValue();
		RBMComponentDO thresholdsDO = request.getRequestData(RBMComponentDO.class);
		if(thresholdsDO!=null){
			ops.add(thresholdsDO);
			result=true;
			resultCode=CSConstants.CSResultCodes.DOCUMENT_SAVED.getValue();
		}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(thresholdsDO);
		return response;
	}

	@Override
	public ServiceResponse updateRisk(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.DOCUMENT_NOT_FOUND.getValue();
		RBMComponentDO thresholdsDO = request.getRequestData(RBMComponentDO.class);
		RBMComponentDO thresholds = null;
		if (thresholdsDO != null && StringUtils.isNotBlank(thresholdsDO.getId())) {
			thresholds = ops.findById(thresholdsDO.getId());
			thresholds.setuModRisk(thresholdsDO.getuModRisk());
			thresholds.setuSevRisk(thresholdsDO.getuSevRisk());
			thresholds.setuLB(thresholdsDO.getuLB());
			thresholds.setWgtORI(thresholdsDO.getWgtORI());
			thresholds.setStatus(thresholdsDO.getStatus());
			thresholds = ops.update(thresholds);
			result = true;
			resultCode=CSConstants.CSResultCodes.DOCUMENT_SAVED.getValue();
		}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(thresholds);
		return response;
	}

	@Override
	public ServiceResponse search(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.DOCUMENT_FOUND.getValue();
		RBMComponentDO thresholdsDO = request.getRequestData(RBMComponentDO.class);
		RBMComponentDO rbmList = null;
		String group = thresholdsDO.getGrpKRI();
		String kri = thresholdsDO.getKri();
		if (group != null && kri != null) {
			rbmList = ops.findbygrpKRIandkri(group, kri);
			result = true;
		} else {
			result = false;
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(rbmList);
		return response;
	}

	@Override
	public ServiceResponse readGroups(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID
				.getValue();
		List<GroupsDO> groups = new ArrayList<GroupsDO>();
		String userId = request.getUser().getUserId();
		if(userId!=null)
		{
			groups=ops.findAllGroups();
			result=true;
			resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
		}

		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(groups);
		return response;
	}
	
	@Override
	public ServiceResponse getKRICountByStudyName(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String studyName = request.getRequestData(String.class);
		String userId = request.getUser().getUserId();
		int kriCount = 0;
		try{
			if(StringUtils.isNotBlank(userId) && StringUtils.isNotBlank(studyName)){
				kriCount = ops.countKriByStudyName(studyName);
				result=true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		}catch(Exception e){
			result = false;
			resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(kriCount);
		return response;
	}

	@Override
	public ServiceResponse getEnrolledCountByStudyName(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String studyName = request.getRequestData(String.class);
		String userId = request.getUser().getUserId();
		int enrolledCount = 0;
			try{
				if(StringUtils.isNotBlank(userId) && StringUtils.isNotBlank(studyName)){
					enrolledCount = ops.countEnrolledByStudyName(studyName);
					result=true;
					resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
				}
			}catch(Exception e){
				result = false;
				resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();	
			}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(enrolledCount);
		return response;
	}
	
	@Override
	public ServiceResponse getSiteCountByStudyName(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String studyName = request.getRequestData(String.class);
		String userId = request.getUser().getUserId();
		int enrolledCount = 0;
			try{
				if(StringUtils.isNotBlank(userId) && StringUtils.isNotBlank(studyName)){
					enrolledCount = ops.countSiteByStudyName(studyName);
					result=true;
					resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
				}
			}catch(Exception e){
				result = false;
				resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
			}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(enrolledCount);
		return response;
	}	
	
	@Override
	public ServiceResponse add(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse delete(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse update(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse read(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
	
}