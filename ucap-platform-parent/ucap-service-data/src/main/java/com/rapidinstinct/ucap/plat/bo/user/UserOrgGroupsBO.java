package com.rapidinstinct.ucap.plat.bo.user;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;

/**
 * @author Gaurav Upreti
 *
 */
public class UserOrgGroupsBO extends BaseBusinessObject {
	private static final long serialVersionUID = 1L;
	
	private String userId;
	private List<OrgGroup> orgGroups;
	
	public UserOrgGroupsBO() { }
	
	public UserOrgGroupsBO(String userId) { 
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<OrgGroup> getOrgGroups() {
		return orgGroups;
	}

	public void setOrgGroups(List<OrgGroup> orgGroups) {
		this.orgGroups = orgGroups;
	}

	public static class OrgGroup {
		
		private OrgDetailsBO organization;
		private Set<Group> groups = null;
		private int status;
		private String vtoken;
		
		public OrgGroup() { }

		public OrgDetailsBO getOrganization() {
			return organization;
		}

		public void setOrganization(OrgDetailsBO organization) {
			this.organization = organization;
		}

		public Set<Group> getGroups() {
			return groups;
		}

		public void setGroups(Set<Group> groups) {
			this.groups = groups;
		}
		
		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getVtoken() {
			return vtoken;
		}

		public void setVtoken(String vtoken) {
			this.vtoken = vtoken;
		}

		public static Set<Group> from(Set<String> grpIds){
			Set<Group> groups = new HashSet<Group>();
    		for(String groupName : grpIds){
    			Role role = Role.getType(groupName);
    			if(role != null){
    				Group grp = new Group(groupName, role.toString(), UserMembershipRequestDO.Status.ACCEPTED.getValue());
    				groups.add(grp);
    			}
    		}
    		return groups;
		}

		public static Set<Group> from(String groupName, int status){
			Set<Group> groups = new HashSet<Group>();
			Role role = Role.getType(groupName);
			if(role != null){
				Group grp = new Group(groupName, role.getValue(), status);
				groups.add(grp);
			}
    		return groups;
		}

	}
	
	
	public static class Group {
		private String code;
		private String name;
		private int status;
		
		public Group(){ }
		
		public Group(String code,String name, int status){
			this.code = code;
			this.name = name;
			this.status = status;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}
	}
}
