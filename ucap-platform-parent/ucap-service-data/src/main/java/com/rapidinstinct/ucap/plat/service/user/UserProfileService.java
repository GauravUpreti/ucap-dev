package com.rapidinstinct.ucap.plat.service.user;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public interface UserProfileService {

	public ServiceResponse getUserProfileDetails(ServiceRequest request);
	
	public ServiceResponse updateUserProfileDetails(ServiceRequest request);
	
	public ServiceResponse changePassword(ServiceRequest request);
	
	public ServiceResponse getUserWorkDetails(ServiceRequest request);
	
	public ServiceResponse updateUserWorkDetails(ServiceRequest request);
	
	public ServiceResponse getUserOrgDetails(ServiceRequest request);
	
	public ServiceResponse searchOrgUsers(ServiceRequest request);
	
	public ServiceResponse addEmail(ServiceRequest request);
	
	public ServiceResponse removeEmail(ServiceRequest request);
	
	public ServiceResponse verifyEmail(ServiceRequest request);
	
	public ServiceResponse resendVerifyEmail(ServiceRequest request);
	
	public ServiceResponse addOrUpdateOrgEmails(ServiceRequest request);
	
	public ServiceResponse getUserPreference(ServiceRequest request);
	
	public ServiceResponse acceptOrgInvite(ServiceRequest request);
	
	public ServiceResponse rejectOrgInvite(ServiceRequest request);

}
