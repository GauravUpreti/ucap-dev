package com.rapidinstinct.ucap.plat.bo.user;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.PeopleSkillProfileDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;

/**
 * @author Gaurav Upreti
 *
 */

public class UserSummaryBO extends BaseBusinessObject {

	private static final long serialVersionUID = 1L;
	private UserDetailsDO profile;
	private PeopleSkillProfileDO skillProfile;
	private OrganizationGroupsDO orgMemberships;
	private UserMembershipRequestDO orgRequest;
	
	public UserDetailsDO getProfile() {
		return profile;
	}
	
	public void setProfile(UserDetailsDO profile) {
		this.profile = profile;
	}
	
	public PeopleSkillProfileDO getSkillProfile() {
		return skillProfile;
	}
	
	public void setSkillProfile(PeopleSkillProfileDO skillProfile) {
		this.skillProfile = skillProfile;
	}
	
	public OrganizationGroupsDO getOrgMemberships() {
		return orgMemberships;
	}

	public void setOrgMemberships(OrganizationGroupsDO orgMemberships) {
		this.orgMemberships = orgMemberships;
	}

	public UserMembershipRequestDO getOrgRequest() {
		return orgRequest;
	}

	public void setOrgRequest(UserMembershipRequestDO orgRequest) {
		this.orgRequest = orgRequest;
	}
}
