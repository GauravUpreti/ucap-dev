package com.rapidinstinct.ucap.plat.bo.user;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Gaurav Upreti
 *
 */
public class UserGroupRequestBO {
	public String userId;
	public List<String> grantAccesses;
	public List<String> revokeAccesses;

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public List<String> getGrantAccesses() {
		return grantAccesses;
	}
	
	public void setGrantAccesses(List<String> grantAccesses) {
		this.grantAccesses = grantAccesses;
	}
	
	public List<String> getRevokeAccesses() {
		return revokeAccesses;
	}

	public void setRevokeAccesses(List<String> revokeAccesses) {
		this.revokeAccesses = revokeAccesses;
	}
	
	public boolean hasGrantAccesses(){
		return (this.grantAccesses !=null && this.grantAccesses.size() > 0);
	}
	
	public boolean hasRevokeAccesses(){
		return (this.revokeAccesses !=null && this.revokeAccesses.size() > 0);
	}

	public boolean isValidData(){
		return (StringUtils.isNotBlank(this.userId) 
				&& ((this.grantAccesses !=null && this.grantAccesses.size() > 0) 
						|| (this.revokeAccesses !=null && this.revokeAccesses.size() > 0))
				);

	}
}
