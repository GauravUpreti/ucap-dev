package com.rapidinstinct.ucap.plat.bo.user;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;

/**
 * @author Gaurav Upreti
 *
 */
public class UserPassChangeRequestBO extends BaseBusinessObject{
	private static final long serialVersionUID = 1L;

	private String currentPassword;
	private String newPassword;
	
	private UserPassChangeRequestBO(){ }
	
	public UserPassChangeRequestBO(String currentPassword, String newPassword) { 
		this();
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
}
