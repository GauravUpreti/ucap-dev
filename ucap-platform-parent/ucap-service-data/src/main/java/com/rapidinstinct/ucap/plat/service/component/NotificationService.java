package com.rapidinstinct.ucap.plat.service.component;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * @author Gaurav Upreti
 *
 */

@Service
public interface NotificationService {

	public ServiceResponse updateNotification(ServiceRequest sreq);

	public ServiceResponse readAllNotifications(ServiceRequest sreq);

	public ServiceResponse readNotifications(ServiceRequest sreq);
	
	public ServiceResponse countPendingNotifications(ServiceRequest sreq);

}
