package com.rapidinstinct.ucap.plat.cbo;

/**
 *
 * @author Shamim Ahmad
 *
 */
public class RBMComponentBO extends BaseDocumentBO {
	private static final long serialVersionUID = 1L;

	private String grpKRI;
	private String kri;
	private String kriCode;	
	private float sModRisk;
	private float sSevRisk;
	private float sLB;
	private float uModRisk;
	private float uSevRisk;
	private float uLB;
	private float wgtORI;
	private int status;
	private String study;
	
	public String getGrpKRI() {
		return grpKRI;
	}
	public void setGrpKRI(String grpKRI) {
		this.grpKRI = grpKRI;
	}
	public String getKri() {
		return kri;
	}
	public void setKri(String kri) {
		this.kri = kri;
	}
	public String getKriCode() {
		return kriCode;
	}
	public void setKriCode(String kriCode) {
		this.kriCode = kriCode;
	}
	public float getsModRisk() {
		return sModRisk;
	}
	public void setsModRisk(float sModRisk) {
		this.sModRisk = sModRisk;
	}
	public float getsSevRisk() {
		return sSevRisk;
	}
	public void setsSevRisk(float sSevRisk) {
		this.sSevRisk = sSevRisk;
	}
	public float getsLB() {
		return sLB;
	}
	public void setsLB(float sLB) {
		this.sLB = sLB;
	}
	public float getuModRisk() {
		return uModRisk;
	}
	public void setuModRisk(float uModRisk) {
		this.uModRisk = uModRisk;
	}
	public float getuSevRisk() {
		return uSevRisk;
	}
	public void setuSevRisk(float uSevRisk) {
		this.uSevRisk = uSevRisk;
	}
	public float getuLB() {
		return uLB;
	}
	public void setuLB(float uLB) {
		this.uLB = uLB;
	}
	public float getWgtORI() {
		return wgtORI;
	}
	public void setWgtORI(float wgtORI) {
		this.wgtORI = wgtORI;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStudy() {
		return study;
	}
	public void setStudy(String study) {
		this.study = study;
	}
	
}