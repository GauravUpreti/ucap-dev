package com.rapidinstinct.ucap.plat.service.resource;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.service.CrudService;

/**
 * @author Gaurav Upreti
 *
 */

@Service
public interface OrgSkillService extends CrudService {

	public ServiceResponse getSkills(ServiceRequest request);

	public ServiceResponse searchSkills(ServiceRequest request);

}
