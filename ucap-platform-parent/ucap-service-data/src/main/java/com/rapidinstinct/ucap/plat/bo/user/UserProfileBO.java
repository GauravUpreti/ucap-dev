package com.rapidinstinct.ucap.plat.bo.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.bo.SerializableObject;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserProfileBO extends BaseBusinessObject {
	
	private static final long serialVersionUID = 1L;

	private String userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String middleName;
	private String profileImgURL;
	private List<OrgRolesBO> orgRoles = new ArrayList<OrgRolesBO>();

	public UserProfileBO(){	}

	public UserProfileBO(String userId, String userName){
		this.userId = userId;
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public String getProfileImgURL() {
		return profileImgURL;
	}
	
	public void setProfileImgURL(String profileImgURL) {
		this.profileImgURL = profileImgURL;
	}
		
	public void userNameToLower(){
		this.userName = StringUtils.lowerCase(this.userName);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String email) {
		this.userName = email;
	}
	
	public List<OrgRolesBO> getOrgRoles() {
		return orgRoles;
	}
	
	public void addOrgRoles(OrgRolesBO org){
		orgRoles.add(org);
	}
	
	public static class OrgRolesBO extends SerializableObject{

		private static final long serialVersionUID = 1L;
		private String orgId;
		private String name;
	    private List<Role> roles = new ArrayList<Role>();

	    public OrgRolesBO(String orgId, String name){
			this.orgId = orgId;
			this.name = name;
		}
		
		public String getOrgId() {
			return orgId;
		}
		
		public String getName() {
			return name;
		}

		public List<Role> getGrpIds() {
			return roles;
		}

		public void setGrpIds(List<Role> roles) {
			this.roles = roles;
		}
		
	    public boolean hasRole(Role role) {
	        return (this.roles.contains(role));
	    }
		
	    public void addRole(Role role){
			if(role!=null){
				if(!hasRole(role)){
					this.roles.add(role);	
				}
			}
		}
	    
	}
}
