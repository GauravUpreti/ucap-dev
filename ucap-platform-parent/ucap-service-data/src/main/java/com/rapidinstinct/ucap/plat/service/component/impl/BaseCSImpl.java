package com.rapidinstinct.ucap.plat.service.component.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.UserOperations;
import com.rapidinstinct.ucap.plat.service.component.ComponentService;

/**
 * 
 * @author Gaurav Upreti
 *	
 */

public abstract class BaseCSImpl implements ComponentService {
	@Autowired  protected UserGroupsRepository userGroupRepo;
	@Autowired  protected UserDetailsRepository userDtlRepo;
	@Autowired  protected UserOperations uops;
	
	@Value("${component.list.size.limit}") private int listSize;




	public boolean isUserIsAdmin(String userId, String orgId){
		UserGroupsDO grps = userGroupRepo.findOne(userId);
		if(grps != null){
			OrganizationGroupsDO grp = grps.getOrganizationGroups(orgId);
			return (grp!=null && grp.isAdmin());
		}
		return false;
	}
	
	public boolean isUserIsKnowledgeManager(String userId, String orgId){
		UserGroupsDO grps = userGroupRepo.findOne(userId);
		if(grps != null){
			OrganizationGroupsDO grp = grps.getOrganizationGroups(orgId);
			return (grp!=null && (grp.isAdmin() || grp.isKnowlegeManager()));
		}
		return false;
	}

	public boolean isUserIsResourceManager(String userId, String orgId){
		UserGroupsDO grps = userGroupRepo.findOne(userId);
		if(grps != null){
			OrganizationGroupsDO grp = grps.getOrganizationGroups(orgId);
			return (grp!=null && (grp.isAdmin() || grp.isResourceManager()));
		}
		return false;
	}

	public boolean isUserMemberOfOrg(String userId, String orgId){
		UserDetailsDO user = userDtlRepo.findOne(userId);
		return (user!=null && user.isMemberOfOrg(orgId));
	}

	public int getListSize() {
		return listSize;
	}
	
	@Override
	public ServiceResponse delete(ServiceRequest request) {
		return createOpDeniedResp();
	}

	public  ServiceResponse addCollaborator(ServiceRequest request){
		return createOpDeniedResp();
	}
	
	public  ServiceResponse removeCollaborator(ServiceRequest request){
		return createOpDeniedResp();
	}

	public  ServiceResponse listContributor(ServiceRequest request){
		return createOpDeniedResp();
	}
	
	public  ServiceResponse searchContributor(ServiceRequest request){
		return createOpDeniedResp();
	}
	
	public  ServiceResponse deleteAttachments(ServiceRequest request){
		return createOpDeniedResp();
	}
	
	public  ServiceResponse readAttachments(ServiceRequest request){
		return createOpDeniedResp();
	}

	public  ServiceResponse changeOwner(ServiceRequest request){
		return createOpDeniedResp();
	}

	protected ServiceResponse createOpDeniedResp() {
		return new ServiceResponse(false, CSResultCodes.DOCUMENT_OPERATION_DENIED.getValue());
	}
	
	/*public void addOwnerAndCollabDetails(BaseComponentBO bcbo){
		Set<String> userIds = new HashSet<String>();
		userIds.add(bcbo.getOwner());
		userIds.add(bcbo.getCreator());
		if(bcbo.getCollaborators()!=null){
			userIds.addAll(bcbo.getCollaborators().getMembers());
		}
		List<UserDetailsDO> users  =  uops.getUsers(userIds);
		for(UserDetailsDO user:users){
			bcbo.addUser(user);
		}
	}
	
	public void addOwnerAndCollabDetails(BaseDocument baseDoc){
		Set<String> userIds = new HashSet<String>();
		userIds.add(baseDoc.getOwner());
		userIds.add(baseDoc.getCreator());
		if(baseDoc.getCollaborators()!=null){
			userIds.addAll(baseDoc.getCollaborators().getMembers());
		}
		List<UserDetailsDO> users  =  uops.getUsers(userIds);
		for(UserDetailsDO user:users){
			baseDoc.addUser(user);
		}
	}

	public void addOwnerDetails(BaseDocument baseDoc){
		UserDetailsDO user  =  uops.getUser(baseDoc.getOwner());
		baseDoc.addUser(user);
	}*/
}
