package com.rapidinstinct.ucap.plat.service.helper;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;

/**
 * 
 * @author Gaurav Upreti
 * 
 */

public class AuthorizationHelper {

	public static AuthorizationResult checkAuthroization(ServiceRequest request, KAResourceOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case ADD :
				case UPDATE :
				case READ :
				case PUBLISH :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgAdmin() || user.isOrgKM()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	public static AuthorizationResult checkAuthroization(ServiceRequest request, RAResourceOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case ADD :
				case UPDATE :
				case READ :
				case PUBLISH :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgAdmin() || user.isOrgResourceManager()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	public static AuthorizationResult checkAuthroization(ServiceRequest request, OrganizationResourceOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case ADD :
				case UPDATE :
				case PUBLISH :
					if(user.isOrgAdmin()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case READ :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgAdmin() || user.isOrgResourceManager() || user.isOrgMember() || user.isOrgResourceManager()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	public static AuthorizationResult checkAuthroization(ServiceRequest request, ProjectConceptOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case ADD :
				case UPDATE :
				case READ :
				case SUBMIT :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgMember()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	
	public static AuthorizationResult checkAuthroization(ServiceRequest request, ProjectInitiationOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case ADD :
				case UPDATE :
				case READ :
				case SUBMIT :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgMember()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	public static AuthorizationResult checkAuthroization(ServiceRequest request, ProjectPlanOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case ADD :
				case UPDATE :
				case READ :
				case SUBMIT :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgMember()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	public static AuthorizationResult checkAuthroization(ServiceRequest request, ProjectExecuteOperation operation) {
		AuthorizationResult result = new AuthorizationResult();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			switch(operation){
				case READ_ALL:
				case SEARCH :
				case READ_PAGINATE :
				case FAVORITE:
				case ADD :
				case UPDATE :
				case READ :
				case SUBMIT :
				case TAG :	
				case LIKE_DISLIKE :	
				case COMMENT :
				case TODO :	
					if(user.isOrgMember()){
						result.setAccessible(true);
					}else{
						result.setAccessible(false);
						result.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());	
					}
					break;
					
				default:
					result.setAccessible(false);
					result.setResultCode(CSResultCodes.USER_ATTRIBUTES_NF.getValue());
				}
		}
		return result;
	}

	public static enum KAResourceOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), PUBLISH(256), 
		TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096)
		;

		private KAResourceOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
		public static KAResourceOperation getType(int value) {
			KAResourceOperation types[] = KAResourceOperation.values();
			for (KAResourceOperation type : types) {
				if (type.getValue() == value) {
					return type;
				}
			}
			return null;
		}
	}

	public static enum RAResourceOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), PUBLISH(256), TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096)
		;

		private RAResourceOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
		public static RAResourceOperation getType(int value) {
			RAResourceOperation types[] = RAResourceOperation.values();
			for (RAResourceOperation type : types) {
				if (type.getValue() == value) {
					return type;
				}
			}
			return null;
		}
	}

	public static enum OrganizationResourceOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), PUBLISH(256), 
		TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096), MANAGE_MANAGERS(8192)
		;

		private OrganizationResourceOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
	}

	public static enum ProjectConceptOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), SUBMIT(256), 
		TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096), MANAGE_MANAGERS(8192)
		;

		private ProjectConceptOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
	}

	public static enum ProjectInitiationOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), SUBMIT(256), 
		TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096), MANAGE_MANAGERS(8192)
		;

		private ProjectInitiationOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
	}


	public static enum ProjectPlanOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), SUBMIT(256), 
		TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096), MANAGE_MANAGERS(8192)
		;

		private ProjectPlanOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
	}

	public static enum ProjectExecuteOperation {
		READ(1), UPDATE(2), ADD(4), DELETE(16), READ_ALL(128), SEARCH(128), 
		READ_PAGINATE(128), FAVORITE(32), LOCK_UNLOCK(64), SUBMIT(256), 
		TAG(512), LIKE_DISLIKE(1024), COMMENT(2048), TODO(4096), MANAGE_MANAGERS(8192)
		;

		private ProjectExecuteOperation(final int text) {
			this.operation = text;
		}
		private final int operation;
		public int getValue() {
			return operation;
		}
	}

}
