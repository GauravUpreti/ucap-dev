package com.rapidinstinct.ucap.plat.cbo;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public abstract class BaseDocumentBO extends BaseBusinessObject{

	private static final long serialVersionUID = 1L;

	protected String id;
	protected int status;
	protected String creator;
	protected String owner;
	protected String modifier;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}	
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getCreator() {
		return creator;
	}
	
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getModifier() {
		return modifier;
	}
	
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}


}
