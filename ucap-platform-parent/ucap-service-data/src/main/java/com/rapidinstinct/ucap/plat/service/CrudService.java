package com.rapidinstinct.ucap.plat.service;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;


/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public interface CrudService {

	public  ServiceResponse read(ServiceRequest request);

	public  ServiceResponse add(ServiceRequest request);

	public  ServiceResponse update(ServiceRequest request);

	public  ServiceResponse delete(ServiceRequest request);

}
