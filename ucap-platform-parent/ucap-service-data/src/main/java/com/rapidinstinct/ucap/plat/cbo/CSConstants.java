package com.rapidinstinct.ucap.plat.cbo;

import org.apache.commons.lang3.StringUtils;


/**
 * 
 * @author Gaurav Upreti
 *
 */

public class CSConstants {

	public static enum CSResultCodes {
		
		DOCUMENT_NOT_FOUND("document-not-found"),
		DOCUMENT_FOUND("document-found"),
		DOCUMENT_NOT_ACCESSIBLE("document-not-accessible"),
		USER_ATTRIBUTES_NF("user-attributes-nf"),
		OPERATION_NOT_SUPPORTED("operation-ns"),
		OPERATION_FAILURE("operation-failure"),
		OPERATION_SUCCESS("operation-success"),
		DOCUMENT_SAVED("document-saved"),
		DOCUMENT_DELETED("document-deleted"),
		DOCUMENT_SAVE_FAILED("document-save-failed"),
		REQUEST_DATA_INVALID("request-data-invalid"),
		DOCUMENT_OPERATION_DENIED("document-operation-denied"),
		DOCUMENT_VALIDATION_FAILED("document-validation-failed"),
		REQUEST_DATA_VALID("request-data-valid"),
		;
		
		
		private CSResultCodes(final String text) {
			this.text = text;
		}

		private final String text;

		public String getValue() {
			return text;
		}
		
		public CSResultCodes getType(String value){
			CSResultCodes types [] = CSResultCodes.values();
			for(CSResultCodes type:types){
				if(StringUtils.equals(value, type.getValue())){
					return type;
				}
			}
			return null;
		}
	}

}
