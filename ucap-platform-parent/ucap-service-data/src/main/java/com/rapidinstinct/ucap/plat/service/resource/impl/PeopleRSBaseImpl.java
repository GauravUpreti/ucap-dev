package com.rapidinstinct.ucap.plat.service.resource.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserSummaryBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.PeopleSkillProfileDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.resource.PeopleSkillProfileRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.OrganizationRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserLoginRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserMembershipRequestRepository;
import com.rapidinstinct.ucap.plat.service.helper.AuthorizationHelper;
import com.rapidinstinct.ucap.plat.service.helper.AuthorizationResult;
import com.rapidinstinct.ucap.plat.service.helper.AuthorizationHelper.RAResourceOperation;
import com.rapidinstinct.ucap.plat.service.resource.PeopleRS;

/**
 * @author Gaurav Upreti
 *
 */

public abstract class PeopleRSBaseImpl implements PeopleRS {

	@Autowired protected UserLoginRepository userLoginRepo;
	@Autowired protected OrganizationRepository organizationRepo;
	@Autowired protected UserGroupsRepository userGroupsRepo;
	@Autowired protected UserDetailsRepository userDetailsRepo;	
	@Autowired protected PeopleSkillProfileRepository userProfileRepo;	
	@Autowired protected MongoOperations operations;
	@Autowired protected UserMembershipRequestRepository memReqRepo;
	
	protected static final String DEFAULT_PASSWORD  ="welcome";
	@Value("${component.list.size.limit}") protected int listSize;
	
	@Override
	public ServiceResponse readAll(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO> userList = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.READ_ALL);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()){
			UserDetailsBO user =   request.getUser();
			Sort sortOrder = new Sort(Direction.ASC, DocumentField.FIRSTNAME.toString());
			PageRequest pageRequest = new PageRequest(0, listSize, sortOrder);		
			userList =  userDetailsRepo.findByOrgIds(user.getCurrentOrgId(), pageRequest);
			result = true;
			resultCode = USResultCodes.USER_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}

	@Override
	public ServiceResponse searchAllPeople(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO>  userList = null;
		String searchTerm = request.getRequestData(String.class);
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.SEARCH);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()){
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(searchTerm)){
				String queryStr = buildSearchQuery(searchTerm, null);
				Sort sortOrder = new Sort(Direction.ASC, DocumentField.FIRSTNAME.toString());
				Query query = new BasicQuery(queryStr);
				query.limit(listSize);	
				query.with(sortOrder);
				userList = operations.find(query, UserDetailsDO.class);
				result = true;
				resultCode = USResultCodes.USER_FOUND.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}

	
	/**
	 * create UserLoginDO, UserDetailsDO, UserProfileDO 
	 */
	//TODO - send email
	@Override
	public ServiceResponse add(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		PeopleSkillProfileDO profile = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.ADD);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			profile = request.getRequestData(PeopleSkillProfileDO.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(profile != null) {
				UserDetailsBO user = request.getUser();
				resultCode = USResultCodes.DUPLICATE_EMAIL.getValue();
				if(!isEmailInUse(profile.getEmail())){
					UserLoginDO loginDO = UserLoginDOHelper.createNewLocalLogin(profile.getEmail(), DEFAULT_PASSWORD);
					UserLoginDO updatedLogin = userLoginRepo.save(loginDO);
					resultCode = CSResultCodes.OPERATION_FAILURE.getValue();
					if(updatedLogin != null){
						result = true;
						UserDetailsDO detailDO = new UserDetailsDO(updatedLogin.getId());
						detailDO.setFirstName(profile.getFirstName());
						detailDO.setLastName(profile.getLastName());
						detailDO.addOrgId(user.getCurrentOrgId());
						userDetailsRepo.save(detailDO);
						
						profile.setUserId(updatedLogin.getId());
						profile = userProfileRepo.save(profile);
						resultCode = USResultCodes.REGISTRATION_SUCCESS.getValue();
						
					}
				}
			}
		}

		ServiceResponse response = new ServiceResponse(result, resultCode);	
		response.addResponseData(profile);
		return response;
	}

	@Override
	public ServiceResponse readProfile(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserSummaryBO userSummary = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.READ);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()){
			String uid = request.getRequestData(String.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(uid)){
				UserDetailsBO user = request.getUser();
				String orgId = user.getCurrentOrgId();
				UserDetailsDO userDO = userDetailsRepo.findByUserIdAndOrgIds(uid, orgId);
				resultCode = USResultCodes.USER_NOT_FOUND.getValue();
				if(userDO != null){
					userSummary = new UserSummaryBO();
					userSummary.setProfile(userDO);

					UserGroupsDO userGroups = userGroupsRepo.findOne(uid);
					if(userGroups!=null){
						OrganizationGroupsDO groups = userGroups.getOrganizationGroups(orgId);
						if(groups!=null){
							userSummary.setOrgMemberships(groups);
						}
					}
					
					PeopleSkillProfileDO skillProfile = userProfileRepo.findOne(uid);
					if(skillProfile != null){
						userSummary.setSkillProfile(skillProfile);
					}

					List<UserMembershipRequestDO> memReq = memReqRepo.findByUserIdAndOrgId(uid, orgId);
					if(memReq!=null && memReq.size() > 0){
						userSummary.setOrgRequest(memReq.get(0));
					}
					
					resultCode = USResultCodes.USER_FOUND.getValue();
					result = true;
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userSummary);
		return response;
	}

	@Override
	public ServiceResponse updateProfile(ServiceRequest request) {
		boolean result = false;
		String resultCode = USResultCodes.USER_NOT_FOUND.getValue();
		PeopleSkillProfileDO profile = null;
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.UPDATE);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()) {
			profile = request.getRequestData(PeopleSkillProfileDO.class);
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(profile != null) {
				UserDetailsBO user = request.getUser();
				UserDetailsDO userDO = userDetailsRepo.findByUserIdAndOrgIds(profile.getUserId(), user.getCurrentOrgId());
				resultCode = USResultCodes.USER_NOT_FOUND.getValue();
				if(userDO != null) {
					profile.setModifiedDT(new Date());
					userProfileRepo.save(profile);
					result = true;
					resultCode = USResultCodes.USER_PROFILE_UPDATED.getValue();
				}
			}
		}
		
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(profile);
		return response;
	}

	protected  boolean isEmailInUse(String emailId) {
		UserLoginDO login = null;
		if(StringUtils.isNotBlank(emailId)){
			login = userLoginRepo.findByLocalEmailIdsEmailId(StringUtils.lowerCase(emailId));
		}
		return login != null;
	}

	@Override
	public ServiceResponse searchOrgPeople(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO>  userList = null;
		UserDetailsBO user = request.getUser();
		String searchTerm = request.getRequestData(String.class);
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.SEARCH);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()){
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(searchTerm)){
				String orgId = user.getCurrentOrgId();
				String queryStr = buildSearchQuery(searchTerm, orgId);
				userList = searchUsers(queryStr);
				result = true;
				resultCode = USResultCodes.USER_FOUND.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}
	
	
	@Override
	public ServiceResponse searchOrgNonMembers(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO>  userList = null;
		UserDetailsBO user = request.getUser();
		String searchTerm = request.getRequestData(String.class);
		AuthorizationResult authResult = AuthorizationHelper.checkAuthroization(request, RAResourceOperation.SEARCH);
		resultCode = authResult.getResultCode();
		if(authResult.isAccessible()){
			resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
			if(StringUtils.isNotBlank(searchTerm)){
				String orgId = user.getCurrentOrgId();
				String queryStr = buildNonMemberSearchQuery(searchTerm, orgId);
				userList = searchUsers(queryStr);
				result = true;
				resultCode = USResultCodes.USER_FOUND.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}

	
	private List<UserDetailsDO> searchUsers(String queryStr){
		Sort sortOrder = new Sort(Direction.ASC, DocumentField.FIRSTNAME.toString());
		Query query = new BasicQuery(queryStr);
		query.limit(listSize);	
		query.with(sortOrder);
		return operations.find(query, UserDetailsDO.class);
	}

	private String buildSearchQuery(String searchTerm, String orgId){
		String queryStr = createMatchCriteria(searchTerm);
		if(StringUtils.isNotBlank(orgId)){
			queryStr = queryStr + ", orgIds : '"+ orgId + "'";
		}
		queryStr = queryStr + "}";
		return queryStr;
	}

	/**
	 * @param searchTerm
	 * @return
	 */
	private String createMatchCriteria(String searchTerm) {
		String queryStr = null;
		queryStr = "{"
				+ "$or:["
						+ "{firstName: {$regex: '" + searchTerm +  "', $options:'i'}}, "
						+ "{lastName: {$regex: '" + searchTerm + "', $options:'i'}}, "
						+ "{emails: {$regex: '" + searchTerm + "', $options:'i'}} "
					+ "]";
		return queryStr;
	}

	private String buildNonMemberSearchQuery(String searchTerm, String orgId){
		String queryStr = createMatchCriteria(searchTerm);
		if(StringUtils.isNotBlank(orgId)){
			queryStr = queryStr + ", orgIds :{ $nin: ['"+ orgId + "']}";
		}
		queryStr = queryStr + "}";
		return queryStr;
	}
}
