package com.rapidinstinct.ucap.plat.service.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.cbo.CSConstants;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.MCCReportsComponentDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.MCCReportsComponentDO.Reports;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.MCCReportsCommonOperations;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.RBMCommonOperations;
import com.rapidinstinct.ucap.plat.service.component.MCCReportsService;

/**
 * 
 * @author Shamim Ahmad
 * 
 */

@Service
public class MCCReportsServiceImpl implements MCCReportsService {

	@Autowired
	private MCCReportsCommonOperations mro;
	@Autowired
	private RBMCommonOperations ops;

	@Override
	public ServiceResponse readAll(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID
				.getValue();
		String userName = request.getUser().getUserId();
		List<MCCReportsComponentDO> mccReports = null;
		if (userName != null) {
			mccReports = (List<MCCReportsComponentDO>) mro.read();
			for (int i = 0; i < mccReports.size(); i++) {
				mccReports.get(i).setStudyCount(mro.userStudyCount(userName));
				List<Reports> reports= mccReports.get(i).getReports();
				int count=0;
				for (int j=0;j<reports.size();j++)
				if (reports.get(j).getUserName().equals(userName)){
					count++;}
				mccReports.get(i).setActiveCount(count);
				
			}
			result = true;
			resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(mccReports);
		return response;
	}

	@Override
	public ServiceResponse readByModuleId(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID
				.getValue();
		String moduleId = request.getRequestData(String.class);
		String userId = request.getUser().getUserId();
		MCCReportsComponentDO mccComponentDO = null;
		List<Reports> reports=new ArrayList<Reports>();
		List<Reports> userReports=new ArrayList<Reports>();
	    if (userId != null && StringUtils.isNotBlank(moduleId)) {
		mccComponentDO = mro.readByModuleId(moduleId);
		reports=mccComponentDO.getReports();
		for(int i=0;i<reports.size();i++)
		if(userId.equals(reports.get(i).getUserName()))
		{
		reports.get(i).setBgColorCode(mccComponentDO.getBgColorCode());
		userReports.add(reports.get(i));
	    result = true;
		resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
		}
	    else{
			result = false;
			resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		}}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userReports);
		return response;
	}

	@Override
	public ServiceResponse readUserStudy(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID
				.getValue();
		List<StudyDO> studyDO = new ArrayList<StudyDO>();
		UserAccessDO userStudy = null;
		List<String> study = null;
		String userId = request.getUser().getUserId();
		try {
			userStudy = (UserAccessDO) ops.findByUserId(userId);
			study = userStudy.getStudyName();
			if (userStudy != null && study != null) {
				for (int i = 0; i < study.size(); i++) {
					studyDO.addAll(ops.findByStudyId(study.get(i)));
				}
				result = true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND
						.getValue();
			}
		} catch (Exception e) {
			result = false;
			resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID
					.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(studyDO);
		return response;
	}


	@Override
	public ServiceResponse updateActiveStudies(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		MCCReportsComponentDO mccDO = null;
		List<Reports> reports=new ArrayList<Reports>();
		MCCReportsComponentDO mccBO = request.getRequestData(MCCReportsComponentDO.class);
		String moduleId = mccBO.getModuleId();
		String userId = request.getUser().getUserId();
		if(mccBO!=null && StringUtils.isNotBlank(moduleId) && StringUtils.isNotBlank(userId)){
				mccDO = mro.readByModuleId(moduleId);
				if(mccDO.getReports().size()!=0)
				reports.addAll( mccDO.getReports());
				for (int i=0;i<mccDO.getReports().size();i++){
				for(int j=0;j<mccBO.getReports().size();j++)	
				{
					if(mccDO.getReports().get(i).getUserName().equals(mccBO.getReports().get(j).getUserName())&& mccDO.getReports().get(i).getReportName().equals(mccBO.getReports().get(j).getReportName()))
						{
						mccDO.getReports().get(i).setActiveStudies(mccBO.getReports().get(j).getActiveStudies());
						}
				}}
     			mccDO.setReports(reports);
				mro.update(mccDO);
				result=true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_SAVED.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(mccDO);
		return response;
	}

	@Override
	public ServiceResponse add(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse update(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse delete(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse read(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
}