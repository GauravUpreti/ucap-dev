package com.rapidinstinct.ucap.plat.service.exception;


/**
 * 
 * @author Gaurav Upreti
 *
 */


public class DocumentServiceException extends DataServiceException {

	private static final long serialVersionUID = 1L;


	public DocumentServiceException(String errorCode){
        super(errorCode);
    }

	public DocumentServiceException(String errorCode, String message){
        super(errorCode, message);
    }

 
}
