package com.rapidinstinct.ucap.plat.service.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.OrgDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserLocalLoginBO;
import com.rapidinstinct.ucap.plat.bo.user.UserOrgGroupsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserPassChangeRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.UserProfileBO;
import com.rapidinstinct.ucap.plat.bo.user.UserProfileRequestBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.AccessAttributes;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.bo.user.UserOrgGroupsBO.OrgGroup;
import com.rapidinstinct.ucap.plat.bo.user.UserProfileBO.OrgRolesBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.commons.util.Encryptor;
import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentField;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.PeopleSkillProfileDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserGroupsDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDOHelper;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginStatus;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginLocalDO.LoginEmail;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO.Context;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.VerificationDO.ContextDataKey;
import com.rapidinstinct.ucap.plat.data.mongo.repo.resource.PeopleSkillProfileRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.OrganizationRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserLoginRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserMembershipRequestRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.VerificationOperations;
import com.rapidinstinct.ucap.plat.service.MailServiceHelper;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserManagementServiceImpl implements UserManagementService {

	private static Logger log = LoggerFactory.getLogger(UserManagementServiceImpl.class);
	@Autowired private UserLoginRepository userLoginRepo;
	@Autowired private UserDetailsRepository userDetailsRepo;
	@Autowired private OrganizationRepository organizationRepo;
	@Autowired private UserGroupsRepository userGroupsRepo;
	@Autowired private UserMembershipRequestRepository membershipRepo;
	@Autowired private MongoOperations operations;
	@Autowired private PeopleSkillProfileRepository skillRepo;
	@Autowired private VerificationOperations verOps;
	@Autowired private MailServiceHelper msInvk;
	
	@Value("${component.list.size.limit}") private int listSize;

	public UserManagementServiceImpl(){ }
	
	private UserLoginDO addUser(UserLoginDO document){
		return userLoginRepo.save(document);
	}

	private UserLoginDO findUserByEmailId(String emailId){
		UserLoginDO login = null;
		if(StringUtils.isNotBlank(emailId)){
			login = userLoginRepo.findByLocalEmailIdsEmailId(StringUtils.lowerCase(emailId));
		}
		return login;
	}

	@Override
	public ServiceResponse isEmailInUse(ServiceRequest request) {
		String emailId = request.getStringAttribute(AccessAttributes.EMAIL_ID.getValue());
		UserLoginDO login = findUserByEmailId(StringUtils.lowerCase(emailId));
		ServiceResponse response = null;
		if(login != null){
			response = new ServiceResponse(true, USResultCodes.DUPLICATE_EMAIL.getValue());
		}else{
			response = new ServiceResponse(false, USResultCodes.EMAIL_NOT_FOUND.getValue());
		}
		return response;
	}

	@Override
	public ServiceResponse registerUser(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		LoginBO localLogin = request.getRequestData(LoginBO.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(localLogin != null){
			UserLoginDO login = findUserByEmailId(localLogin.getEmailId());
			resultCode = USResultCodes.DUPLICATE_EMAIL.getValue();	
			if(login == null){			
				//create a new local login user
				UserLoginDO loginDO = UserLoginDOHelper.createNewLocalLogin(localLogin.getEmailId(), localLogin.getPassword());
				loginDO.setStatus(LoginStatus.VERIFICATION_PENDING.getValue());
				loginDO = addUser(loginDO);
				localLogin.setUserId(loginDO.getId());
				localLogin.maskPassword();
				resultCode = USResultCodes.REGISTRATION_SUCCESS.getValue();	
				result = true;
				//send verification email
				String token = Encryptor.getInstance().getRandomString(15);
				VerificationDO vdo = new VerificationDO(loginDO.getId(), token, Context.USER_REGISTRATION.getValue(), loginDO.getId());
				vdo.addContextData(ContextDataKey.email.toString(), localLogin.getEmailId());
				verOps.add(vdo);
				msInvk.sendRegVerMail(token, localLogin.getEmailId());
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(localLogin);
		return response;
	}

	@Override
	public ServiceResponse autheticate(ServiceRequest request) {
		LoginBO localLogin = request.getRequestData(LoginBO.class);
		UserLoginDO loginDO = findUserByEmailId(localLogin.getEmailId());
		boolean result = false;
		String resultCode = null;
		UserDetailsBO userDetails = null;
		resultCode = USResultCodes.USER_NOT_FOUND.getValue();
		if(loginDO != null){
			if(loginDO.isActiveUser()){
				resultCode = USResultCodes.INVALID_CREDENTIAL.getValue();
				if(loginDO.getLocal().comparePassword(localLogin.getPassword())){
					localLogin.maskPassword();
					addUser(loginDO);
					//get user profile details
					userDetails = new UserDetailsBO(loginDO.getId());
					userDetails.setEmail(localLogin.getEmailId());
					loadUserDetails(userDetails);
					result = true;
					resultCode = USResultCodes.LOGIN_SUCCESS.getValue();
				}
			}else if(loginDO.getStatus() ==  LoginStatus.INACTIVE.getValue()){
				resultCode = USResultCodes.USER_INACTIVATED.getValue();
			}else if(loginDO.getStatus() ==  LoginStatus.VERIFICATION_PENDING.getValue()){
				resultCode = USResultCodes.VERIFICATION_PENDING.getValue();
			}
		}
		
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userDetails);
		return response;
	}

	@Override
	public ServiceResponse updateUserProfile(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		UserDetailsDO updtDetails = null;
		UserDetailsBO curUser = request.getUser();
		UserProfileRequestBO profile = request.getRequestData(UserProfileRequestBO.class);
		if(curUser!=null && profile!=null){
			profile.setUserId(curUser.getUserId());
			UserDetailsDO detailsDO = userDetailsRepo.findOne(curUser.getUserId());
			if (detailsDO != null){
				BeanUtils.copyProperties(profile, detailsDO);
				detailsDO.setModifiedDT(new Date());
			}else{
				detailsDO = new UserDetailsDO(curUser.getUserId());
				BeanUtils.copyProperties(profile, detailsDO);
			}
			updtDetails = userDetailsRepo.save(detailsDO);
			result = true;
			resultCode = USResultCodes.USER_PROFILE_UPDATED.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(updtDetails);
		return response;
	}


	//user name is email id in case of local login,
	@Override
	public UserDetails loadUserByUsername(String username)	throws UsernameNotFoundException {
		UserLoginDO loginDO = findUserByEmailId(username);
		if(loginDO == null){
			throw new UsernameNotFoundException("User name not found-->" + username);
		}

		UserDetailsBO user = new UserDetailsBO(loginDO.getId());
		user.setEmail(username);
		if(!loginDO.isActiveUser()){
			user.setEnabled(false);
		}else if(loginDO.isActiveUser()){
			loadUserDetails(user);
		}
		return user;
	}

	private void loadUserDetails(UserDetailsBO user) {
		UserDetailsDO detailsDO = userDetailsRepo.findOne(user.getUserId());
		if(detailsDO != null){
			user.setFirstName(detailsDO.getFirstName());
			user.setLastName(detailsDO.getLastName());
		}
	}

	@Override
	public ServiceResponse addUserOrgGroups(ServiceRequest request) {
		UserDetailsBO curUser = request.getRequestData(UserDetailsBO.class);
		boolean result = false;
		String resultCode = null;
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser != null){
			loadUserDetails(curUser);
			UserGroupsDO userGroups = userGroupsRepo.findOne(curUser.getUserId());
			resultCode = USResultCodes.USER_GROUPS_NOT_FOUND.getValue();
			if(userGroups != null){
				for(OrganizationGroupsDO orgGrp : userGroups.getOrgGroups()){
					if(orgGrp.getOrgId().equals(curUser.getCurrentOrgId())){
						for(String grp : orgGrp.getGrpIds()){
							result = true;
							Role role = Role.getType(grp);
							if(role!=null){
								curUser.addRole(role);
							}
						}
						break;
					}
				}
			}
			if(result){
				resultCode = USResultCodes.USER_GROUPS_FOUND.getValue();
			}
		}
		
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(curUser);
		return response;
	}

	@Override
	public ServiceResponse getUserDetails(ServiceRequest request) {
		UserDetailsBO userDetails = request.getUser();
		UserProfileBO profile = null;
		boolean result = false;
		String resultCode = USResultCodes.USER_NOT_FOUND.getValue();;
		if(userDetails != null){
			profile = new UserProfileBO(userDetails.getUserId(), userDetails.getUsername());
			loadUserDetails(userDetails);
			profile.setFirstName(userDetails.getFirstName());
			profile.setLastName(userDetails.getLastName());
			UserGroupsDO userGroups = userGroupsRepo.findOne(userDetails.getUserId());
			if(userGroups!=null){
	        	for(OrganizationGroupsDO orgGroup: userGroups.getOrgGroups()){
	        		OrganizationDO org = organizationRepo.findOne(orgGroup.getOrgId());
	        		String orgName = org != null ? org.getName() : StringUtils.EMPTY;
	        		OrgRolesBO orgRoles = new OrgRolesBO(orgGroup.getOrgId(), orgName);
	        		for(String groupName : orgGroup.getGrpIds()){
	        			Role role = Role.getType(groupName);
	        			if(role != null){
	        				orgRoles.addRole(role);
	        			}
	        		}
	        		profile.addOrgRoles(orgRoles);
	        	}
			}
			result = true;
			resultCode = USResultCodes.USER_FOUND.getValue();
		}		
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(profile);
		return response;
	}

	@Override
	public ServiceResponse searchOrgUsers(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<UserDetailsDO>  userList = null;
		UserDetailsBO user = request.getUser();
		String searchTerm = request.getRequestData(String.class);
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(StringUtils.isNotBlank(searchTerm)){
			String orgId = user.getCurrentOrgId();
			String queryStr = "{"
					+ "$or:["
							+ "{firstName: {$regex: '" + searchTerm +  "', $options:'i'}}, "
							+ "{lastName: {$regex: '" + searchTerm + "', $options:'i'}}"
						+ "], "
					+ "orgIds : '"+ orgId + "'}";
			Sort sortOrder = new Sort(Direction.ASC, DocumentField.FIRSTNAME.toString());
			Query query = new BasicQuery(queryStr);
			query.limit(listSize);	
			query.with(sortOrder);
			userList = operations.find(query, UserDetailsDO.class);
			result = true;
			resultCode = USResultCodes.USER_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userList);
		return response;
	}

	//TODO - Invalidate Token
	@Override
	public ServiceResponse changePassword(ServiceRequest request) {
		boolean result = false;
		UserDetailsBO curUser = request.getUser();
		UserPassChangeRequestBO profile = request.getRequestData(UserPassChangeRequestBO.class);
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser!=null && profile!=null){
			UserLoginDO login = userLoginRepo.findOne(curUser.getUserId());
			resultCode = USResultCodes.USER_NOT_FOUND.getValue();
			if(login!=null){
				resultCode = USResultCodes.INVALID_CREDENTIAL.getValue();
				if(login.getLocal().comparePassword(profile.getCurrentPassword())){
					login.getLocal().setPassword(profile.getNewPassword());
					login.getLocal().encryptPassword();
					userLoginRepo.save(login);
					result = true;
					resultCode = USResultCodes.USER_PROFILE_UPDATED.getValue();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(curUser);
		return response;
	}

	//TODO shootout an email with temp password
	@Override
	public ServiceResponse recoverPassword(ServiceRequest request) {
		boolean result = true;
		String emailId = request.getRequestData(String.class);
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(StringUtils.isNotBlank(emailId)){
			UserLoginDO loginDO = findUserByEmailId(emailId);
			if(loginDO!=null && loginDO.isActiveUser()){
				String tempPass = Encryptor.getInstance().getRandomPassword(10);
				log.info("Temporary Password:" + tempPass);
				loginDO.getLocal().setPassword(tempPass);
				loginDO.getLocal().encryptPassword();
				//userLoginRepo.save(loginDO);
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	@Override
	public ServiceResponse getUserWorkDetails(ServiceRequest request) {
		boolean result = true;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		PeopleSkillProfileDO skillProfile = null;
		if(curUser != null){
			skillProfile = skillRepo.findOne(curUser.getUserId());
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(skillProfile);
		return response;
	}

	@Override
	public ServiceResponse updateUserWorkDetails(ServiceRequest request) {
		boolean result = true;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		PeopleSkillProfileDO reqSkill = request.getRequestData(PeopleSkillProfileDO.class);
		PeopleSkillProfileDO respSkill = null;
		if(curUser != null && reqSkill != null){
			PeopleSkillProfileDO skillProfile = skillRepo.findOne(curUser.getUserId());
			reqSkill.setUserId(curUser.getUserId());
			
			if(skillProfile != null){
				skillProfile.setSummary(skillProfile.getSummary());

				if(reqSkill.getExperience()!=null){
					skillProfile.setExperience(reqSkill.getExperience());	
				}

				if(reqSkill.getEducation()!=null){
					skillProfile.setEducation(reqSkill.getEducation());	
				}

				if(reqSkill.getSkills()!=null){
					skillProfile.setSkills(reqSkill.getSkills());	
				}

				if(reqSkill.getSkillProfiles()!=null){
					skillProfile.setSkillProfiles(reqSkill.getSkillProfiles());	
				}

				if(reqSkill.getProfileUrls()!=null){
					skillProfile.setProfileUrls(reqSkill.getProfileUrls());	
				}
				
				reqSkill.setModifiedDT(new Date());
			}
			
			respSkill = skillRepo.save(skillProfile);
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(respSkill);
		return response;
	}

	@Override
	public ServiceResponse getUserOrgDetails(ServiceRequest request) {
		boolean result = true;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		UserOrgGroupsBO orgGroupsBO = null;
		if(curUser!=null){
			List<OrgGroup> orgGrpList = new ArrayList<OrgGroup>();
			//add accepted org membership details
			UserGroupsDO userGroupsDO = userGroupsRepo.findOne(curUser.getUserId());
			if(userGroupsDO != null){
				orgGroupsBO = new UserOrgGroupsBO();	
				orgGroupsBO.setUserId(curUser.getUserId());
				for(OrganizationGroupsDO orgGrpDO : userGroupsDO.getOrgGroups()){
					OrganizationDO orgDO = organizationRepo.findOne(orgGrpDO.getOrgId());
					if(orgDO != null){
						OrgGroup orgGroup = new OrgGroup();
						orgGroup.setOrganization(OrgDetailsBO.fromDO(orgDO));
						orgGroup.setGroups(OrgGroup.from(orgGrpDO.getGrpIds()));
						orgGrpList.add(orgGroup);
					}
				}
				
			}
			//add new org membership request details
			List<UserMembershipRequestDO> memRequests = membershipRepo.findByUserIdAndStatus(curUser.getUserId(), UserMembershipRequestDO.Status.NEW.getValue());
			for(UserMembershipRequestDO memReq : memRequests){
				OrganizationDO orgDO = organizationRepo.findOne(memReq.getOrgId());
				if(orgDO != null){
					OrgGroup orgGroup = new OrgGroup();
					orgGroup.setOrganization(OrgDetailsBO.fromDO(orgDO));
					orgGroup.setGroups(OrgGroup.from(memReq.getMembershipType(), UserMembershipRequestDO.Status.NEW.getValue()));
					orgGrpList.add(orgGroup);
				}
			}
			orgGroupsBO.setOrgGroups(orgGrpList);
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(orgGroupsBO);
		return response;
	}

	@Override
	public ServiceResponse verifyRegistation(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String verToken = request.getRequestData(String.class);
		UserLocalLoginBO ullb = null;
		if(StringUtils.isNotBlank(verToken)){
			VerificationDO vdo = verOps.findByToken(verToken);
			if(vdo != null && vdo.isValidToken() && StringUtils.isNotBlank(vdo.getContextRefId())){
				String emailId = (String) vdo.getContextData(ContextDataKey.email.toString());
				UserLoginDO uldo = userLoginRepo.findOne(vdo.getContextRefId());
				resultCode = USResultCodes.USER_NOT_FOUND.getValue();
				if(uldo != null && uldo.isVerificationPending()){
					uldo.setStatus(LoginStatus.ACTIVE.getValue());
					Set<LoginEmail> loginMails = uldo.getLocal().getEmailIds();
					Iterator<LoginEmail> itrt = loginMails.iterator();
					while(itrt.hasNext()){
						LoginEmail le = itrt.next();
						if(le.getEmailId().equals(emailId)){
							le.setStatus(LoginStatus.ACTIVE.getValue());
							result = true;
						}
					}
					if(result){
						resultCode = USResultCodes.REGISTRATION_SUCCESS.getValue();
						userLoginRepo.save(uldo);
						verOps.delete(vdo);
						//add new default organization
						OrganizationDO orgDO = OrganizationDOHelper.createNew(uldo.getId(), emailId);
						orgDO = organizationRepo.save(orgDO);
						
						//add newly created user as admin of default organization
						UserGroupsDO userGroup = UserGroupsDOHelper.createNew(uldo.getId(), orgDO.getId(), UserGroupsDO.GRP_ADMIN);
						userGroupsRepo.save(userGroup);
						ullb = new UserLocalLoginBO(uldo.getId());
						ullb.setEmailId(emailId);
					}else{
						resultCode = USResultCodes.REGISTRATION_FAILED.getValue();
					}
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(ullb);
		return response;
	}
}