package com.rapidinstinct.ucap.plat.service.exception;


/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserServiceException extends DataServiceException {
	private static final long serialVersionUID = 1L;

	public UserServiceException(String errorCode){
        super(errorCode);
    }

	public UserServiceException(String errorCode, String message){
        super(errorCode, message);
    }
	

}
