package com.rapidinstinct.ucap.plat.bo.user;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.Address;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO.ContactNumber;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserProfileRequestBO extends BaseBusinessObject {
	private static final long serialVersionUID = 1L;
	private String userId;
	private String firstName;
	private String lastName;
	private String middleName;
	private String email;
	private String profileImgURL;
	private Set<String> emails = new HashSet<String>();
	private Set<ContactNumber> workCNS = new HashSet<ContactNumber>();
	private Set<ContactNumber> personalCNS = new HashSet<ContactNumber>();
	private Date dob;
	private Address address;
	private Map<String, Object> metadata;

	public UserProfileRequestBO(){	}

	public UserProfileRequestBO(String userId){
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public String getProfileImgURL() {
		return profileImgURL;
	}
	
	public void setProfileImgURL(String profileImgURL) {
		this.profileImgURL = profileImgURL;
	}
		
	public Set<String> getEmails() {
		return emails;
	}

	public void setEmails(Set<String> alternateEmailIds) {
		this.emails = alternateEmailIds;
	}

	public boolean addEmail(String emailId) {
		return this.emails.add(emailId);
	}

	public boolean addEmails(List<String> emailId) {
		return this.emails.addAll(emailId);
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Set<ContactNumber> getWorkCNS() {
		return workCNS;
	}

	public void setWorkCNS(Set<ContactNumber> workCNS) {
		this.workCNS = workCNS;
	}

	public Set<ContactNumber> getPersonalCNS() {
		return personalCNS;
	}

	public void setPersonalCNS(Set<ContactNumber> personalCNS) {
		this.personalCNS = personalCNS;
	}

	public Map<String, Object> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}
	
}
