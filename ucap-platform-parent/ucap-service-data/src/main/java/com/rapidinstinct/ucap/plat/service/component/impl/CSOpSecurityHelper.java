package com.rapidinstinct.ucap.plat.service.component.impl;

import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.BaseDocumentDO;
import com.rapidinstinct.ucap.plat.service.helper.DocAuthResult;

/**
 * @author Gaurav Upreti
 *
 */

public abstract class CSOpSecurityHelper {

	private CSOpSecurityHelper() { }
	
	public static DocAuthResult isPublishedDocReadable(BaseDocumentDO document, UserDetailsBO user){
		DocAuthResult daRes = createDefaultRequest();
		if(document == null){
			daRes.setResultCode(CSResultCodes.DOCUMENT_NOT_FOUND.getValue());
		}else {
			daRes.setAccessible(true);
			daRes.setResultCode(CSResultCodes.DOCUMENT_FOUND.getValue());
		}
		return daRes;
	}
	
	private static DocAuthResult createDefaultRequest() {
		DocAuthResult daRes = new DocAuthResult();
		daRes.setAccessible(false);
		daRes.setResultCode(CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue());
		return daRes;
	}

}
