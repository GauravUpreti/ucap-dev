package com.rapidinstinct.ucap.plat.service.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.cbo.CSConstants;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudiesDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.StudyDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.SubjectDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.UserAccessDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.DashBoardOperations;
import com.rapidinstinct.ucap.plat.service.component.DashBoardService;

/**
 * @author Gaurav Upreti
 *
 */
@Service
public class DashBoardServiceImpl  implements DashBoardService {

	@Autowired	private DashBoardOperations ops;
	
	@Override
	public ServiceResponse readSubjects(ServiceRequest sreq) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<SubjectDO> dbdo = null;
		String user = sreq.getUser().getUserId();
			if (user != null) {
				dbdo = ops.read();
				result = true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(dbdo);
		return response;
	}

	@Override
	public ServiceResponse readStudies(ServiceRequest sreq) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<StudiesDO> dbdo = null;
		String user = sreq.getUser().getUserId();
			if (user != null) {
				dbdo = ops.readStudies();
				result = true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(dbdo);
		return response;
	}

	@Override
	public ServiceResponse readUserStudy(ServiceRequest request) {
		
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<StudyDO> studyDO = new ArrayList<StudyDO>();
		UserAccessDO userStudy = null;
		List<String> study = null;
		String userId = request.getUser().getUserId();
		try {
			userStudy = (UserAccessDO) ops.findByUserId(userId);
			study = userStudy.getStudyName();
			if (userStudy != null && study != null) {
				for (int i = 0; i < study.size(); i++) {
					studyDO.addAll(ops.findByStudyId(study.get(i)));
				}
				result = true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		} catch (Exception e) {
			result = false;
			resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(studyDO);
		return response;
	}

}
