package com.rapidinstinct.ucap.plat.service.user;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public interface UserManagementService extends UserDetailsService {

	//User registration operations
	public ServiceResponse isEmailInUse(ServiceRequest request);
	public ServiceResponse registerUser(ServiceRequest request);
	public ServiceResponse verifyRegistation(ServiceRequest request);
	public ServiceResponse recoverPassword(ServiceRequest request);
	

	//user profile operations
	public ServiceResponse getUserDetails(ServiceRequest request);
	public ServiceResponse updateUserProfile(ServiceRequest request);
	public ServiceResponse changePassword(ServiceRequest request);
	public ServiceResponse getUserWorkDetails(ServiceRequest request);
	public ServiceResponse updateUserWorkDetails(ServiceRequest request);
	
	//user security operations
	public ServiceResponse autheticate(ServiceRequest request);
	public ServiceResponse getUserOrgDetails(ServiceRequest request);
	public ServiceResponse addUserOrgGroups(ServiceRequest request);
	
	public ServiceResponse searchOrgUsers(ServiceRequest request);

}
