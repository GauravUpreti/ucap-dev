package com.rapidinstinct.ucap.plat.service.component;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Service
public interface RBMService extends ComponentService{
	
	public ServiceResponse addRisk(ServiceRequest request);
	
	public ServiceResponse updateRisk(ServiceRequest request);
	
	public ServiceResponse readThresholds(ServiceRequest request);
	
	public ServiceResponse readUserStudy(ServiceRequest request);

	public ServiceResponse search(ServiceRequest request);

	public ServiceResponse readGroups(ServiceRequest request);

	public ServiceResponse getKRICountByStudyName(ServiceRequest request);
	
	public ServiceResponse getEnrolledCountByStudyName (ServiceRequest request);

	public ServiceResponse getSiteCountByStudyName (ServiceRequest request);

}