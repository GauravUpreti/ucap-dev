package com.rapidinstinct.ucap.plat.bo.user;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.Address;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;

/**
 * @author Gaurav Upreti
 *
 */
public class OrgDetailsBO extends BaseBusinessObject {
	private static final long serialVersionUID = 1L;

	protected String id;
	private String code;
	private String name;
	private String description;
	private String email;
	private String website;
	private Address address;
	private Set<String> alternateEmailIds = new HashSet<String>();
	private Set<String> phones = new HashSet<String>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Set<String> getAlternateEmailIds() {
		return alternateEmailIds;
	}
	public void setAlternateEmailIds(Set<String> alternateEmailIds) {
		this.alternateEmailIds = alternateEmailIds;
	}
	public Set<String> getPhones() {
		return phones;
	}
	public void setPhones(Set<String> phones) {
		this.phones = phones;
	}

	public boolean addAlternateEmail(String emailId) {
		return this.alternateEmailIds.add(emailId);
	}

	public boolean addAlternateEmail(List<String> emailId) {
		return this.alternateEmailIds.addAll(emailId);
	}

	public boolean addPhone(String phone) {
		return this.phones.add(phone);
	}

	public boolean addPhone(List<String> phones) {
		return this.phones.addAll(phones);
	}
	
	public static OrgDetailsBO fromDO(OrganizationDO orgDO){
		OrgDetailsBO details = null;
		if(orgDO != null){
			details = new OrgDetailsBO();
			BeanUtils.copyProperties(orgDO, details);
		}
		return details;
	}

}
