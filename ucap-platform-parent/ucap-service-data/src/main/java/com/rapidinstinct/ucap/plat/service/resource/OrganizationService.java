package com.rapidinstinct.ucap.plat.service.resource;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.service.CrudService;

/**
 * @author Gaurav Upreti
 *
 */
@Service
public interface OrganizationService extends CrudService {

	public  ServiceResponse verifyRegistration(ServiceRequest request);
	
	public  ServiceResponse updatePaymentInfo(ServiceRequest request);
	
}
