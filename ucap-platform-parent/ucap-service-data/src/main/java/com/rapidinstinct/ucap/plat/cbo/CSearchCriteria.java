package com.rapidinstinct.ucap.plat.cbo;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Gaurav Upreti
 *
 */

public class CSearchCriteria {

	private static Logger logger = LoggerFactory.getLogger(CSearchCriteria.class);    
	
	private String searchTerm;
	private String filters;
	private String mongoFilters;
	private boolean applyFilter = false;
	public static final String SEPARATOR_OP = ":";
	public static final String SEPARATOR_PARAMS = "|";
	public static final String SEPARATOR_VALUE = ",";

	public static final String PORTFOLIO_ALL = "all";

	public static final String QUERY_KEY_VALUE = "%s: %s";
	public static final String QUERY_KEY_OP_VALUE = "%s: {%s : %s}";
	public static final String QUERY_KEY_IN_VALUE = "%s: {$in : [%s]}";
	public static final String QUERY_KEY_RANGE_VALUE = "%s :{'%s' : %s, %s : %s}";
	
	private Map<String, String> queryKVMap = new HashMap<String, String>();
	
	//2014-10-08T00:00:00.000Z
	public static final String DATE_FORMAT = "%sT00:00:00.000Z";

	//IMPORTANT index ids should match with enums
	//TODO array index approach not safer, move mapping to map structure
	public static final String[] PARAMS_MAPPING = {
													"owner", 
													"createdDT", 
													"categoryCode", 
													"type", 
													"tags", 
													"status", 
													"publishedDT",
													"collaborators.members",
													"completedDT",
													"dueDT",
													"openedDT",
													"contextType",
													"contextId",
													"portfolioId"
													};
	public static final String[] OPERATOR_MAPPING = {"$lt", "$lte", "$gt", "$gte"};

	public CSearchCriteria(){ }
	
	public CSearchCriteria(String searchTerm, String filters){
		this.searchTerm = searchTerm;
		this.filters = filters;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public String getFilters() {
		return filters;
	}

	public String getMongoFilters() {
		return mongoFilters;
	}

	public boolean isFilterDefined(){
		return StringUtils.isNotBlank(filters);
	}

	public boolean isApplyFilter(){
		return applyFilter;
	}

	public boolean isSearchTermNotBlank(){
		return StringUtils.isNotBlank(searchTerm);
	}

	public String parseCriteria() {
		String criteriaStr = StringUtils.EMPTY;
		StringBuilder criteriaBuilder = new StringBuilder();
		if(StringUtils.isNotBlank(filters)){
			String criteriaParams[] = StringUtils.split(filters, SEPARATOR_PARAMS);
			for(int i = 0; i < criteriaParams.length; i++){
				boolean correct = true;
				String paramKeyVal = criteriaParams[i]; 
				String keyOpValue[] = StringUtils.split(paramKeyVal, SEPARATOR_OP);
				if(keyOpValue.length == 2){
					correct = equalityCondition(criteriaBuilder, paramKeyVal, keyOpValue);
				}else if(keyOpValue.length > 2){
					correct = otherCondition(criteriaBuilder, paramKeyVal);
				}else{
					correct = false;
				}
				if(!correct){
					logger.warn("Found isnvalid criteria, skipping : {}", paramKeyVal);
				}

				if(correct && i <= (criteriaParams.length-1)){
					criteriaBuilder.append(", ");
				}
			}
			criteriaStr = criteriaBuilder.toString();
			if(StringUtils.endsWith(criteriaStr, ", ")){
				criteriaStr = criteriaStr.substring(0, criteriaStr.length()- 2);
			}
			logger.info("Constructed mongo criteria : {}", criteriaStr);
		}

		this.mongoFilters = criteriaStr;
		if(StringUtils.isNotBlank(mongoFilters)){
			this.applyFilter = true;
		}
		return criteriaStr;
	}

	//Date field with single in-equality operator/range format
	public boolean otherCondition(StringBuilder criteriaBuilder, String paramKeyVal) {
		boolean correct = true;
		String[] rangesValues = StringUtils.split(paramKeyVal, SEPARATOR_VALUE);
		//FORMAT created:gte:2014-09-08
		if(rangesValues.length == 1){
			correct = singleDateEquility(criteriaBuilder, paramKeyVal);
		//FORMAT created:gte:2014-09-08,lte:2015-09-08	
		}else if(rangesValues.length == 2){
			correct = rangeDate(criteriaBuilder, rangesValues);
		}
		return correct;
	}

	//FORMAT created:gte:2014-09-08,lte:2015-09-08
	public boolean rangeDate(StringBuilder criteriaBuilder, String[] rangesValues) {
		boolean correct = false;
		String atrName=null, fop=null, fdateStr=null, sop=null, sdateStr = null;
		//first part containing key, operator and value
		String parts[] = StringUtils.split(rangesValues[0], SEPARATOR_OP);
		if(parts.length == 3){
			String key = parts[0];
			String op = parts[1];
			String value = parts[2];
			Params keyType = Params.getType(key);
			Operators opType = Operators.getType(op);
			if(keyType != null && opType!= null) {
				if(keyType == Params.CREATED || keyType == Params.PUBLISHED){
					atrName = PARAMS_MAPPING[keyType.ordinal()];
					queryKVMap.put(atrName, value);
					fop = OPERATOR_MAPPING[opType.ordinal()];
					fdateStr = " {$date:'" + String.format(DATE_FORMAT, value) + "'}";
					correct = true;
				}
			}
		}
		if(correct){
			//other part containing operator and value
			correct = false;
			parts = StringUtils.split(rangesValues[1], SEPARATOR_OP);
			if(parts.length == 2){
				String op = parts[0];
				String value = parts[1];
				Operators opType = Operators.getType(op);
				if(opType!=null){
					sop = OPERATOR_MAPPING[opType.ordinal()];
					sdateStr = " {$date:'" + String.format(DATE_FORMAT, value) + "'}";
					correct = true;
				}
			}
		}
		if(correct){
			criteriaBuilder.append(String.format(QUERY_KEY_RANGE_VALUE, atrName, fop, fdateStr, sop, sdateStr));	
		}
		return correct;
	}

	//FORMAT created:gte:2014-09-08
	public boolean singleDateEquility(StringBuilder criteriaBuilder, String paramKeyVal) {
		boolean correct = false;
		String parts[] = StringUtils.split(paramKeyVal, SEPARATOR_OP);
		if(parts.length == 3){
			String key = parts[0];
			String op = parts[1];
			String value = parts[2];
			Params keyType = Params.getType(key);
			Operators opType = Operators.getType(op);
			if(keyType != null && opType!= null) {
				if(keyType == Params.CREATED 
						|| keyType == Params.PUBLISHED 
						|| keyType == Params.DUEDT 
						|| keyType == Params.COMPLETEDDT 
						|| keyType == Params.OPENDT){
					String atrName = PARAMS_MAPPING[keyType.ordinal()];
					queryKVMap.put(atrName, value);
					String opName = OPERATOR_MAPPING[opType.ordinal()];
					String dateStr = String.format(DATE_FORMAT, value);
					criteriaBuilder.append(String.format(QUERY_KEY_OP_VALUE, atrName, opName, " {$date:'" + dateStr + "'}"));
					correct = true;
				}
			}
		}
		return correct;
	}

	/**
	 * Parse parameter of format - 
	 *  key:value
	 *  key:value1,value2 
	 *  
	 */
	public boolean equalityCondition(StringBuilder criteriaBuilder, String paramKeyVal, String[] keyOpValue) {
		boolean correct = true;
		String keyParam = keyOpValue[0];
		Params keyType = Params.getType(keyParam);
		if(keyType != null){
			String atrName = PARAMS_MAPPING[keyType.ordinal()];
			String value = keyOpValue[1];
			queryKVMap.put(atrName, value);
			if(StringUtils.isNotBlank(value)){
				String values[] = StringUtils.split(value, SEPARATOR_VALUE);
				if(values!=null && values.length == 1){
					if(keyType == Params.CREATED || keyType == Params.PUBLISHED || keyType == Params.DUEDT || keyType == Params.COMPLETEDDT || keyType == Params.OPENDT ){
						String dateStr = String.format(DATE_FORMAT, value);
						criteriaBuilder.append(String.format(QUERY_KEY_VALUE, atrName, " {$date:'" + dateStr + "'}"));
					}else if(keyType == Params.STATUS || keyType == Params.CONTEXTTYPE){
						criteriaBuilder.append(String.format(QUERY_KEY_VALUE, atrName, value));
					}else{
						criteriaBuilder.append(String.format(QUERY_KEY_VALUE, atrName, "'" + value + "'"));
					}
				}else if(values!=null && values.length > 1){
					criteriaBuilder.append(String.format(QUERY_KEY_IN_VALUE, atrName, multiValParam(values, keyType)));
				}
			}
		}else{
			correct = false;
			logger.warn("Invalid criteria skipping : {}", paramKeyVal);
		}
		return correct;
	}
	
	/**
	 * convert multi values to an array 
	 */
	public String multiValParam(String values[], Params type){
		StringBuilder builder = new StringBuilder();
		switch (type) {
			case CREATED:
			case PUBLISHED:
			case DUEDT:
			case COMPLETEDDT:
			case OPENDT:
						for (int i = 0; i < values.length; i++) {
							String dateStr = String.format(DATE_FORMAT, values[i]);
							builder.append(" {$date:'" + dateStr + "'}");
							if (i < values.length - 1) {
								builder.append(",");
							}
						}
						break;
			case STATUS:
			case CONTEXTTYPE:
						for (int i = 0; i < values.length; i++) {
							builder.append(values[i]);
							if (i < values.length - 1) {
								builder.append(",");
							}
						}
						break;
			default:
						for (int i = 0; i < values.length; i++) {
							builder.append("'" + values[i] + "'");
							if (i < values.length - 1) {
								builder.append(",");
							}
						}
						break;
		}

		return builder.toString();
	}

	public Map<String, String> getQueryKVMap() {
		return queryKVMap;
	}

	public void setQueryKVMap(Map<String, String> queryKVMap) {
		this.queryKVMap = queryKVMap;
	}

	public static enum Operators {
		LESS_THAN("lt"), LESS_THAN_EQUAL("lte"), GREATER_THAN("gt"), GREATER_THAN_EQUAL("gte");
		
		private Operators(final String text) {
			this.text = text;
		}
		private final String text;

		public String getValue() {
			return text;
		}

		public static Operators getType(String value){
			Operators types [] = Operators.values();
			for(Operators type:types){
				if(StringUtils.equals(value, type.getValue())){
					return type;
				}
			}
			return null;
		}
	}
	
	public static enum Params {
		AUTHOR("author"), 
		CREATED("created"), 
		CATEGORY("category"), 
		TYPE("type"), 
		TAG("tag"), 
		STATUS("status"),
		PUBLISHED("published"),
		COLLABORATOR("collaborator"),
		COMPLETEDDT("completedDT"),
		DUEDT("dueDT"),
		OPENDT("openDT"),
		CONTEXTTYPE("contextType"),
		CONTEXTID("contextId"),
		PORTFOLIOID("portfolioId"),
		;
		
		private Params(final String text) {
			this.text = text;
		}
		private final String text;

		public String getValue() {
			return text;
		}
	
		public static Params  getType(String value){
			Params types [] = Params.values();
			for(Params type:types){
				if(StringUtils.equals(value, type.getValue())){
					return type;
				}
			}
			return null;
		}
	}
}