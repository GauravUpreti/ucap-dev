package com.rapidinstinct.ucap.plat.service.resource.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.resource.OrgCompanyDO;
import com.rapidinstinct.ucap.plat.data.mongo.repo.resource.op.OrgCompanyOperations;
import com.rapidinstinct.ucap.plat.service.resource.OrgCompanyService;

/**
 * @author Gaurav Upreti
 *
 */
@Service
public class OrgCompanyServiceImpl implements OrgCompanyService {

	@Autowired 	private OrgCompanyOperations ops;
	
	@Override
	public ServiceResponse read(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		OrgCompanyDO resDoc = null;
		resultCode = CSResultCodes.USER_ATTRIBUTES_NF.getValue();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			String curOrgId = user.getCurrentOrgId();
			String name = request.getRequestData(String.class);
			resDoc = ops.findCompany(name, curOrgId);
			resultCode = CSResultCodes.DOCUMENT_NOT_FOUND.getValue();
			if(resDoc != null){
				result = true;
				resultCode = CSResultCodes.DOCUMENT_FOUND.getValue();
			}		
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(resDoc);
		return response;
	}

	@Override
	public ServiceResponse add(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		OrgCompanyDO resDoc = null;
		resultCode = CSResultCodes.USER_ATTRIBUTES_NF.getValue();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			String curOrgId = user.getCurrentOrgId();
			String name = request.getRequestData(String.class);
			resDoc = ops.findCompany(name, curOrgId);
			resultCode = CSResultCodes.DOCUMENT_FOUND.getValue();
			if(resDoc == null){
				resDoc = ops.add(curOrgId, name, user.getUserId());
				result = true;
				resultCode = CSResultCodes.DOCUMENT_SAVED.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(resDoc);
		return response;
	}

	@Override
	public ServiceResponse getCompanies(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<OrgCompanyDO> resDocs = null;
		resultCode = CSResultCodes.USER_ATTRIBUTES_NF.getValue();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			String curOrgId = user.getCurrentOrgId();
			resDocs = ops.getCompanies(curOrgId);
			resultCode = CSResultCodes.DOCUMENT_NOT_FOUND.getValue();
			if(resDocs!=null){
				result = true;
				resultCode = CSResultCodes.DOCUMENT_FOUND.getValue();
			}		
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(resDocs);
		return response;
	}

	@Override
	public ServiceResponse searchCompanies(ServiceRequest request) {
		boolean result = false;
		String resultCode = null;
		List<OrgCompanyDO> resDocs = null;
		resultCode = CSResultCodes.USER_ATTRIBUTES_NF.getValue();
		if(request.userAttributeExists()){
			UserDetailsBO user = request.getUser();
			String curOrgId = user.getCurrentOrgId();
			String term = request.getRequestData(String.class);
			resDocs = ops.searchCompanies(term, curOrgId);
			resultCode = CSResultCodes.DOCUMENT_NOT_FOUND.getValue();
			if(resDocs!=null){
				result = true;
				resultCode = CSResultCodes.DOCUMENT_FOUND.getValue();
			}		
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(resDocs);
		return response;
	}

	@Override
	public ServiceResponse update(ServiceRequest request) {
		return createOpDeniedResp();
	}

	@Override
	public ServiceResponse delete(ServiceRequest request) {
		return createOpDeniedResp();
	}

	private ServiceResponse createOpDeniedResp() {
		return new ServiceResponse(false, CSResultCodes.DOCUMENT_OPERATION_DENIED.getValue());
	}

}
