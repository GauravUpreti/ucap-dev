package com.rapidinstinct.ucap.plat.service.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.cbo.CSConstants;
import com.rapidinstinct.ucap.plat.cbo.RBMConstants;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.NotificationsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.component.op.NotificationOperations;
import com.rapidinstinct.ucap.plat.service.component.NotificationService;

/**
 * 
 * @author Gaurav Upreti
 * 
 */

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired	private NotificationOperations ops; 
	@Override
	public ServiceResponse readNotifications(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<NotificationsDO> notifications = new ArrayList<NotificationsDO>();
		String studyName = request.getRequestData(String.class);
		if(studyName!=null)
		{
			notifications=ops.findNotificationsbyStudyName(studyName);
			result=true;
			resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(notifications);
		return response;
	}
	
	@Override
	public ServiceResponse readAllNotifications(ServiceRequest sreq) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		List<NotificationsDO> notifications = new ArrayList<NotificationsDO>();
		String userId = sreq.getUser().getUserId();
		if(userId!=null)
			{
				notifications=ops.findAllNotifications();
				result=true;
				resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
			}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(notifications);
		return response;
	}
	
	@Override
	public ServiceResponse updateNotification(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.DOCUMENT_SAVE_FAILED.getValue();
		NotificationsDO notification = request.getRequestData(NotificationsDO.class);
		if(notification!=null){
			NotificationsDO	notificationDO = ops.findByNotificationId(notification.getId());
			
			if(StringUtils.isNotBlank(notification.getUserComment())){
				notificationDO.setUserComments(notification.getUserComment());
			}
			
			if(notification.getStatus()==RBMConstants.StudyNotificationStatusCodes.PENDING.getValue()||notification.getStatus()==RBMConstants.StudyNotificationStatusCodes.ATTENDED.getValue()){
				notificationDO.setStatus(notification.getStatus());
			}
			
			ops.updateNotification(notificationDO);
			result=true;
			resultCode=CSConstants.CSResultCodes.DOCUMENT_SAVED.getValue();
		}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(notification);
		return response;
	}

	@Override
	public ServiceResponse countPendingNotifications(ServiceRequest request) {
		String studyName = request.getRequestData(String.class);
		String userId = request.getUser().getUserId();
		int notificationCount = 0;
		boolean result = false;
		String resultCode = CSConstants.CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(userId!=null && studyName!=null){
			notificationCount= ops.findPendingNotificationsbyStudyName(studyName, RBMConstants.StudyNotificationStatusCodes.PENDING.getValue()).size();
			result=true;
			resultCode = CSConstants.CSResultCodes.DOCUMENT_FOUND.getValue();
		}
		ServiceResponse response = new ServiceResponse(result,resultCode);
		response.addResponseData(notificationCount);
		return response;
	}
	
}
