package com.rapidinstinct.ucap.plat.service.component;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.service.CrudService;

/**
 * 
 * @author Gaurav Upreti
 *
 */


@Service
public interface ComponentService extends CrudService {
}
