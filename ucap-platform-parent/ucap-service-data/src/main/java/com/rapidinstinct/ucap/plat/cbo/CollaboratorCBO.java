package com.rapidinstinct.ucap.plat.cbo;

import org.apache.commons.lang3.StringUtils;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;

/**
 * @author Gaurav Upreti
 *
 */
public class CollaboratorCBO extends BaseBusinessObject {
	private static final long serialVersionUID = 1L;
	private String componentId;
	private String parentComponentId;
	private String collaboratorId;
	private String displayName;
	
	public CollaboratorCBO(){ }
	
	public CollaboratorCBO(String componentId, String collaboratorId){ 
		this.componentId = componentId;
		this.collaboratorId = collaboratorId;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getCollaboratorId() {
		return collaboratorId;
	}

	public void setCollaboratorId(String collaboratorId) {
		this.collaboratorId = collaboratorId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public boolean hasValidData(){
		return StringUtils.isNotBlank(collaboratorId) && StringUtils.isNotBlank(componentId);
	}

	public String getParentComponentId() {
		return parentComponentId;
	}

	public void setParentComponentId(String parentComponentId) {
		this.parentComponentId = parentComponentId;
	}
}
