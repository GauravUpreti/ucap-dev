package com.rapidinstinct.ucap.plat.cbo;

import org.apache.commons.lang3.StringUtils;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.data.mongo.collection.DocumentConstants.DocumentType;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class ComponentTagCBO extends BaseBusinessObject{

	private static final long serialVersionUID = 1L;
	
	private String documentId;
	private DocumentType documentType;
	private String tagName;

	public ComponentTagCBO(DocumentType documentType, String documentId, String tagName){
		this.documentId = documentId;
		this.documentType = documentType;
		this.tagName = tagName;
	}
	
	public String getDocumentId() {
		return documentId;
	}
	
	public DocumentType getDocumentType() {
		return documentType;
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public boolean hasValidData(){
		return StringUtils.isNotBlank(documentId) && StringUtils.isNotBlank(tagName);
	}

}
