package com.rapidinstinct.ucap.plat.bo.user;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginLocalDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginLocalDO.LoginEmail;


/**
 * 
 * @author Gaurav Upreti
 *
 */

public class UserLocalLoginBO extends BaseBusinessObject{
	
	private static final long serialVersionUID = 1L;

	private String userId;
	private String emailId;
	private String password;
	private String currentOrgId;
	
	public static final String MASK_PASSWORD = "****************";
	

	public UserLocalLoginBO(String userId){
		super();
		this.userId = userId;
	}

	public UserLocalLoginBO(String emailId, String password){
		super();
		this.emailId = emailId;
		this.password = password;
	}
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCurrentOrgId() {
		return currentOrgId;
	}

	public void setCurrentOrgId(String defaultOrgId) {
		this.currentOrgId = defaultOrgId;
	}

	public void maskPassword(){
		this.password = MASK_PASSWORD;
	}
	
	public UserLoginLocalDO toUserLoginLocalDO(){
		LoginEmail loginEmail = new LoginEmail(emailId);
		loginEmail.emailToLower();
		UserLoginLocalDO localLogin = new UserLoginLocalDO(loginEmail, this.password);
		return localLogin;
	}
	
}
