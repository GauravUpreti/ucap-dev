package com.rapidinstinct.ucap.plat.service.component;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * @author Gaurav Upreti
 *
 */

@Service
public interface DashBoardService {

	public ServiceResponse readSubjects(ServiceRequest sreq);

	public ServiceResponse readStudies(ServiceRequest sreq);

	public ServiceResponse readUserStudy(ServiceRequest sreq);

}
