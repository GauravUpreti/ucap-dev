package com.rapidinstinct.ucap.plat.service.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.OrgDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.UserOrgGroupsBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.bo.user.UserOrgGroupsBO.OrgGroup;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserMembershipRequestDO.Status;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public class UserOrgServiceImpl extends UserProfileServiceImpl {

	@Override
	public ServiceResponse acceptOrgInvite(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String memReqId = request.getRequestData(String.class);
		UserDetailsBO curUser = request.getUser();
		if(curUser!=null && StringUtils.isNotBlank(memReqId)){
			UserMembershipRequestDO memReq = membershipRepo.findOne(memReqId);
			if(memReq!=null){
				String orgId = memReq.getOrgId();
				String userId = memReq.getUserId();
				if(StringUtils.equals(userId, curUser.getUserId())){
					//add request org to user access list
					UserDetailsDO udo = userDetailsRepo.findOne(userId);
					if(!udo.isMemberOfOrg(orgId)){
						udo.addOrgId(orgId);
						udo.setModifiedDT(new Date());
						userDetailsRepo.save(udo);
						//assign default membership role
						OrganizationGroupsDO orgGroups = new OrganizationGroupsDO();
						orgGroups.setOrgId(orgId);
						orgGroups.addGroupId(UserGroupsDO.GRP_MEMBER);
						UserGroupsDO currentGroups = userGroupsRepo.findOne(userId);
						if(currentGroups != null){
							currentGroups.addOrganizationGroups(orgGroups);
							currentGroups.setModifiedDT(new Date());
						}else{
							currentGroups = new UserGroupsDO(userId);
							currentGroups.addOrganizationGroups(orgGroups);
						}
						userGroupsRepo.save(currentGroups);
 						result = true;
						resultCode = USResultCodes.USER_PROFILE_UPDATED.toString();
						membershipRepo.delete(memReqId);
					}
				}
			}
		}

		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}

	@Override
	public ServiceResponse rejectOrgInvite(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		String memReqId = request.getRequestData(String.class);
		UserDetailsBO curUser = request.getUser();
		if(curUser!=null && StringUtils.isNotBlank(memReqId)){
			UserMembershipRequestDO memReq = membershipRepo.findOne(memReqId);
			if(memReq!=null){
				String userId = memReq.getUserId();
				if(StringUtils.equals(userId, curUser.getUserId())){
					membershipRepo.delete(memReqId);
					result = true;
					resultCode = CSResultCodes.OPERATION_SUCCESS.toString();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		return response;
	}
	
	
	@Override
	public ServiceResponse getUserOrgDetails(ServiceRequest request) {
		boolean result = true;
		String resultCode = null;
		UserDetailsBO curUser = request.getUser();
		UserOrgGroupsBO orgGroupsBO = null;
		if(curUser != null){
			List<OrgGroup> orgGrpList = new ArrayList<OrgGroup>();
			//add accepted org membership details
			UserGroupsDO userGroupsDO = userGroupsRepo.findOne(curUser.getUserId());
			orgGroupsBO = new UserOrgGroupsBO();	
			if(userGroupsDO != null){
				orgGroupsBO.setUserId(curUser.getUserId());
				for(OrganizationGroupsDO orgGrpDO : userGroupsDO.getOrgGroups()){
					OrganizationDO orgDO = orgOps.findById(orgGrpDO.getOrgId());
					if(orgDO != null){
						OrgGroup orgGroup = new OrgGroup();
						orgGroup.setOrganization(OrgDetailsBO.fromDO(orgDO));
						orgGroup.setGroups(OrgGroup.from(orgGrpDO.getGrpIds()));
						orgGroup.setStatus(Status.ACCEPTED.getValue());
						orgGrpList.add(orgGroup);
					}
				}
				
			}
			//add new org membership request details
			List<UserMembershipRequestDO> memRequests = membershipRepo.findByUserIdAndStatus(curUser.getUserId(), Status.NEW.getValue());
			for(UserMembershipRequestDO memReq : memRequests){
				OrganizationDO orgDO = orgOps.findById(memReq.getOrgId());
				if(orgDO != null){
					OrgGroup orgGroup = new OrgGroup();
					orgGroup.setOrganization(OrgDetailsBO.fromDO(orgDO));
					orgGroup.setGroups(OrgGroup.from(memReq.getMembershipType(), Status.NEW.getValue()));
					orgGroup.setVtoken(memReq.getId());
					orgGrpList.add(orgGroup);
					orgGroup.setStatus(Status.NEW.getValue());
				}
			}
			orgGroupsBO.setOrgGroups(orgGrpList);
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(orgGroupsBO);
		return response;
	}

	@Override
	public ServiceResponse getUserWorkDetails(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse updateUserWorkDetails(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse addOrUpdateOrgEmails(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse getUserPreference(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

}