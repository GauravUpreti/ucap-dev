package com.rapidinstinct.ucap.plat.service.user;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public interface UserAuthService extends UserDetailsService {

	public ServiceResponse autheticate(ServiceRequest request);
	public ServiceResponse addCurrentOrgGroups(ServiceRequest request);
	
}
