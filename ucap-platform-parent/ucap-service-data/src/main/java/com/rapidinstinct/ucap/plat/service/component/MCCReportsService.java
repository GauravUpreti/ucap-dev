package com.rapidinstinct.ucap.plat.service.component;

import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;

/**
 *
 * @author Shamim Ahmad
 *
 */
@Service
public interface MCCReportsService extends ComponentService{

	public ServiceResponse readAll(ServiceRequest request); 
	
	public ServiceResponse readByModuleId(ServiceRequest request);
	
	public ServiceResponse readUserStudy(ServiceRequest request);
	
	public ServiceResponse updateActiveStudies(ServiceRequest request);
}