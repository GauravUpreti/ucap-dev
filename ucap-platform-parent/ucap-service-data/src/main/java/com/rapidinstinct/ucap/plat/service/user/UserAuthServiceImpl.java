package com.rapidinstinct.ucap.plat.service.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO.OrganizationGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserLoginDO.LoginStatus;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.OrganizationRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserLoginRepository;

/**
 * 
 * @author Gaurav Upreti
 *
 */

@Service
public class UserAuthServiceImpl implements UserAuthService {

	@Autowired private UserLoginRepository userLoginRepo;
	@Autowired private UserDetailsRepository userDetailsRepo;
	@Autowired private OrganizationRepository organizationRepo;
	@Autowired private UserGroupsRepository userGroupsRepo;
	@Autowired private UserRegistrationService regService;

	private UserLoginDO addUser(UserLoginDO document){
		return userLoginRepo.save(document);
	}

	private UserLoginDO findUserByEmailId(String emailId){
		UserLoginDO login = null;
		if(StringUtils.isNotBlank(emailId)){
			login = userLoginRepo.findByLocalEmailIdsEmailId(emailId);
		}
		return login;
	}

	@Override
	public ServiceResponse autheticate(ServiceRequest request) {
		boolean result = false;
		String resultCode = USResultCodes.USER_NOT_FOUND.getValue();
		UserDetailsBO userDetails = null;
		LoginBO localLogin = request.getRequestData(LoginBO.class);
		if(localLogin!=null){
			UserLoginDO loginDO = findUserByEmailId(localLogin.getEmailId());
			if(loginDO != null){
				if(loginDO.isActiveUser()){
					resultCode = USResultCodes.INVALID_CREDENTIAL.getValue();
					if(loginDO.getLocal().comparePassword(localLogin.getPassword())){
						localLogin.maskPassword();
						addUser(loginDO);
						//get user profile details
						userDetails = new UserDetailsBO(loginDO.getId());
						userDetails.setEmail(localLogin.getEmailId());
						loadUserDetails(userDetails);
						result = true;
						resultCode = USResultCodes.LOGIN_SUCCESS.getValue();
					}
				}else if(loginDO.getStatus() ==  LoginStatus.VERIFICATION_PENDING.getValue()){
					resultCode = USResultCodes.VERIFICATION_PENDING.getValue();
					localLogin.setUserId(loginDO.getId());
					//check for invitation token - currently membership token 
					if(localLogin.isValidInviteRequest()){
						ServiceRequest srequest = new ServiceRequest();
						srequest.addRequestData(localLogin);
						ServiceResponse sresponse = regService.activateInvitedLogin(request);
						result = sresponse.getServiceResult().isResult();
						resultCode = sresponse.getServiceResult().getResultCode();
						if(result){
							//get user profile details
							userDetails = new UserDetailsBO(loginDO.getId());
							userDetails.setEmail(localLogin.getEmailId());
							loadUserDetails(userDetails);
							result = true;
							resultCode = USResultCodes.LOGIN_SUCCESS.getValue();
						}
					}
				}else if(loginDO.getStatus() ==  LoginStatus.INACTIVE.getValue()){
					resultCode = USResultCodes.USER_INACTIVATED.getValue();
				}
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(userDetails);
		return response;
	}


	//user name is email id in case of local login,
	@Override
	public UserDetails loadUserByUsername(String username)	throws UsernameNotFoundException {
		UserLoginDO loginDO = findUserByEmailId(username);
		if(loginDO == null){
			throw new UsernameNotFoundException("User name not found-->" + username);
		}

		UserDetailsBO user = new UserDetailsBO(loginDO.getId());
		user.setEmail(username);
		if(!loginDO.isActiveUser()){
			user.setEnabled(false);
		}else if(loginDO.isActiveUser()){
			loadUserDetails(user);
		}
		return user;
	}

	private void loadUserDetails(UserDetailsBO user) {
		UserDetailsDO detailsDO = userDetailsRepo.findOne(user.getUserId());
		if(detailsDO != null){
			user.setFirstName(detailsDO.getFirstName());
			user.setLastName(detailsDO.getLastName());
		}
	}

	@Override
	public ServiceResponse addCurrentOrgGroups(ServiceRequest request) {
		UserDetailsBO curUser = request.getRequestData(UserDetailsBO.class);
		boolean result = false;
		String resultCode = null;
		resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		if(curUser != null){
			loadUserDetails(curUser);
			UserGroupsDO userGroups = userGroupsRepo.findOne(curUser.getUserId());
			resultCode = USResultCodes.USER_GROUPS_NOT_FOUND.getValue();
			if(userGroups != null){
				for(OrganizationGroupsDO orgGrp : userGroups.getOrgGroups()){
					if(orgGrp.getOrgId().equals(curUser.getCurrentOrgId())){
						for(String grp : orgGrp.getGrpIds()){
							result = true;
							Role role = Role.getType(grp);
							if(role!=null){
								curUser.addRole(role);
							}
						}
						break;
					}
				}
			}
			if(result){
				resultCode = USResultCodes.USER_GROUPS_FOUND.getValue();
			}
		}
		
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(curUser);
		return response;
	}
}