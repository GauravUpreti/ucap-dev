package com.rapidinstinct.ucap.plat.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestTemplate;

import com.rapidinstinct.ucap.plat.commons.util.GsonHelper;

/**
 * @author Gaurav Upreti
 *
 */

@Component
public class MailServiceHelper {
	private static Logger logger = LoggerFactory.getLogger(MailServiceHelper.class);
	
	@Value("${mail.serviceURL}") private String serviceURL;
	@Value("${mail.defaultFromId}") private String defaultMailFrom;
	@Value("${ui.verifyURL}") private String uiAppPrefix;
	@Value("${ui.loginURL}") private String uiAppLogin;
	
	//TODO Configure HTTPClient for REST template 
	private AsyncRestTemplate mailInvoker = new AsyncRestTemplate();;
	private HttpHeaders headers = new HttpHeaders();

	
	public static final String UREG_VERIFY_TPL = "ureg_verification";
	public static final String UREG_SUCCESS_TPL = "ureg_success";
	public static final String UPASS_TEMP_TPL = "upass_temp";
	public static final String OREG_VERIFY_TPL = "oreg_verification";
	public static final String ORG_MEMREQ_TPL = "org_mem_req";
	public static final String REG_INVITE_TPL = "reg_invite";
	public static final String ADD_EMAIL_TPL = "mail_verification";
	public static final String EMAIL_ADD_VERIFY = "org_mem_register";

	public static final String REG_SUBJECT = "Ucap Registration Verification";
	public static final String PASSWORD_SUBJECT = "Ucap Password Recovery";
	public static final String ORG_REG_SUBJECT = "Ucap Organization Registration Verification";
	public static final String ORG_MEMREQ_SUBJECT = "Ucap Organization Membership Invite";
	public static final String REG_INVITE_SUBJECT = "You are invited to join Ucap";
	public static final String ADD_EMAIL_SUBJECT = "A new email address was added to your account";
	public static final String EMAIL_ADD_VERIFY_SUBJECT = "You have been Invited";

	public static enum TplParams{
		title, verURL, loginURL, tmpPassword, acceptURL, rejectURL, regInviteURL, name, userName, orgName, inviteURL
	}

	public MailServiceHelper(){
		headers.setContentType(MediaType.APPLICATION_JSON);	
	}

	public void sendEmailVerification(String token, String userMailId, String fname){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		String verURL = uiAppPrefix + "?token=" + token + "&email=" + userMailId;
		bindParams.put(TplParams.loginURL.name(), verURL);
		bindParams.put(TplParams.title.name(), ADD_EMAIL_SUBJECT);
		bindParams.put(TplParams.name.name(), fname);
		sendMail(userMailId, ADD_EMAIL_SUBJECT, ADD_EMAIL_TPL, bindParams);
	}

	
	public void sendRegInviteMail(String token, String userMailId){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		String verURL = uiAppLogin + "#/signupform/" + token + "/" + userMailId;
		bindParams.put(TplParams.regInviteURL.name(), verURL);
		bindParams.put(TplParams.title.name(), REG_INVITE_SUBJECT);
		sendMail(userMailId, REG_INVITE_SUBJECT, REG_INVITE_TPL, bindParams);
	}

	public void sendOrgMemReqMail(String token, String userMailId, String orgName){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		String verURL = uiAppLogin + "#/people/invitation/";
		if(StringUtils.isNotBlank(token)){
			verURL = verURL + userMailId + "/" + token;
		}else{
			verURL = verURL + userMailId;
		}
		bindParams.put(TplParams.orgName.name(), orgName);
		bindParams.put(TplParams.inviteURL.name(), verURL);
		bindParams.put(TplParams.title.name(), ORG_MEMREQ_SUBJECT);
		sendMail(userMailId, ORG_MEMREQ_SUBJECT, ORG_MEMREQ_TPL, bindParams);
	}

	public void sendOrgMemReqMail(String userMailId, String orgName){
		sendOrgMemReqMail(null, userMailId, orgName);
	}

	public void sendTempPassMail(String tmpPass, String userMailId){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		bindParams.put(TplParams.loginURL.name(), uiAppLogin);
		bindParams.put(TplParams.tmpPassword.name(), tmpPass);
		bindParams.put(TplParams.title.name(), PASSWORD_SUBJECT);
		sendMail(userMailId, PASSWORD_SUBJECT, UPASS_TEMP_TPL, bindParams);
	}
	
	public void sendRegVerMail(String token, String userMailId){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		String verURL = uiAppPrefix + "?token=" + token;
		bindParams.put(TplParams.verURL.name(), verURL);
		bindParams.put(TplParams.title.name(), REG_SUBJECT);
		sendMail(userMailId, REG_SUBJECT, UREG_VERIFY_TPL, bindParams);
	}

	public void sendRegVerMail(String token, String userMailId, String tmpPassword){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		String verURL = uiAppLogin + "#/people/activate-login/" + token;
		bindParams.put(TplParams.userName.name(), userMailId);
		bindParams.put(TplParams.tmpPassword.name(), tmpPassword);
		bindParams.put(TplParams.title.name(), EMAIL_ADD_VERIFY_SUBJECT);
		bindParams.put(TplParams.loginURL.name(), verURL);
		sendMail(userMailId, EMAIL_ADD_VERIFY_SUBJECT, EMAIL_ADD_VERIFY, bindParams);
	}

	public void sendOrgRegVerMail(String token, String userMailId){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		String verURL = uiAppPrefix + "?token=" + token;
		bindParams.put(TplParams.verURL.name(), verURL);
		bindParams.put(TplParams.title.name(), ORG_REG_SUBJECT);
		sendMail(userMailId, ORG_REG_SUBJECT, OREG_VERIFY_TPL, bindParams);
	}

	public void sendMail(String mailTo, String subject, String tplName, Map<String, Object> bindParams) {
		logger.info("initiated mail send request for email id:{}",  mailTo);
		MailInfo request = new MailInfo(defaultMailFrom, mailTo, subject, tplName);
		request.setMessageParams(bindParams);
		logger.debug("Mail Info Request:{}",  request);
		HttpEntity<String> requestEntity = new HttpEntity<String>(GsonHelper.toJsonAll(request),headers);
		mailInvoker.exchange(serviceURL, HttpMethod.POST, requestEntity, String.class);
	}
}
