package com.rapidinstinct.ucap.plat.bo.user;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;

/**
 * @author Gaurav Upreti
 *
 */

public class UserGroupsBO extends BaseBusinessObject{
	private static final long serialVersionUID = 1L;
	private String userId;
	private String group = null;
	private Set<String> groups = new HashSet<String>();
	
	private String emailId;
	
	public UserGroupsBO(){ }
	
	public UserGroupsBO(String userId){ 
		this.userId = userId;
	}

	public UserGroupsBO(String userId, String groupId){ 
		this.userId = userId;
		this.group = groupId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Set<String> getGroups() {
		return groups;
	}

	public void setGroups(Set<String> groups) {
		this.groups = groups;
	}
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public boolean addGroupId(String grpId){
		if(StringUtils.isNotBlank(grpId)){
			return this.groups.add(grpId);	
		}
		return false;
	}

	public boolean removeGroupId(String grpId){
		if(StringUtils.isNotBlank(grpId)){
			return this.groups.remove(grpId);	
		}
		return false;
	}

	public String getEmailId() {
		if(StringUtils.isNotBlank(emailId)){
			return StringUtils.lowerCase(emailId);
		}
		return emailId;
	}

	public void setEmailId(String emailId) {
		if(StringUtils.isNotBlank(emailId)){
			this.emailId = StringUtils.lowerCase(emailId);
		}
		this.emailId = emailId;
	}
}
