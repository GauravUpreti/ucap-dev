package com.rapidinstinct.ucap.plat.bo.user;

import org.apache.commons.lang3.StringUtils;

import com.rapidinstinct.ucap.plat.bo.BaseBusinessObject;

/**
 * @author Gaurav Upreti
 *
 */
public class LoginBO extends BaseBusinessObject {
	
	private static final long serialVersionUID = 1L;


	private String userId;
	private String emailId;
	private String password;
	private String inviteToken; //field applicable if registration is invitation based
	public static final String MASK_PASSWORD = "****************";
	private String firstName;
	private String lastName;
	private String job;
	private String function;
	private int phone;
	private String company;
	private String country;
	private String comments;

	public LoginBO(){ }
	
	public LoginBO(String emailId, String password){
		this.emailId = StringUtils.lowerCase(emailId);
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = StringUtils.lowerCase(emailId);
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void maskPassword(){
		this.password = MASK_PASSWORD;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getInviteToken() {
		return inviteToken;
	}

	public void setInviteToken(String inviteToken) {
		this.inviteToken = inviteToken;
	}
	
	public boolean hasInviteToken(){
		return StringUtils.isNotBlank(inviteToken);
	}

	public boolean hasUserId(){
		return StringUtils.isNotBlank(userId);
	}

	public boolean hasPassword(){
		return StringUtils.isNotBlank(password);
	}

	public boolean hasEmailId(){
		return StringUtils.isNotBlank(emailId);
	}
	
	public boolean isValidInviteRequest(){
		return hasEmailId() && hasPassword() && hasUserId();
	}
}
