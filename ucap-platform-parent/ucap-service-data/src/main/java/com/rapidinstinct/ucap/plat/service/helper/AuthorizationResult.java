package com.rapidinstinct.ucap.plat.service.helper;

/**
 * 
 * @author Gaurav Upreti
 *
 */

public class AuthorizationResult {
	private boolean accessible;
	private String resultCode = null;
	
	
	public AuthorizationResult(){
		this.accessible = false;
	}
	
	public AuthorizationResult(boolean accessible, String resultCode){
		this.accessible = accessible;
		this.resultCode = resultCode;
	}
	
	public boolean isAccessible() {
		return accessible;
	}
	
	public String getResultCode() {
		return resultCode;
	}

	public void setAccessible(boolean accessible) {
		this.accessible = accessible;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

}
