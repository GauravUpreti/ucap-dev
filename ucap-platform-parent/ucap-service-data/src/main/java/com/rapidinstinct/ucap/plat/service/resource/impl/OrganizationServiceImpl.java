package com.rapidinstinct.ucap.plat.service.resource.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.cbo.CSConstants.CSResultCodes;
import com.rapidinstinct.ucap.plat.data.mongo.collection.security.UserGroupsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.UserDetailsDO;
import com.rapidinstinct.ucap.plat.data.mongo.collection.user.OrganizationDO.OrgStatus;
import com.rapidinstinct.ucap.plat.data.mongo.repo.security.UserGroupsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.UserDetailsRepository;
import com.rapidinstinct.ucap.plat.data.mongo.repo.user.op.OrganizationOperations;
import com.rapidinstinct.ucap.plat.service.resource.OrganizationService;

/**
 * @author Shiva Kalgudi
 *
 */
@Service
public class OrganizationServiceImpl implements OrganizationService {
	
	@Autowired private OrganizationOperations ops;
	@Autowired private UserDetailsRepository userDetailsRepo;
	@Autowired protected UserGroupsRepository userGroupsRepo;
	


	@Override
	public ServiceResponse add(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		OrganizationDO reqOrg = request.getRequestData(OrganizationDO.class);
		UserDetailsBO curUser = request.getUser();
		OrganizationDO resultOrg = null;
		if(curUser != null && reqOrg != null && reqOrg.getPaymentInfo() != null){
			String userId = curUser.getUserId();
			resultCode = CSResultCodes.DOCUMENT_SAVE_FAILED.getValue();
			//make sure id is not set with junk value - let mongo autogenerate value
			reqOrg.setId(null);
			reqOrg.setOwnerId(userId);
			reqOrg.setCreatorId(userId);
			reqOrg.setStatus(OrgStatus.ACTIVE.getValue());
			resultOrg = ops.add(reqOrg);
			if(resultOrg != null){
				String orgId = resultOrg.getId();
				result = true;
				resultCode = CSResultCodes.DOCUMENT_SAVED.getValue();
				addOrgAdmin(userId, orgId);
			}
		}

		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(resultOrg);
		return response;
	}

	private void addOrgAdmin(String userId, String orgId) {
		//Add user as org admin
		UserDetailsDO userDT = userDetailsRepo.findOne(userId);
		if(userDT != null){
			userDT.addOrgId(orgId);
			userDT.setModifiedDT(new Date());
			userDetailsRepo.save(userDT);
		}
		
		UserGroupsDO userGrps = userGroupsRepo.findOne(userId);
		if(userGrps != null){
			userGrps.addOrganizationGroups(orgId, UserGroupsDO.GRP_ADMIN);
			userGrps.addOrganizationGroups(orgId, UserGroupsDO.GRP_MEMBER);
			userGrps.setModifiedDT(new Date());
			userGroupsRepo.save(userGrps);
		}
	}

	@Override
	public ServiceResponse update(ServiceRequest request) {
		boolean result = false;
		String resultCode = CSResultCodes.REQUEST_DATA_INVALID.getValue();
		OrganizationDO reqOrg = request.getRequestData(OrganizationDO.class);
		UserDetailsBO curUser = request.getUser();
		OrganizationDO resultOrg = null;
		if(curUser != null && reqOrg != null) {
			resultOrg = ops.findById(reqOrg.getId());
			resultCode = CSResultCodes.DOCUMENT_NOT_FOUND.getValue();
			if(resultOrg != null && resultOrg.isOwner(curUser.getUserId())){
				resultOrg.setModifiedDT(new Date());
				resultOrg.setName(reqOrg.getName());
				resultOrg.setDescription(reqOrg.getDescription());
				resultOrg.setEmail(reqOrg.getEmail());
				resultOrg.setWebsite(reqOrg.getWebsite());
				resultOrg.setAddress(reqOrg.getAddress());
				if(reqOrg.getAlternateEmailIds()!=null && reqOrg.getAlternateEmailIds().size() > 0){
					resultOrg.setAlternateEmailIds(reqOrg.getAlternateEmailIds());
				}
				if(reqOrg.getPhones()!=null && reqOrg.getPhones().size() > 0){
					resultOrg.setPhones(reqOrg.getPhones());
				}
				ops.update(resultOrg);
				result = true;
				resultCode = CSResultCodes.DOCUMENT_SAVED.getValue();
			}else if(resultOrg != null){
				resultCode = CSResultCodes.DOCUMENT_NOT_ACCESSIBLE.getValue();
			}
		}
		ServiceResponse response = new ServiceResponse(result, resultCode);
		response.addResponseData(resultOrg);
		return response;
	}



	@Override
	public ServiceResponse verifyRegistration(ServiceRequest request) {
		return new ServiceResponse(false, CSResultCodes.DOCUMENT_OPERATION_DENIED.getValue());	
	}

	@Override
	public ServiceResponse delete(ServiceRequest request) {
		return new ServiceResponse(false, CSResultCodes.DOCUMENT_OPERATION_DENIED.getValue());	
	}

	@Override
	public ServiceResponse read(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponse updatePaymentInfo(ServiceRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
}