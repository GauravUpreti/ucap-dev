package com.rapidinstinct.ucap.plat.service.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rapidinstinct.ucap.plat.service.MailServiceHelper;
import com.rapidinstinct.ucap.plat.service.MailServiceHelper.TplParams;

/**
 * @author Shiva Kalgudi
 *
 */

@ContextConfiguration({"/com.rapidinstinct.ucap.plat.service.config/service-beans.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class MailServiceInvokerTest {

	@Autowired private MailServiceHelper invoker;
	
	//@Test
	public void send(){
		Map<String, Object> bindParams = new HashMap<String, Object>();
		bindParams.put(TplParams.verURL.name(), "http://test");
		bindParams.put(TplParams.title.name(), MailServiceHelper.REG_SUBJECT);
		invoker.sendMail("kalgudi@gmail.com", MailServiceHelper.REG_SUBJECT, MailServiceHelper.UREG_VERIFY_TPL, bindParams);
	}
	
	@Test
	public void sendRegInvite(){
		String token = "1234r";
		String email = "shiva.kalgudi@wincere.com";
		invoker.sendRegVerMail(token, email, "123");
		
	}
	

}
