package com.rapidinstinct.ucap.plat.cbo.test;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.rapidinstinct.ucap.plat.cbo.CSearchCriteria;
import com.rapidinstinct.ucap.plat.cbo.CSearchCriteria.Params;

/**
 * @author Shiva Kalgudi
 *
 */
public class CSearchCriteriaTest {
	
	@Test
	public void testCriteriaParse(){
		CSearchCriteria criteria = new CSearchCriteria("welcome", "tag1s:welcome,project|author:skalgudi,sghosh|status:10,60|published:gte:2014-09-08,lte:2015-09-08");
		System.out.println(criteria.parseCriteria());
		Map<String, String> kvs = criteria.getQueryKVMap();
		for(String key : kvs.keySet()){
			System.out.println(key + "-" + kvs.get(key));
		}
	}

	//@Test
	public void testCriteriaParseEmtpyStr(){
		CSearchCriteria criteria = new CSearchCriteria("", "");
		System.out.println(criteria.parseCriteria().length());
	}

	//@Test
	public void testCriteriaParseSingle(){
		CSearchCriteria criteria = new CSearchCriteria("", "tag:welcome");
		System.out.println(criteria.parseCriteria());
	}
	
	//@Test
	public void multiValParam(){
		String cr[] = {"welcome","don"};
		CSearchCriteria criteria = new CSearchCriteria();
		System.out.println(criteria.multiValParam(cr, Params.TAG));
		String[] status = {"10","60"};
		System.out.println(criteria.multiValParam(status, Params.STATUS));
		String[] dates = {"2014-09-08","2014-09-10","2014-09-11"};
		System.out.println(criteria.multiValParam(dates, Params.CREATED));
	}
	
	//@Test
	public void equalityCondition(){
		CSearchCriteria criteria = new CSearchCriteria();
		StringBuilder builder = new StringBuilder();
		//String paramKeyVal="type:textbox,checkbox";
		//String paramKeyVal="created:2014-09-08,2014-09-10";
		//String paramKeyVal="status:10,60";
		//String paramKeyVal="status:10";
		//String paramKeyVal="type:textbox";
		//String paramKeyVal="created:2014-09-08";
		String paramKeyVal="published:2014-09-08";
		String values[] = StringUtils.split(paramKeyVal, ":");
		System.out.println(criteria.equalityCondition(builder, paramKeyVal, values));
		System.out.println(builder.toString());
	}
	
	//@Test
	public void otherCondition(){
		CSearchCriteria criteria = new CSearchCriteria();
		String paramKeyVals[]={
				"created:gte:2014-09-08", 
				"created:gt:2014-09-08",
				"created:lte:2014-09-08",
				"created:lt:2014-09-08", 
				"created:gte:2014-09-08,lte:2015-09-08",
				"published:gte:2014-09-08", 
				"published:gt:2014-09-08",
				"published:lte:2014-09-08",
				"published:lt:2014-09-08", 
				"published:gte:2014-09-08,lte:2015-09-08",
				"published1:gte:2014-09-08,lte:2015-09-08"
				};
		for(String paramKeyVal:paramKeyVals){
			StringBuilder builder = new StringBuilder();
			criteria.otherCondition(builder, paramKeyVal);	
			System.out.println(builder.toString());
		}
	}
}
