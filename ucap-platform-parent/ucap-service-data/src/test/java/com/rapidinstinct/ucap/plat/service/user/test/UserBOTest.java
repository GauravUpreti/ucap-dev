package com.rapidinstinct.ucap.plat.service.user.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.Role;
import com.rapidinstinct.ucap.plat.commons.util.GsonHelper;

/**
 * @author Shiva Kalgudi
 *
 */
public class UserBOTest {

	@Test
	public void hasRole() {
		UserDetailsBO details = new UserDetailsBO();
		details.addRole(Role.ROLE_ADMIN);
		details.addRole(Role.ROLE_USER);
		Assert.assertTrue(details.isOrgAdmin());
		Assert.assertFalse(details.isOrgKM());
	}

	@Test
	public void toJSON() {
		LoginBO loginReq = new LoginBO("kalgudi@gmail.com", "welcome");
		System.out.println("JSON-->" + GsonHelper.toJson(loginReq));
	}
	
	@Test
	public void testDate() {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		System.out.println(f.format(new Date()));
	}
}
