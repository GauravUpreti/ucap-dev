package com.rapidinstinct.ucap.plat.service.user.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.AccessAttributes;
import com.rapidinstinct.ucap.plat.bo.user.USConstants.USResultCodes;
import com.rapidinstinct.ucap.plat.service.test.BaseServiceTest;
import com.rapidinstinct.ucap.plat.service.user.UserManagementService;

/**
 * 
 * @author Shiva Kalgudi
 *
 */

public class UserManagementServiceTest extends BaseServiceTest{

	@Autowired UserManagementService userService;
	
	@Test
	public void isEmailInUse(){
		
		ServiceRequest request = new ServiceRequest();
		request.addAttribute(AccessAttributes.EMAIL_ID.getValue(), "Shivakumar.Kalgudi@wincere.com");		
		ServiceResponse response = userService.isEmailInUse(request);
		
		Assert.assertTrue(response.getServiceResult().isResult());
		Assert.assertTrue(response.getServiceResult().getResultCode() == USResultCodes.DUPLICATE_EMAIL.getValue());
		

		request.addAttribute(AccessAttributes.EMAIL_ID.getValue(), "skalgudi@wincere.com");		
		response = userService.isEmailInUse(request);
		Assert.assertFalse(response.getServiceResult().isResult());
		Assert.assertTrue(response.getServiceResult().getResultCode() == USResultCodes.EMAIL_NOT_FOUND.getValue());
	}

	
	@Test
	public void registerLocalUser(){
		LoginBO localLogin = new LoginBO("sandeep.bhat@wincere.com", "welcome");
		ServiceRequest request = new ServiceRequest();
		request.addRequestData(localLogin);
		ServiceResponse response = userService.registerUser(request);
		Assert.assertTrue(response.getServiceResult().getResultCode() == USResultCodes.REGISTRATION_SUCCESS.getValue());
		Assert.assertTrue(response.getServiceResult().isResult());

	}
	
	/*	@Test
	public void checkLocalLogin(){
		UserLocalLoginBO localLogin = new UserLocalLoginBO("Vicky.Kak@wincere.com", "welcome");
		UserLocalLoginBO result = userService.checkLocalLogin(localLogin);
		Assert.assertTrue(result.getServiceResultCode().equals(UserServiceResultCodes.LOGIN_SUCCESS.getValue()));
		
		localLogin = new UserLocalLoginBO("Vicky.Kak@wincere.com", "welcome1");
		result = userService.checkLocalLogin(localLogin);
		Assert.assertTrue(result.getServiceResultCode().equals(UserServiceResultCodes.INVALID_CREDENTIAL.getValue()));

		localLogin = new UserLocalLoginBO("surrya.ghosh@wincere.com", "welcome");
		result = userService.checkLocalLogin(localLogin);
		//Assert.assertTrue(result.getServiceResultCode().equals(UserServiceResultCodes.USER_INACTIVATED.getValue()));

		localLogin = new UserLocalLoginBO("surrya.ghosh1@wincere.com", "welcome");
		result = userService.checkLocalLogin(localLogin);
		Assert.assertTrue(result.getServiceResultCode().equals(UserServiceResultCodes.USER_NOT_FOUND.getValue()));

	}

	@Test
	public void updateUserProfile(){
		UserProfileBO profile  = new UserProfileBO();
		profile.setUserName("Vicky.Kak@wincere.com");
		profile.setFirstName("Vicky");
		profile.setLastName("Kak");
		UserProfileBO result = userService.updateUserProfile(profile);
		Assert.assertTrue(result.getServiceResultCode().equals(UserServiceResultCodes.USER_PROFILE_UPDATED.getValue()));
		
		profile = new UserProfileBO();
		profile.setUserName("surrya.ghosh@wincere.com");
		profile.setFirstName("Suryya");
		profile.setLastName("Ghosh");
		result = userService.updateUserProfile(profile);
		//Assert.assertTrue(result.getServiceResultCode().equals(UserServiceResultCodes.USER_INACTIVATED.getValue()));
	}
	

*/}
