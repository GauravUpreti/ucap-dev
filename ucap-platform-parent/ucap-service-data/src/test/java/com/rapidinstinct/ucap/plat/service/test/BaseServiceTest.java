package com.rapidinstinct.ucap.plat.service.test;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rapidinstinct.ucap.plat.bo.ServiceRequest;
import com.rapidinstinct.ucap.plat.bo.ServiceResponse;
import com.rapidinstinct.ucap.plat.bo.user.LoginBO;
import com.rapidinstinct.ucap.plat.bo.user.UserDetailsBO;
import com.rapidinstinct.ucap.plat.service.user.UserManagementService;

/**
 * 
 * @author Shiva Kalgudi
 *
 */
@ContextConfiguration({"/com.rapidinstinct.ucap.plat.service.config/service-beans.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class BaseServiceTest {
	
	protected UserDetailsBO localLogin = null;
	@Autowired UserManagementService userService;

	@Before
	public void init(){
		LoginBO ll = new LoginBO("shivakumar.kalgudi@wincere.com", "welcome");
		ServiceRequest request = new ServiceRequest();
		request.addRequestData(ll);
		ServiceResponse response = userService.autheticate(request);
		localLogin = response.getResponseData(UserDetailsBO.class);
		localLogin.setCurrentOrgId("wincere");
		request.addRequestData(localLogin);
		userService.addUserOrgGroups(request);
	}	


}
