package com.rapidinstinct.ucap.plat.service.pg.paypal.test;

import org.junit.Assert;
import org.junit.Test;

import com.rapidinstinct.ucap.plat.service.pg.PaymentGatewayError;
import com.rapidinstinct.ucap.plat.service.pg.paypal.PayPalApiUtil;

/**
 * @author Shiva Kalgudi
 *
 */
public class PayPalApiUtilTest {

	String message = "Error code : 400 with response : {\"name\":\"VALIDATION_ERROR\",\"details\":[{\"field\":\"number\",\"issue\":\"Value is invalid\"}],\"message\":\"Invalid request - see details\",\"information_link\":\"https://developer.paypal.com/docs/api/#VALIDATION_ERROR\",\"debug_id\":\"5c16c7e108c14\"}";

	@Test
	public void parseMessage(){
		PayPalApiUtil ppau = new PayPalApiUtil();
		PaymentGatewayError em = ppau.extractErrorDetails(message);
		Assert.assertEquals("400", em.getCode());
		Assert.assertTrue(em.getMessage().contains("{"));
		System.out.println(em.getMessage());
	}
}
