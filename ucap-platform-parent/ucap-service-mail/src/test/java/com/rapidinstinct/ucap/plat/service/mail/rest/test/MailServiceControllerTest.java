package com.rapidinstinct.ucap.plat.service.mail.rest.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.rapidinstinct.ucap.plat.commons.util.GsonHelper;
import com.rapidinstinct.ucap.plat.service.mail.MailInfo;

public class MailServiceControllerTest extends AbstractContextControllerTests{

	
	@Test
	public void testSendMail() throws Exception{

		    this.mockMvc.perform(
		            post("/sendmail")
		                    .content(getMailRequestJSON())
		                    .contentType(MediaType.APPLICATION_JSON)
		                    .accept(MediaType.APPLICATION_JSON))
		    				.andDo(print())
		    				.andExpect(status().is(HttpStatus.OK.value()));

	}


	public static String getMailRequestJSON(){
		MailInfo sender = new MailInfo();
		sender.setTo("shiva.kalgudi@yahoo.com");
		sender.setFrom("kalgudi@gmail.com");
		sender.setTemplateName("welcome");
		sender.setSubject("welcome to UCAP");
		sender.setMessageId("123");
		return GsonHelper.toJson(sender);
	}

}
