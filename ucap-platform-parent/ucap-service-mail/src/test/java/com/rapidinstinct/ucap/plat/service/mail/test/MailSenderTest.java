package com.rapidinstinct.ucap.plat.service.mail.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rapidinstinct.ucap.plat.service.mail.MailInfo;
import com.rapidinstinct.ucap.plat.service.mail.MailSender;

@ContextConfiguration({"/com.rapidinstinct.ucap.plat.service.mail.config/mail-beans.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class MailSenderTest {
	
	@Autowired MailSender mailSender;

	@Test
	public void testMailSender(){
		MailInfo sender = new MailInfo();
		sender.setTo("shiva.kalgudi@yahoo.com");
		sender.setFrom("kalgudi@gmail.com");
		sender.setTemplateName("welcome");
		sender.setSubject("welcome to Ucap");
		sender.setMessageId("123");
		mailSender.send(sender);
	}
}
