package com.rapidinstinct.ucap.plat.service.rest.pg;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rapidinstinct.ucap.plat.service.pg.PaymentGatewayError;
import com.rapidinstinct.ucap.plat.service.pg.PaymentServiceException;

/**
 * @author Shiva Kalgudi
 *
 */

@ControllerAdvice
public class ServiceExceptionHandler {

    @ExceptionHandler(value = PaymentServiceException.class)
    public ResponseEntity<PaymentGatewayError> defaultErrorHandler(PaymentServiceException e) throws Exception {
    	PaymentGatewayError result = e.getError();
       	return new ResponseEntity<PaymentGatewayError>(result, HttpStatus.BAD_REQUEST);
    }
    
}
