package com.rapidinstinct.ucap.plat.service.pg.paypal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paypal.api.payments.CreditCard;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.rapidinstinct.ucap.plat.service.pg.CreditCardInfo;
import com.rapidinstinct.ucap.plat.service.pg.PaymentGatewayError;
import com.rapidinstinct.ucap.plat.service.pg.PaymentService;
import com.rapidinstinct.ucap.plat.service.pg.PaymentServiceException;

/**
 * @author Shiva Kalgudi
 *
 */
@Service
public class PayPalPaymentService implements PaymentService {
	private final static Logger logger = LoggerFactory.getLogger(PayPalApiUtil.class);

	@Autowired PayPalBeanMapper mapper;
	@Autowired PayPalApiUtil ppu;

	@Override
	public CreditCardInfo storeCreditCard(CreditCardInfo cardInfo) throws PaymentServiceException{
		CreditCard cc = mapper.createCreditCard(cardInfo);
		CreditCardInfo cci = null;
		try {
			APIContext apiContext = ppu.createApiContext();
			CreditCard ccc = cc.create(apiContext);
			if(ccc!=null){
				cci = mapper.convertCreditCard(ccc);
			}
		} catch (PayPalRESTException e) {
			handleException(e);
		}
		return cci;
	}

	@Override
	public CreditCardInfo readCreditCard(String cardId) throws PaymentServiceException{
		CreditCardInfo cci = null;
		try {
			APIContext apiContext = ppu.createApiContext();
			CreditCard cc = CreditCard.get(apiContext, cardId);
			if(cc!=null){
				cci = mapper.convertCreditCard(cc);
			}
		} catch (PayPalRESTException e) {
			handleException(e);
		}
		return cci;
	}

	private void handleException(PayPalRESTException e) throws PaymentServiceException {
		logger.error("Exception occurred while storing credit-card:" + e.getMessage());
		PaymentGatewayError em = ppu.extractErrorDetails(e.getMessage());
		PaymentServiceException ex = new PaymentServiceException(e);
		ex.setError(em);
		throw ex;
	}

}
