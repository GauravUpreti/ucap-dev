package com.rapidinstinct.ucap.plat.service.mail;

import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.rapidinstinct.ucap.plat.commons.util.ToStringBuilderFactory;


/**
 * 
 * @author Shiva Kalgudi
 *
 * Class defines attributes supported by mail Service
 */

public class MailInfo {
	
	//FROM Email Address
	private String from;
	//Single TOs Email Address
	private String to;
	//for mutliple TO addresses use this property instead of "to" 
	private String[] tos;
	//mail subject
	private String subject;
	//Single CC Email Address
	private String cc;
	//for mutliple CC addresses use this property instead of "cc"
	private String[] ccs;
	//Single BCC Email Address
	private String bcc;
	//for mutliple BCC addresses use this property instead of "bcc"
	private String[] bccs;
	//Mail template Name - dont include .ftl sufix
	private String templateName;
	//application calling the mail service
	private String applicationName;	
	//Message Id - Provide unique id to message - responsibility of caller
	private String messageId;
	//REPLY-TO Email
	private String replyTo;
	//mail priority, valid values (1-5) 
	private int priority;	
	//mail template binding data
	private Map<String, Object> messageParams;
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getCc() {
		return cc;
	}
	
	public void setCc(String cc) {
		this.cc = cc;
	}
	
	public String getBcc() {
		return bcc;
	}
	
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	
	public String getTemplateName() {
		return templateName;
	}
	
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Map<String, Object> getMessageParams() {
		return messageParams;
	}

	public void setMessageParams(Map<String, Object> messageParams) {
		this.messageParams = messageParams;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String[] getTos() {
		return tos;
	}

	public void setTos(String[] tos) {
		this.tos = tos;
	}

	public String[] getCcs() {
		return ccs;
	}

	public void setCcs(String[] ccs) {
		this.ccs = ccs;
	}

	public String[] getBccs() {
		return bccs;
	}

	public void setBccs(String[] bccs) {
		this.bccs = bccs;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	
	public String toString() {
		ToStringBuilder out = ToStringBuilderFactory.build(this);
		out.appendSuper(super.toString());
		out.append("from", this.getFrom());
		out.append("to", this.getTo());
		out.append("tos", this.getTos());
		out.append("cc", this.getCc());
		out.append("ccs", this.getCcs());
		out.append("bcc", this.getBcc());
		out.append("bccs", this.getBccs());
		out.append("templateName", this.getTemplateName());
		return out.toString();
	}

}

