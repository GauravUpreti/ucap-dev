package com.rapidinstinct.ucap.plat.service.rest.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rapidinstinct.ucap.plat.service.mail.MailInfo;
import com.rapidinstinct.ucap.plat.service.mail.MailSendResponse;
import com.rapidinstinct.ucap.plat.service.mail.MailSender;


/**
 * 
 * @author Shiva Kalgudi
 *
 */

@Controller
@RequestMapping("/sendmail")
public class MailServiceController {
	private final static Logger logger = LoggerFactory.getLogger(MailServiceController.class);
	@Autowired private MailSender mailSender;

	@RequestMapping(method = RequestMethod.POST, consumes="application/json", produces="application/json")
	public ResponseEntity<MailSendResponse> sendMail(@RequestBody MailInfo  mailInfo){
		logger.info("Mail Service Controller invoked for request :{}", mailInfo);
		boolean result = false;
		MailSendResponse response = new MailSendResponse();
		response.setMessageId(mailInfo.getMessageId());
    	HttpStatus status = HttpStatus.OK;
		try{
			result = mailSender.send(mailInfo);
		}catch(Exception e){
			logger.error("Error occurred while sending mail for :{}, Exception details :{}", mailInfo, e.toString());
			response.setResultMessage("Error occurred while sending mail");
			if(e instanceof IllegalArgumentException || e instanceof NullPointerException){
				status = HttpStatus.BAD_REQUEST;
			}else{
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}
		response.setResult(result);
		logger.info("Mail Service Controller processed request with result :{}", result);
		
		return new ResponseEntity<MailSendResponse>(response, status);
	}
	
}
