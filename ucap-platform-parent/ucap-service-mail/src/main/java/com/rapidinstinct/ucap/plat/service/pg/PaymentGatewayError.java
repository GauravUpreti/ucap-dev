package com.rapidinstinct.ucap.plat.service.pg;

/**
 * @author Shiva Kalgudi
 *
 */
public class PaymentGatewayError {
	private String code;
	private String message;

	public PaymentGatewayError(String code, String message){
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
