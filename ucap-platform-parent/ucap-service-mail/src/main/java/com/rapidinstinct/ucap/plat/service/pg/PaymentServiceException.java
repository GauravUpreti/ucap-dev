package com.rapidinstinct.ucap.plat.service.pg;

/**
 * @author Shiva Kalgudi
 *
 */
public class PaymentServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	private PaymentGatewayError error;
	
	public PaymentServiceException(String message) {
		super(message);
	}

	public PaymentServiceException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public PaymentServiceException(Throwable throwable) {
		super(throwable);
	}

	public PaymentGatewayError getError() {
		return error;
	}

	public void setError(PaymentGatewayError error) {
		this.error = error;
	}

}
