package com.rapidinstinct.ucap.plat.service.mail;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 
 * @author Shiva Kalgudi
 * 
 * Mail Sender Interface implementation class
 *
 */

@Service
public class MailSenderServiceImpl implements MailSender {	
	
	private final static Logger logger = LoggerFactory.getLogger(MailSenderServiceImpl.class);
	
	protected static final String MESSAGE_BODY_PARAM = "bodyContent";
	protected static final String MESSAGE_TITLE_PARAM = "title";
	protected static final String MESSAGE_TITLE_DEFAULT_VALUE = "Default Title";
	protected static final String MESSAGE_TEMPLATE_COMMON = "mail-common";
	
	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private Configuration fmConfig;
	

	/**
	 * Mail archistration method
	 */
	public boolean send(MailInfo mailInfo) {	
		boolean result = false;
		Assert.notNull(mailInfo, "Mail Information request is empty. Mail processing skipped");
		logger.info("Mail sending process is started for {}", mailInfo.getMessageId());
		MimeMessage message = mailSender.createMimeMessage();	
		result = setMailInfo(message, mailInfo);		
		mailSender.send(message);
		logger.info("Mail sending process is completed for {}", mailInfo.getMessageId());
		return result;
	}


	/**
	 *  Method compose mime message and sets the mail details
	 *  Following attributes are set
	 *  	. Subject - Expected to be non-empty
	 *  	. From Email- Expected to be non-empty
	 *  	. Priority - Input is expected to contain valid value (1-5). If value is not valid default prioroty is used
	 *  	. To - Non-Empty value is checked in inputs to field first, if not valid tos input field is used
	 *  	. bcc - Non-Empty value is checked in inputs bcc field first, if not valid bccs input field is used 	
	 *  	. cc - Non-Empty value is checked in inputs cc field first, if not valid ccs input field is used
	 *  Created Mail message content is HTML with UTF-8 encoding 	
	 */
	private boolean setMailInfo(MimeMessage message, MailInfo mailInfo){
		boolean result = false;
		try {
			
			MimeMessageHelper mimeMsgHelper = new MimeMessageHelper(message, MESSAGE_ENCODING);			
			mimeMsgHelper.setSubject(mailInfo.getSubject());
			mimeMsgHelper.setFrom(mailInfo.getFrom());
			
			if(mailInfo.getPriority() > 0 && mailInfo.getPriority()  < 6){
				mimeMsgHelper.setPriority(mailInfo.getPriority());
			}else{
				mimeMsgHelper.setPriority(MESSAGE_DEFAULT_PRIORITY);
			}
			
			if(StringUtils.isNotEmpty(mailInfo.getTo())){
				mimeMsgHelper.setTo(mailInfo.getTo());	
			}else{
				mimeMsgHelper.setTo(mailInfo.getTos());
			}
			
			if(StringUtils.isNotEmpty(mailInfo.getBcc())){
				mimeMsgHelper.setBcc(mailInfo.getBcc());	
			}else if(mailInfo.getBccs()!=null && mailInfo.getBccs().length > 0){
				mimeMsgHelper.setBcc(mailInfo.getBccs());
			}
			
			if(StringUtils.isNotEmpty(mailInfo.getCc())){
				mimeMsgHelper.setCc(mailInfo.getCc());	
			}else if(mailInfo.getCcs()!=null && mailInfo.getCcs().length > 0){
				mimeMsgHelper.setCc(mailInfo.getCcs());
			}

			mimeMsgHelper.setText(createMsgContent(mailInfo), MESSAGE_HTML);
			
			result = true;
		} catch (MessagingException e) {
			logger.error("Problem in setting mailerInfo. Error details: {}", e.toString());
		} catch (RuntimeException e) {
			logger.error("Problem in setting mailerInfo. Error details: {}", e.toString());
		}
		return result;
	}
	
	/**
	 * method creates message content.
	 * Composition is split into 2 stages.
	 * 1. Processing of given template
	 * 2. Adding processed message content to common template contains header and footer shared across templates
	 *  
	 */
	private String createMsgContent(MailInfo mailInfo) {
		Assert.notNull(mailInfo, "Mail Information request is empty. Mail processing is skipped");
		
		String msgBodyContent = processTemplate(mailInfo.getTemplateName(), mailInfo.getMessageParams());
		Assert.notNull(msgBodyContent, "Mail content is empty. Mail processing is skipped");
		Map<String, Object> msgBindParams = mailInfo.getMessageParams();
		if(msgBindParams==null){
			msgBindParams = new HashMap<String, Object>();
		}
		msgBindParams.put(MESSAGE_BODY_PARAM, msgBodyContent);
		String title = (String) msgBindParams.get(MESSAGE_TITLE_PARAM); 
		if(StringUtils.isEmpty(title)){
			 msgBindParams.put(MESSAGE_TITLE_PARAM, MESSAGE_TITLE_DEFAULT_VALUE);
		}
		String msgContent = processTemplate(MESSAGE_TEMPLATE_COMMON, msgBindParams);
		Assert.notNull(msgContent, "Mail content is empty.");
		logger.debug("Mail Message content: {} for mail template : {}", msgContent, mailInfo.getTemplateName());
		return msgContent;
	}
	
	/**
	 * Method process given template. If template name is empty, error message is logged 
	 */
	private String processTemplate(String templateName, Map<String, Object> bindData){
		Assert.notNull(templateName, "Mail Template Information is not provided, Skipping the mail process");
		String msgContent = null;
		logger.debug("Processing Mail Template: {}", templateName);
		try {
			Template template = fmConfig.getTemplate(templateName + TPL_EXTN);
			StringWriter tplContentWrtr = new StringWriter();
			template.process(bindData, tplContentWrtr);
			msgContent = tplContentWrtr.toString();
		} catch (IOException e) {
			logger.error("Error occured while loading Mail Template {}. Error details {}" 
										, templateName, e.toString());
		} catch (TemplateException e) {
			logger.error("Error occured while loading Mail Template {}. Error details {}" 
					, templateName, e.toString());
		}
		return msgContent;
	}


	public Configuration getFMConfiguration() {
		return this.fmConfig;
	}

}
