package com.rapidinstinct.ucap.plat.service.rest.pg;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.rapidinstinct.ucap.plat.service.pg.CreditCardInfo;
import com.rapidinstinct.ucap.plat.service.pg.PaymentService;
import com.rapidinstinct.ucap.plat.service.pg.PaymentServiceException;

/**
 * @author Shiva Kalgudi
 *
 */

@RestController
@RequestMapping("/payments")
public class PaymentController {
	private static Logger _LOG = LoggerFactory.getLogger(PaymentController.class);    
    @Autowired PaymentService payService;
    
	@RequestMapping(method = RequestMethod.POST, value = "/cc")
    public ResponseEntity<CreditCardInfo> storeCreditCard(@RequestBody @Valid CreditCardInfo cci, 
    											BindingResult result,
    											UriComponentsBuilder builder) throws PaymentServiceException {
    	_LOG.debug("Request for storing credit card ");
    	HttpHeaders headers = new HttpHeaders();
    	CreditCardInfo ccc = payService.storeCreditCard(cci); 
    	HttpStatus status = HttpStatus.CREATED;
    	if(ccc!=null){
            headers.setLocation(builder.path("/payments/cc/{id}").buildAndExpand(ccc.getId()).toUri());
    	}else{
    		status = HttpStatus.BAD_REQUEST;
    	}
        return new ResponseEntity<CreditCardInfo>(ccc, headers, status);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/cc/{ccid}")
    public CreditCardInfo readCreditCard(@PathVariable String ccid) throws PaymentServiceException{
    	_LOG.debug("Request for reading credit card ");
    	return payService.readCreditCard(ccid); 
    }
}
