package com.rapidinstinct.ucap.plat.service.pg.paypal;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.paypal.api.payments.Address;
import com.paypal.api.payments.CreditCard;
import com.rapidinstinct.ucap.plat.service.pg.CreditCardInfo;

/**
 * @author Shiva Kalgudi
 *
 */
@Component
public class PayPalBeanMapper {

	public CreditCard createCreditCard(CreditCardInfo cci){
		CreditCard cc = null;
		if(cci!=null){
			cc = new CreditCard();
			BeanUtils.copyProperties(cci, cc);
			if(cci.getBillingAddress()!=null){
				Address address = new Address();
				BeanUtils.copyProperties(cci.getBillingAddress(), address);
				cc.setBillingAddress(address);
			}
		}
		return cc;
	}

	public CreditCardInfo convertCreditCard(CreditCard cc){
		CreditCardInfo cci = null;
		if(cc!=null){
			cci = new CreditCardInfo();
			BeanUtils.copyProperties(cc, cci);
			if(cc.getBillingAddress()!=null){
				BeanUtils.copyProperties(cc.getBillingAddress(), cci.getBillingAddress());
			}
		}
		return cci;
	}
}
