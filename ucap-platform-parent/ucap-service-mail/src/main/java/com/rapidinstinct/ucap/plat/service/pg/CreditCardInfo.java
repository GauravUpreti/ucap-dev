package com.rapidinstinct.ucap.plat.service.pg;


/**
 * @author Shiva Kalgudi
 *
 */
public class CreditCardInfo {

	private String id;
	private String externalCustomerId;
	private String number;
	private String type;
	private int expireMonth;
	private int expireYear;
	private int cvv2;
	private String firstName;
	private String lastName;
	private Address billingAddress = new Address();

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getExternalCustomerId() {
		return externalCustomerId;
	}

	public void setExternalCustomerId(String externalCustomerId) {
		this.externalCustomerId = externalCustomerId;
	}

	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public int getExpireMonth() {
		return expireMonth;
	}
	
	public void setExpireMonth(int expireMonth) {
		this.expireMonth = expireMonth;
	}
	
	public int getExpireYear() {
		return expireYear;
	}
	
	public void setExpireYear(int expireYear) {
		this.expireYear = expireYear;
	}
	
	public int getCvv2() {
		return cvv2;
	}
	
	public void setCvv2(int cvv2) {
		this.cvv2 = cvv2;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public static class Address  {
		private String line1;
		private String line2;
		private String city;
		private String countryCode;
		private String postalCode;
		private String state;
		private String phone;

		public Address() { }

		public Address(String line1, String city, String countryCode) {
			this.line1 = line1;
			this.city = city;
			this.countryCode = countryCode;
		}

		public String getLine1() {
			return line1;
		}

		public void setLine1(String line1) {
			this.line1 = line1;
		}

		public String getLine2() {
			return line2;
		}

		public void setLine2(String line2) {
			this.line2 = line2;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getPostalCode() {
			return postalCode;
		}

		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}
	}

	
	public static enum CardType{
		VISA("visa"), MASTERCARD("mastercard"), DISCOVER("discover"), AMEX("amex");
		
		private CardType(final String text) {
			this.text = text;
		}

		private final String text;

		public String getValue() {
			return text;
		}
	}
}
