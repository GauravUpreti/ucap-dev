package com.rapidinstinct.ucap.plat.service.mail;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.rapidinstinct.ucap.plat.commons.util.ToStringBuilderFactory;

/**
 * 
 * @author Shiva Kalgudi
 * 
 * Mail Service Response Object
 */
public class MailSendResponse {

	//result of mail send service
	private boolean result;
	//message id value populated from request
	private String messageId;
	
	private String resultMessage;
	
	public boolean isResult() {
		return result;
	}
	
	public void setResult(boolean result) {
		this.result = result;
	}
	
	public String getMessageId() {
		return messageId;
	}
	
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public String toString() {
		ToStringBuilder out = ToStringBuilderFactory.build(this);
		out.appendSuper(super.toString());
		out.append("messageId", this.getMessageId());
		out.append("result", this.isResult());
		out.append("resultMessage", this.getResultMessage());
		return out.toString();
	}
	
}
