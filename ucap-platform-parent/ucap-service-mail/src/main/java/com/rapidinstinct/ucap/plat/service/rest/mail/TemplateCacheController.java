package com.rapidinstinct.ucap.plat.service.rest.mail;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.rapidinstinct.ucap.plat.service.mail.MailSender;


/**
 * 
 * @author Shiva Kalgudi
 *
 * Template controller class used to clear template cache
 */

@Controller
public class TemplateCacheController {

	private final static Logger logger = LoggerFactory.getLogger(TemplateCacheController.class);

	@Autowired
	MailSender mailSender;

	
	/**
	 * Method forward request to template invlidation form
	 */
	@RequestMapping(value="/templateForm", method=RequestMethod.GET)
	public ModelAndView loadTemplateForm(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("templateForm");
		return mav;
	}
	
	/**
	 * Method handles cache invalidation for given template 
	 */
	@RequestMapping(value="/invalidateCache", method=RequestMethod.POST)
	public ModelAndView invalidateCacheForTemplate(@RequestParam("templateName") String templateName){
		ModelAndView mav = new ModelAndView(); 
		logger.info("Invalidating cache for template :{}", templateName);
		if(StringUtils.isNotEmpty(templateName)){
			try {
				
				mailSender.getFMConfiguration().removeTemplateFromCache(templateName + MailSender.TPL_EXTN);				
				mav.setViewName("tplInvalidateSuccess");
				mav.addObject("msg", "Template :" + templateName + " cache invalidated successfully");
			} catch (IOException e) {
				logger.error("Error occured during cache template invlidation :{}, Error detais :{}", templateName, e.toString());
				mav.setViewName("templateForm");
				mav.addObject("errorMsg", "Error occured during cache template invlidation:" + e.toString());
			}	
		}else{			
			logger.error("Template Name is empty, returing default view");
			mav.addObject("errorMsg", "Template Name is empty");
			mav.setViewName("templateForm");
		}
		return mav;
	}
	
	/**
	 * 
	 * Method handles invalidating all templates cache 
	 */
	@RequestMapping(value="/invalidateCacheAll", method=RequestMethod.GET)
	public ModelAndView invalidateAlltemplatesCache(){
		ModelAndView mav = new ModelAndView(); 
		logger.info("Invalidating all templates cache");
		mailSender.getFMConfiguration().clearTemplateCache();				
		mav.setViewName("tplInvalidateSuccess");
		mav.addObject("msg", "All templates cache invalidation is successful");
		return mav;
	}

}
