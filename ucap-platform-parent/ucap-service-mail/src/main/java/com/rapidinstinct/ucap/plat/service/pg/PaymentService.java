package com.rapidinstinct.ucap.plat.service.pg;

import org.springframework.stereotype.Service;

/**
 * @author Shiva Kalgudi
 *
 */

@Service
public interface PaymentService {

	public CreditCardInfo storeCreditCard(CreditCardInfo cardInfo) throws PaymentServiceException;
	
	public CreditCardInfo readCreditCard(String cardId) throws PaymentServiceException;
}
