package com.rapidinstinct.ucap.plat.service.pg.paypal;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.paypal.base.ConfigManager;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.OAuthTokenCredential;
import com.paypal.base.rest.PayPalRESTException;
import com.paypal.base.rest.PayPalResource;
import com.rapidinstinct.ucap.plat.commons.util.SystemPropertiesUtil;
import com.rapidinstinct.ucap.plat.service.pg.PaymentGatewayError;

/**
 * @author Shiva Kalgudi
 *
 */

@Component
public class PayPalApiUtil {
	private final static Logger logger = LoggerFactory.getLogger(PayPalApiUtil.class);
	
	/**
	 * TODO - Move SDK property load to Spring config
	 */
	public PayPalApiUtil(){
		logger.info("Initializing PayPal SDK Configs\t");	
		try {
			String configPath = "/" + SystemPropertiesUtil.getDeploymentEnvironment() + "/paypal/sdk_config.properties";
			Resource payPalSDKProps = new ClassPathResource(configPath);
			InputStream is = payPalSDKProps.getInputStream();
			PayPalResource.initConfig(is);
			logger.info("Initialized PayPal SDK Configs");
		} catch (PayPalRESTException e) {
			logger.error("Error while initializing SDK Configs-->" + e.toString());
		} catch (IOException e) {
			logger.error("Error while initializing SDK Configs-->" + e.toString());		
		}
	}

	/**
	 * TODO - Store token in memcached till the token validity 
	 */
	public String getAccessToken() throws PayPalRESTException {
		String clientID = ConfigManager.getInstance().getConfigurationMap().get("clientID");
		String clientSecret = ConfigManager.getInstance().getConfigurationMap().get("clientSecret");
		return new OAuthTokenCredential(clientID, clientSecret).getAccessToken();
	}

	
	public APIContext createApiContext() throws PayPalRESTException{
		String token = getAccessToken();
		return new APIContext(token);
	}

	/**
	 * 	This workaround to extract is needed since the SDK implementation 
	 *  don't directly expose the error codes 
	 */
	public PaymentGatewayError extractErrorDetails(String message){
		PaymentGatewayError emsg = null;
		int jsonFIndex = StringUtils.indexOf(message, "{");
		if(jsonFIndex > 0 ){
			String jsonMsg = message.substring(jsonFIndex);
			String errorCodeContent = message.substring(0, jsonFIndex);
			String errorCode = StringUtils.split(errorCodeContent, " ")[3];
			emsg = new PaymentGatewayError(errorCode, jsonMsg);
		}
		return emsg;
	}
}
