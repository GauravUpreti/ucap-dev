package com.rapidinstinct.ucap.plat.service.mail;

import org.springframework.stereotype.Service;

import freemarker.template.Configuration;


/**
 * 
 * @author Shiva Kalgudi
 *
 * Mail send interface with common constants
 */
@Service
public interface MailSender {
	public static final String TPL_EXTN = ".ftl";
	public static final String MESSAGE_ENCODING = "UTF-8";
	public static final boolean MESSAGE_HTML = true;
	public static final int MESSAGE_DEFAULT_PRIORITY = 3;

	public boolean send(MailInfo mailInfo);
	
	public Configuration getFMConfiguration();
}
