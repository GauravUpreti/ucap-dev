Hi ${name},
 
You recently added a new email address to your Ucap account.
 
To confirm the address click this link, or paste it into your browser:
${loginURL}
 
Once you do that, we'll ask you to sign in.
 
Thanks for using Ucap!
The Ucap Team