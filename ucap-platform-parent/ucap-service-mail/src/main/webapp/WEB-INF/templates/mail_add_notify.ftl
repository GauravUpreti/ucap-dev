Hi ${name},
 
The email address ${nemail} was recently added to your Ucap account.
 
If you don't want that email associated with your account after all, here's how to remove it:
 

    Type ${loginURL} into your browser
    Sign in with your email address and password
    Find Primary email in the upper left and click Change/Add
    Click Remove next to the email address you're removing.

 
Thanks for using Ucap and for helping us to keep your account secure.
The Ucap Team