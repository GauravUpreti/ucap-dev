package com.rapidinstinct.ucap.plat.cache.test;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.rapidinstinct.ucap.plat.commons.util.ToStringBuilderFactory;

@SuppressWarnings("serial")
public class CachableBean implements Serializable{

	private String name;
	private String address;
	
	public CachableBean(String name, String address){
		this.name=name;
		this.address=address;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toString(){
		ToStringBuilder builder = ToStringBuilderFactory.build(this);
		builder.appendSuper(super.toString());
		builder.append("name", this.name);
		builder.append("address", this.address);
		return builder.toString();
	}
}
