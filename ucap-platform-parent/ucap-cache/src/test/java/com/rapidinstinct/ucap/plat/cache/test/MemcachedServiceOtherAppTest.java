package com.rapidinstinct.ucap.plat.cache.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rapidinstinct.ucap.plat.cache.CacheService;
import com.rapidinstinct.ucap.plat.cache.CacheServiceFactory;
import com.rapidinstinct.ucap.plat.commons.util.SystemPropertiesUtil;
import com.rapidinstinct.ucap.plat.commons.util.SystemPropertiesUtil.DeploymentEnvironment;

/**
 * 
 * @author Gaurav Upreti
 *
 */
public class MemcachedServiceOtherAppTest {

	private static CacheService service = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		SystemPropertiesUtil.setNamespace("ucap");
		SystemPropertiesUtil.setDeploymentEnvironment(DeploymentEnvironment.DEV);
		service = CacheServiceFactory.getMemcachedService();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if(service.getRepository()!=null){
			service.getRepository().shutdown();
		}
	}

	@Test
	public void testGetRepository() {
		assertNotNull(service.getRepository());
	}

	@Test
	public void testSetRepository() {		
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testClearAll() {
		service.clearAll();
	}

	@Test
	public void testContains() {
		service.put("key4", "ucap");
		assertTrue(service.contains("key4"));
	}

	@Test
	public void testDelete() {
		service.put("key5", "ucap");
		assertTrue(service.delete("key5"));
	}

	@Test
	public void testDeleteAll() {
		service.put("key-d1", "ucap");
		service.put("key-d2", "ucap");
		service.put("key-d3", "ucap");
		Set<String> deleteKeys = new HashSet<String>();
		for(int i=3; i<11; i++){
			deleteKeys.add("key-d" + i);
		}
		Collection<String> deletedKeys = service.deleteAll(deleteKeys);
		assertEquals(1, deletedKeys.size());
	}

	@Test
	public void testGet() {
		service.put("key", "ucap");
		assertEquals("ucap", service.get("key"));
	}

	@Test
	public void testGetAll() {
		service.put("key-g1", "ucap");
		service.put("key-g2", "ucap");
		service.put("key-g3", "ucap");
		Set<String> addedKeys = new HashSet<String>();
		for(int i=1; i<7; i++){
			addedKeys.add("key-g" + i);
		}
		List<Object>  retValues = service.getAll(addedKeys);		
		assertEquals(3, retValues.size());
	}

	@Test
	public void testIncrement() {
		service.delete("key-i1");
		service.put("key-i1", "1");
		service.increment("key-i1", 2);
		//String returnVal = (String) service.get("key-i1");		
		assertTrue(5 == service.increment("key-i1", 2) );
	}

	@Test
	public void testPutStringObject() {
		service.put("key1", "ucap");
		assertEquals("ucap", service.get("key1"));
	}

	@Test
	public void testPutStringObjectInt() {
		service.put("key3", "ucap", 1);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertNull(service.get("key3"));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testPutAllMapOfStringObject() {
		service.putAll(null);
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testPutAllMapOfStringObjectInt() {
		service.putAll(null,0);
	}
	
	
	@Test
	public void testObject() {
		for (int i = 1; i < 100; i++) {
			CachableBean user = new CachableBean("Shiva" + i, "Kalgudi" + i);
			service.put("name" + i, user);
		}
		
		for (int i = 1; i < 100; i++) {			
			CachableBean user = (CachableBean) service.get("name" + i);
			System.out.println(user.getName());
			assertNotNull(user);
		}
		
	}

}
