package com.rapidinstinct.ucap.plat.cache.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.rapidinstinct.ucap.plat.cache.CacheService;
import com.rapidinstinct.ucap.plat.cache.CacheServiceFactory;
import com.rapidinstinct.ucap.plat.cache.memcached.MemcachedRepository;

public class CacheServiceFactoryTest {

	@Test
	public void testGetMemcachedRepository() {
		MemcachedRepository repo = CacheServiceFactory.getMemcachedRepository();
		assertNotNull(repo);
		assertNotNull(repo.getNameSpace());
	}

	@Test
	public void testGetMemcachedService() {
		CacheService mservice = CacheServiceFactory.getMemcachedService();
		assertNotNull(mservice);
	}

}
