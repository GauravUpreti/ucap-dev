package com.rapidinstinct.ucap.plat.cache.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rapidinstinct.ucap.plat.cache.CacheService;
import com.rapidinstinct.ucap.plat.cache.CacheServiceFactory;
import com.rapidinstinct.ucap.plat.commons.util.SystemPropertiesUtil;
import com.rapidinstinct.ucap.plat.commons.util.SystemPropertiesUtil.DeploymentEnvironment;

/**
 * 
 * @author Gaurav Upreti
 *
 */
public class MemcachedServiceTest {

	private static CacheService service = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		SystemPropertiesUtil.setNamespace("ucap");
		SystemPropertiesUtil.setDeploymentEnvironment(DeploymentEnvironment.DEV);
		service = CacheServiceFactory.getMemcachedService();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if(service.getRepository()!=null){
			service.getRepository().shutdown();
		}
	}

	@Test
	public void testGetRepository() {
		assertNotNull(service.getRepository());
	}

	@Test
	public void testSetRepository() {		
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testClearAll() {
		service.clearAll();
	}

	@Test
	public void testContains() {
		service.put("program4", "rapdrp");
		assertTrue(service.contains("program4"));
	}

	@Test
	public void testDelete() {
		service.put("program5", "rapdrp");
		assertTrue(service.delete("program5"));
	}

	@Test
	public void testDeleteAll() {
		service.put("program-d1", "rapdrp");
		service.put("program-d2", "rapdrp");
		service.put("program-d3", "rapdrp");
		Set<String> deleteKeys = new HashSet<String>();
		for(int i=3; i<11; i++){
			deleteKeys.add("program-d" + i);
		}
		Collection<String> deletedKeys = service.deleteAll(deleteKeys);
		assertEquals(1, deletedKeys.size());
	}

	@Test
	public void testGet() {
		service.put("program", "rapdrp");
		assertEquals("rapdrp", service.get("program"));
	}

	@Test
	public void testGetAll() {
		service.put("program-g1", "rapdrp");
		service.put("program-g2", "rapdrp");
		service.put("program-g3", "rapdrp");
		Set<String> addedKeys = new HashSet<String>();
		for(int i=1; i<7; i++){
			addedKeys.add("program-g" + i);
		}
		List<Object>  retValues = service.getAll(addedKeys);		
		assertEquals(3, retValues.size());
	}

	@Test
	public void testIncrement() {
		service.delete("program-i1");
		service.put("program-i1", "1");
		service.increment("program-i1", 2);
		//String returnVal = (String) service.get("program-i1");		
		assertTrue(5 == service.increment("program-i1", 2) );
	}

	@Test
	public void testPutStringObject() {
		service.put("program1", "rapdrp");
		assertEquals("rapdrp", service.get("program1"));
	}

	@Test
	public void testPutStringObjectInt() {
		service.put("program3", "rapdrp", 1);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertNull(service.get("program3"));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testPutAllMapOfStringObject() {
		service.putAll(null);
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testPutAllMapOfStringObjectInt() {
		service.putAll(null,0);
	}
	
	
	@Test
	public void testObject() {
		for (int i = 1; i < 100; i++) {
			CachableBean user = new CachableBean("Shiva" + i, "Kalgudi" + i);
			service.put("name" + i, user);
		}
		
		for (int i = 1; i < 100; i++) {			
			CachableBean user = (CachableBean) service.get("name" + i);
			assertNotNull(user);
		}
		
	}

}
