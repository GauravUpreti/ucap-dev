package com.rapidinstinct.ucap.plat.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

import com.rapidinstinct.ucap.plat.cache.memcached.MemcachedNullService;
import com.rapidinstinct.ucap.plat.cache.memcached.MemcachedRepository;
import com.rapidinstinct.ucap.plat.cache.memcached.MemcachedService;
import com.rapidinstinct.ucap.plat.commons.util.SpringBeanLocator;

/**
 * 
 * @author Gaurav Upreti
 * 
 * Cache Service factory provides access to  underlying cache 
 * 
 */

public class CacheServiceFactory {

	private final static Logger logger = LoggerFactory.getLogger(CacheServiceFactory.class);

	//spring bean configuration file package name
	public static final String CONFIG_PACKAGE = "com.rapidinstinct.ucap.plat.cache.config";
	//spring bean configuration file package file name
	public static final String CONFIG_FILE = "cache-beans.xml";
	
	//Memcached Cache repository implementation spring bean name
	public static final String MEMCACHED_REPO_BEANID = "memcachedRepository";
	//Memcached Cache service implementation spring bean name
	public static final String MEMCACHED_SERVICE_BEANID = "memcachedService";
	
	//Reference to the singleton cache repository and cache service variables
	private static MemcachedRepository memcachedRepo = null;
	private static CacheService memcachedService = null;
	
	//static class block initializing the cache service and cache repository beans
	static{
		logger.info("Initializing CacheServiceFactory");
		try{
			memcachedRepo = SpringBeanLocator.getBean(CONFIG_PACKAGE, CONFIG_FILE, MEMCACHED_REPO_BEANID, MemcachedRepository.class);
			memcachedService = SpringBeanLocator.getBean(CONFIG_PACKAGE, CONFIG_FILE, MEMCACHED_SERVICE_BEANID, MemcachedService.class);
		}catch(BeansException e){
			logger.warn("Exception occurred when initializing memcache service {}", e);
			logger.warn("Using dummy cache service. Check server configuration/memcached server status");
			memcachedService = new MemcachedNullService();
		}
		logger.info("CacheServiceFactory Initializing is successful");
	}

	private CacheServiceFactory(){
		super();
	}
	
	
	/**
	 * Method returns reference to the Memcached cache repository..
	 * Repository provides access to Memecached client low-level APIs
	 */
	public static MemcachedRepository getMemcachedRepository(){
		return memcachedRepo; 
	}

	/**
	 * Method returns reference to the Memcached cache service
	 * Applications would use this service to interact with Memcached Server
	 */
	public static CacheService getMemcachedService(){
		return memcachedService;
	}

}
