package com.rapidinstinct.ucap.plat.cache.memcached;

import java.net.SocketAddress;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.rapidinstinct.ucap.plat.cache.CacheRepository;

import net.spy.memcached.CASResponse;
import net.spy.memcached.CASValue;
import net.spy.memcached.ConnectionObserver;
import net.spy.memcached.NodeLocator;
import net.spy.memcached.internal.BulkFuture;
import net.spy.memcached.transcoders.Transcoder;

/**
 * 
 * @author Gaurav Upreti
 * 
 * Memcached Repository interface methods
 */
public interface MemcachedRepository extends CacheRepository {
	  
	  public Collection<SocketAddress> getAvailableServers();

	  public Collection<SocketAddress> getUnavailableServers();

	  public Transcoder<Object> getTranscoder();

	  public NodeLocator getNodeLocator();

	  public Future<Boolean> append(long cas, String key, Object val);

	  public <T> Future<Boolean> append(long cas, String key, T val, Transcoder<T> tc);

	  public Future<Boolean> prepend(long cas, String key, Object val);

	  public <T> Future<Boolean> prepend(long cas, String key, T val, Transcoder<T> tc);

	  public <T> Future<CASResponse> asyncCAS(String key, long casId, T value, Transcoder<T> tc);

	  public Future<CASResponse> asyncCAS(String key, long casId, Object value);

	  public <T> CASResponse cas(String key, long casId, int exp, T value, Transcoder<T> tc);

	  public CASResponse cas(String key, long casId, Object value);

	  public <T> Future<Boolean> add(String key, int exp, T o, Transcoder<T> tc);

	  public Future<Boolean> add(String key, int exp, Object o);

	  public <T> Future<Boolean> set(String key, int exp, T o, Transcoder<T> tc);

	  public Future<Boolean> set(String key, int exp, Object o);

	  public <T> Future<Boolean> replace(String key, int exp, T o, Transcoder<T> tc);

	  public Future<Boolean> replace(String key, int exp, Object o);

	  public <T> Future<T> asyncGet(String key, Transcoder<T> tc);

	  public Future<Object> asyncGet(String key);

	  public Future<CASValue<Object>> asyncGetAndTouch(final String key, final int exp);

	  public <T> Future<CASValue<T>> asyncGetAndTouch(final String key, final int exp,final Transcoder<T> tc);

	  public CASValue<Object> getAndTouch(String key, int exp);

	  public <T> CASValue<T> getAndTouch(String key, int exp, Transcoder<T> tc);

	  public <T> Future<CASValue<T>> asyncGets(String key, Transcoder<T> tc);

	  public Future<CASValue<Object>> asyncGets(String key);

	  public <T> CASValue<T> gets(String key, Transcoder<T> tc);

	  public CASValue<Object> gets(String key);

	  public <T> T get(String key, Transcoder<T> tc);

	  public Object get(String key);

	  //public <T> BulkFuture<Map<String, T>> asyncGetBulk(Iterator<String> keys, Iterator<Transcoder<T>> tcs);	  
	  //public <T> BulkFuture<Map<String, T>> asyncGetBulk(Collection<String> keys,Iterator<Transcoder<T>> tcs);

	  //public <T> BulkFuture<Map<String, T>> asyncGetBulk(Iterator<String> keys, Transcoder<T> tc);
	  public <T> BulkFuture<Map<String, T>> asyncGetBulk(Collection<String> keys,   Transcoder<T> tc);

	  //public BulkFuture<Map<String, Object>> asyncGetBulk(Iterator<String> keys);
	  public BulkFuture<Map<String, Object>> asyncGetBulk(Collection<String> keys);

	  public <T> BulkFuture<Map<String, T>> asyncGetBulk(Transcoder<T> tc, String... keys);

	  public BulkFuture<Map<String, Object>> asyncGetBulk(String... keys);

	  //public <T> Map<String, T> getBulk(Iterator<String> keys, Transcoder<T> tc);
	  public <T> Map<String, T> getBulk(Collection<String> keys, Transcoder<T> tc);

	  //public Map<String, Object> getBulk(Iterator<String> keys);
	  public Map<String, Object> getBulk(Collection<String> keys);

	  public <T> Map<String, T> getBulk(Transcoder<T> tc, String... keys);

	  public Map<String, Object> getBulk(String... keys);

	  public <T> Future<Boolean> touch(final String key, final int exp,final Transcoder<T> tc);

	  public <T> Future<Boolean> touch(final String key, final int exp);

	  public Map<SocketAddress, String> getVersions();

	  public Map<SocketAddress, Map<String, String>> getStats();

	  public Map<SocketAddress, Map<String, String>> getStats(String prefix);

	  public long incr(String key, long by);

	  public long incr(String key, int by);

	  public long decr(String key, long by);

	  public long decr(String key, int by);

	  public long incr(String key, long by, long def, int exp);

	  public long incr(String key, int by, long def, int exp);

	  public long decr(String key, long by, long def, int exp);

	  public long decr(String key, int by, long def, int exp);

	  public Future<Long> asyncIncr(String key, long by);

	  public Future<Long> asyncIncr(String key, int by);

	  public Future<Long> asyncDecr(String key, long by);

	  public Future<Long> asyncDecr(String key, int by);

	  public long incr(String key, long by, long def);

	  public long incr(String key, int by, long def);

	  public long decr(String key, long by, long def);

	  public long decr(String key, int by, long def);

	  public Future<Boolean> delete(String key);

	  public Future<Boolean> flush(int delay);

	  public Future<Boolean> flush();

	  public void shutdown();

	  public boolean shutdown(long timeout, TimeUnit unit);

	  public boolean waitForQueues(long timeout, TimeUnit unit);

	  public boolean addObserver(ConnectionObserver obs);

	  public boolean removeObserver(ConnectionObserver obs);

}
