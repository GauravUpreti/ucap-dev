package com.rapidinstinct.ucap.plat.cache.memcached;

import java.net.SocketAddress;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import net.spy.memcached.CASResponse;
import net.spy.memcached.CASValue;
import net.spy.memcached.ConnectionObserver;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.NodeLocator;
import net.spy.memcached.internal.BulkFuture;
import net.spy.memcached.transcoders.Transcoder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.rapidinstinct.ucap.plat.cache.BaseCacheRepository;
import com.rapidinstinct.ucap.plat.commons.util.ToStringBuilderFactory;

/**
 * 
 * @author Gaurav Upreti
 *
 * Memcached Repository API implementation
 */
public class MemcachedRepositoryImpl extends BaseCacheRepository implements MemcachedRepository {

	private MemcachedClient memcachedClient = null;
	

	public MemcachedClient getMemcachedClient() {
		return memcachedClient;
	}

	public void setMemcachedClient(MemcachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}


	public Collection<SocketAddress> getAvailableServers() {
		return this.memcachedClient.getAvailableServers();
	}

	
	public Collection<SocketAddress> getUnavailableServers() {		
		return this.memcachedClient.getUnavailableServers();
	}

	
	public Transcoder<Object> getTranscoder() {		
		return this.memcachedClient.getTranscoder();
	}

	
	public NodeLocator getNodeLocator() {		
		return this.memcachedClient.getNodeLocator();
	}

	
	public Future<Boolean> append(long cas, String key, Object val) {		
		return this.memcachedClient.append(cas, prefixApplicationId(key), val);
	}

	
	public <T> Future<Boolean> append(long cas, String key, T val, Transcoder<T> tc) {		
		return this.memcachedClient.append(cas, prefixApplicationId(key), val, tc);
	}

	
	public Future<Boolean> prepend(long cas, String key, Object val) {		
		return this.memcachedClient.prepend(cas, prefixApplicationId(key), val);
	}

	
	public <T> Future<Boolean> prepend(long cas, String key, T val, Transcoder<T> tc) {		
		return this.memcachedClient.prepend(cas, prefixApplicationId(key), val, tc);
	}

	
	public <T> Future<CASResponse> asyncCAS(String key, long casId, T value, Transcoder<T> tc) {		
		return this.memcachedClient.asyncCAS(prefixApplicationId(key), casId, value, tc);
	}

	
	public Future<CASResponse> asyncCAS(String key, long casId, Object value) {		
		return this.memcachedClient.asyncCAS(prefixApplicationId(key), casId, value);
	}

	
	public <T> CASResponse cas(String key, long casId, int exp, T value, Transcoder<T> tc) {		
		return this.memcachedClient.cas(prefixApplicationId(key), casId, exp, value, tc);
	}

	
	public CASResponse cas(String key, long casId, Object value) {		
		return this.memcachedClient.cas(prefixApplicationId(key), casId, value);
	}

	
	public <T> Future<Boolean> add(String key, int exp, T o, Transcoder<T> tc) {		
		return this.memcachedClient.add(prefixApplicationId(key), exp, o, tc);
	}

	public Future<Boolean> add(String key, int exp, Object o) {		
		return this.memcachedClient.add(prefixApplicationId(key), exp, o);
	}

	
	public <T> Future<Boolean> set(String key, int exp, T o, Transcoder<T> tc) {		
		return this.memcachedClient.set(prefixApplicationId(key),  exp, o, tc);
	}

	
	public Future<Boolean> set(String key, int exp, Object o) {		
		return this.memcachedClient.set(prefixApplicationId(key),  exp, o);
	}

	
	public <T> Future<Boolean> replace(String key, int exp, T o, Transcoder<T> tc) {		
		return this.memcachedClient.replace(prefixApplicationId(key),  exp, o, tc);
	}

	
	public Future<Boolean> replace(String key, int exp, Object o) {		
		return this.memcachedClient.replace(prefixApplicationId(key),  exp, o);
	}

	
	public <T> Future<T> asyncGet(String key, Transcoder<T> tc) {		
		return this.memcachedClient.asyncGet(prefixApplicationId(key),  tc);
	}

	
	public Future<Object> asyncGet(String key) {		
		return this.memcachedClient.asyncGet(prefixApplicationId(key));
	}

	
	public Future<CASValue<Object>> asyncGetAndTouch(String key, int exp) {		
		return this.memcachedClient.asyncGetAndTouch(prefixApplicationId(key), exp);
	}

	
	public <T> Future<CASValue<T>> asyncGetAndTouch(String key, int exp, Transcoder<T> tc) {
		return this.memcachedClient.asyncGetAndTouch(prefixApplicationId(key), exp, tc);
	}

	
	public CASValue<Object> getAndTouch(String key, int exp) {		
		return this.memcachedClient.getAndTouch(prefixApplicationId(key), exp);
	}

	
	public <T> CASValue<T> getAndTouch(String key, int exp, Transcoder<T> tc) {
		return this.memcachedClient.getAndTouch(prefixApplicationId(key), exp, tc);
	}

	
	public <T> Future<CASValue<T>> asyncGets(String key, Transcoder<T> tc) {
		return this.memcachedClient.asyncGets(prefixApplicationId(key), tc);	
	}

	public Future<CASValue<Object>> asyncGets(String key) {
		return this.memcachedClient.asyncGets(prefixApplicationId(key));
	}

	
	public <T> CASValue<T> gets(String key, Transcoder<T> tc) {		
		return this.memcachedClient.gets(prefixApplicationId(key),tc);
	}

	
	public CASValue<Object> gets(String key) {
		return this.memcachedClient.gets(prefixApplicationId(key));
	}

	
	public <T> T get(String key, Transcoder<T> tc) {		
		return this.memcachedClient.get(prefixApplicationId(key), tc);
	}

	
	public Object get(String key) {		
		return this.memcachedClient.get(prefixApplicationId(key));
	}

	public <T> BulkFuture<Map<String, T>> asyncGetBulk(Collection<String> keys, Transcoder<T> tc) {
		return this.memcachedClient.asyncGetBulk(prefixApplicationId(keys), tc);
	}

	
	public BulkFuture<Map<String, Object>> asyncGetBulk(Collection<String> keys) {
		return this.memcachedClient.asyncGetBulk(prefixApplicationId(keys));
	}

	public <T> BulkFuture<Map<String, T>> asyncGetBulk(Transcoder<T> tc, String... keys) {
		return this.memcachedClient.asyncGetBulk(prefixApplicationId(keys), tc);
	}

	
	public BulkFuture<Map<String, Object>> asyncGetBulk(String... keys) {
		return this.memcachedClient.asyncGetBulk(prefixApplicationId(keys));	
	}

	public <T> Map<String, T> getBulk(Collection<String> keys, Transcoder<T> tc) {
		return this.memcachedClient.getBulk(prefixApplicationId(keys),tc);
	}

	
	public Map<String, Object> getBulk(Collection<String> keys) {		
		return this.memcachedClient.getBulk(prefixApplicationId(keys));
	}
	
	public <T> Map<String, T> getBulk(Transcoder<T> tc, String... keys) {		
		return this.memcachedClient.getBulk(prefixApplicationId(keys),tc);
	}

	
	public Map<String, Object> getBulk(String... keys) {		
		return this.memcachedClient.getBulk(prefixApplicationId(keys));
	}

	
	public <T> Future<Boolean> touch(String key, int exp, Transcoder<T> tc) {
		return this.memcachedClient.touch(prefixApplicationId(key), exp,tc);
	}

	
	public <T> Future<Boolean> touch(String key, int exp) {		
		return this.memcachedClient.touch(prefixApplicationId(key), exp);
	}

	
	public Map<SocketAddress, String> getVersions() {		
		return this.memcachedClient.getVersions();
	}

	
	public Map<SocketAddress, Map<String, String>> getStats() {		
		return this.memcachedClient.getStats();
	}

	
	public Map<SocketAddress, Map<String, String>> getStats(String prefix) {
		return this.memcachedClient.getStats(prefix);
	}

	
	public long incr(String key, long by) {		
		return this.memcachedClient.incr(prefixApplicationId(key), by);
	}

	
	public long incr(String key, int by) {		
		return this.memcachedClient.incr(prefixApplicationId(key), by);
	}

	
	public long decr(String key, long by) {		
		return this.memcachedClient.decr(prefixApplicationId(key), by);
	}

	
	public long decr(String key, int by) {
		return this.memcachedClient.decr(prefixApplicationId(key), by);
	}

	
	public long incr(String key, long by, long def, int exp) {		
		return this.memcachedClient.incr(prefixApplicationId(key), by, def, exp);
	}

	
	public long incr(String key, int by, long def, int exp) {		
		return this.memcachedClient.incr(prefixApplicationId(key), by, def, exp);
	}

	
	public long decr(String key, long by, long def, int exp) {
		return this.memcachedClient.decr(prefixApplicationId(key), by, def, exp);
	}

	
	public long decr(String key, int by, long def, int exp) {		
		return this.memcachedClient.decr(prefixApplicationId(key), by, def, exp);
	}

	
	public Future<Long> asyncIncr(String key, long by) {		
		return this.memcachedClient.asyncIncr(prefixApplicationId(key), by);
	}

	
	public Future<Long> asyncIncr(String key, int by) {		
		return this.memcachedClient.asyncIncr(prefixApplicationId(key), by);
	}

	
	public Future<Long> asyncDecr(String key, long by) {
		return this.memcachedClient.asyncDecr(prefixApplicationId(key), by);
	}

	
	public Future<Long> asyncDecr(String key, int by) {
		return this.memcachedClient.asyncDecr(prefixApplicationId(key), by);
	}

	
	public long incr(String key, long by, long def) {
		return this.memcachedClient.incr(prefixApplicationId(key), by, def);
	}

	
	public long incr(String key, int by, long def) {
		return this.memcachedClient.incr(prefixApplicationId(key), by, def);
	}

	
	public long decr(String key, long by, long def) {		
		return this.memcachedClient.decr(prefixApplicationId(key), by, def);
	}

	
	public long decr(String key, int by, long def) {		
		return this.memcachedClient.decr(prefixApplicationId(key), by, def);
	}

	
	public Future<Boolean> delete(String key) {		
		return this.memcachedClient.delete(prefixApplicationId(key));
	}

	
	public Future<Boolean> flush(int delay) {		
		return this.memcachedClient.flush(delay);
	}

	
	public Future<Boolean> flush() {		
		return this.memcachedClient.flush();
	}

	
	public void shutdown() {
		this.memcachedClient.shutdown();
	}

	
	public boolean shutdown(long timeout, TimeUnit unit) {		
		return this.memcachedClient.shutdown(timeout, unit);
	}

	
	public boolean waitForQueues(long timeout, TimeUnit unit) {
		return this.memcachedClient.waitForQueues(timeout, unit);
	}

	public boolean addObserver(ConnectionObserver obs) {		
		return this.memcachedClient.addObserver(obs);
	}

	public boolean removeObserver(ConnectionObserver obs) {		
		return this.memcachedClient.removeObserver(obs);
	}

	
	public String toString(){
		ToStringBuilder builder = ToStringBuilderFactory.build(this);
		builder.appendSuper(super.toString());
		builder.append("client", memcachedClient.toString());
		return builder.toString();
	}
}
