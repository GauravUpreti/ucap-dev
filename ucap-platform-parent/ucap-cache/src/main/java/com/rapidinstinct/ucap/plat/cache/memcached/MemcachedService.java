package com.rapidinstinct.ucap.plat.cache.memcached;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import net.spy.memcached.internal.BulkFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rapidinstinct.ucap.plat.cache.CacheService;

/** Gaurav Upreti
 * 
 * Memcached cache service implementation
 */

public class MemcachedService implements CacheService {

	private final static Logger logger = LoggerFactory.getLogger(MemcachedService.class);
	private MemcachedRepository repository = null;
	
	public MemcachedRepository getRepository() {
		return repository;
	}

	public void setRepository(MemcachedRepository repository) {
		this.repository = repository;
	}

	/**
	 * currently not supported due to the fact that cache is used by mutliple applications	
	 */
	public void clearAll() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Method checks for existance of key value.
	 * If key value is not NULL true is returned
	 */
	public boolean contains(String key) {
		boolean result = false;
		try{
			Future<Object> fvalue = this.repository.asyncGet(key);
			Object value = getObjectValue(fvalue);		
			result = value!=null;
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
		return result;
	}

	/**
	 * method deletes given key value and returns delete status
	 */
	public boolean delete(String key) {
		boolean result = false;
		try{
			Future<Boolean> fvalue = repository.delete(key);
			Boolean value = getBooleanValue(fvalue);
			result = value.booleanValue();
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
		return result;
	}

	/**
	 * Method deletes given key values from cache.
	 * It avoid performance hit, it is advised to provide unique set of keys
	 */
	public Set<String> deleteAll(Collection<String> keys) {
		Set<String> deletedKeys = null;
		try{
			deletedKeys = new HashSet<String>();
			for(String key: keys){	
				if(this.delete(key)){
					deletedKeys.add(key);
				}
			}
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
		return deletedKeys;
	}

	/**
	 * Method returns given key values from cache.
	 */
	public Object get(String key) {
		Object value = null;
		try{
			value = this.repository.get(key);
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
		return value;
	}

	
	/**
	 * Method returns given key values from cache.
	 * It avoid performance hit, it is advised to provide unique set of keys
	 */
	public List<Object> getAll(Collection<String> keys) {
		Map<String, Object> returnedValues = null;
		try{
			BulkFuture<Map<String, Object>> modifiedKeyValues = this.repository.asyncGetBulk(keys);		
			returnedValues = getMapObjectValue(modifiedKeyValues);		
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
		return Arrays.asList(returnedValues.values().toArray()) ;
	}

	/**
	 * Method increments given key value and returns incemeneted value
	 */
	public Long increment(String key, long delta) {
		try{
			return this.repository.incr(key, delta);
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
		return -1L;
	}

	/**
	 * method stores given key value in cache for default TTL time
	 */
	public void put(String key, Object value) {
		try{
			this.repository.set(key, DEFAULT_CACHE_TTL_SECONDS, value);
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
	}

	/**
	 * method stores given key value in cache for default TTL time (in seconds)
	 */
	public void put(String key, Object value, int ttl) {
		try{
			this.repository.set(key, ttl, value);
		}catch(Exception e){
			logger.warn("Exception occurred :{}", e.toString());
		}
	}

	/**
	 * native library doesn't support	
	 */
	public void putAll(Map<String, Object> values) {		
		throw new UnsupportedOperationException();
	}
	
	/**
	 * native library doesn't support	
	 */
	public void putAll(Map<String, Object> values, int ttl) {
		throw new UnsupportedOperationException();
	}
	

	/**
	 * helper method to read boolean type value for future object 
	 */
	private boolean getBooleanValue(Future<Boolean> fvalue){
		Boolean value = null;		
		try {
			value = fvalue.get(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			fvalue.cancel(false);
		} catch (InterruptedException e ) {			
			logger.warn("Exception occured while accessing value from memcache : {}" + e.toString());
		} catch (ExecutionException e) {
			logger.warn("Exception occured while accessing value from memcache : {}" + e.toString());
		}
		return value.booleanValue();
	
	}

	/**
	 * helper method to read object type value for future object 
	 */
	private Object getObjectValue(Future<Object> fvalue){
		Object value = null;		
		try {
			value = fvalue.get(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			fvalue.cancel(false);
		} catch (InterruptedException e ) {
			logger.warn("Exception occured while accessing value from memcache : {}" + e.toString());
		} catch (ExecutionException e) {
			logger.warn("Exception occured while accessing value from memcache : {}" + e.toString());
		}
		return value;
	}

	/**
	 * helper method to read map object type value for future object 
	 */
	private Map<String,Object> getMapObjectValue(BulkFuture<Map<String, Object>> bvalues){
		Map<String,Object> values = null;		
		try {
			values = bvalues.getSome(READ_TIMEOUT_SECONDS*10, TimeUnit.SECONDS);
		} catch (InterruptedException e ) {			
			logger.warn("Exception occured while accessing value from memcache : {}" + e.toString());
		} catch (ExecutionException e) {
			logger.warn("Exception occured while accessing value from memcache : {}" + e.toString());
		}
		return values;
	}
	

}
