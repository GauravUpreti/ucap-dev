package com.rapidinstinct.ucap.plat.cache;

/**
 * 
 * @author Gaurav Upreti
 *
 * Cache reposiorty interface
 */
public interface CacheRepository {
	
	  //implementation method provides access to the application id system environment variable	
	  public String getNameSpace();

}
