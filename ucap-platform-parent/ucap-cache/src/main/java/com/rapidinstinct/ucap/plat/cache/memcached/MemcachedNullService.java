package com.rapidinstinct.ucap.plat.cache.memcached;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.rapidinstinct.ucap.plat.cache.CacheService;

/**
 * @author Gaurav Upreti
 *
 */

public class MemcachedNullService implements CacheService {

	
	public MemcachedRepository getRepository() {
		return null;
	}

	@Override
	public void clearAll() {

	}

	@Override
	public boolean contains(String key) {
		return false;
	}

	@Override
	public boolean delete(String key) {
		return false;
	}

	@Override
	public Collection<String> deleteAll(Collection<String> keys) {
		return null;
	}

	@Override
	public Object get(String key) {
		return null;
	}

	@Override
	public List<Object> getAll(Collection<String> keys) {
		return null;
	}

	@Override
	public Long increment(String key, long delta) {
		return null;
	}

	@Override
	public void put(String key, Object value) {
		
	}

	@Override
	public void put(String key, Object value, int ttl) {
	}

	@Override
	public void putAll(Map<String, Object> values) {
		
	}

	@Override
	public void putAll(Map<String, Object> values, int ttl) {

	}

	
}
