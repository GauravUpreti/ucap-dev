package com.rapidinstinct.ucap.plat.cache;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.rapidinstinct.ucap.plat.cache.memcached.MemcachedRepository;

/**
 * 
 * @author Gaurav Upreti
 *
 * Cache API to be implemented by various cache providers.
 */

public interface CacheService {

	//default read timeout in seconds
	public static int READ_TIMEOUT_SECONDS = 5;	
	
	//default cache TTL in seconds - 24 Hrs	
	public static int DEFAULT_CACHE_TTL_SECONDS = 3600*24;

	/**
	 * Flush all keys and values from cache.
	 */
	public void clearAll();

	/**
	 * Method checks existence of key in the cache.
	 * Returns true if cache is not found or value of the key is NULL
	 */
	public boolean contains(String key);

	/**
	 * Method deletes key from cache.
	 *  If cache provider natively supports providing delete status, same status is returned.
	 *  In case native support is not provided, this method returns true
	 */
	public boolean delete(String key);

	/**
	 * Method deletes set of given keys and returns the successfully deleted keys.
	 * if cache provider doesn't provide the delete status, given key set is returned. 
	 */
	public Collection<String> deleteAll(Collection<String> keys);

	/**
	 * Method returns value of given key
	 */
	public Object get(String key);

	/**
	 * method returns values for given set of keys. 
	 * It is important for invoking application to make sure keys are unique. 
	 * Providing duplicate keys will have performance impact 
	 */
	public List<Object> getAll(Collection<String> keys);

	/**
	 * method increments the given key value by delta
	 */
	public Long increment(String key, long delta);

	/**
	 * method stores provided key and value in cache 
	 */
	public void put(String key, Object value);

	/**
	 * method stores provided key and value in cache with specified ttl.
	 * TTL is seconds 
	 */
	public void put(String key, Object value, int ttl);

	/**
	 * method puts all the keys and values provided in input map into cache 
	 */
	public void putAll(Map<String, Object> values);

	/**
	 * method puts all the keys and values provided in input map into cache with specified ttl 
	 */
	public void putAll(Map<String, Object> values, int ttl);
	
	public MemcachedRepository getRepository();
}
