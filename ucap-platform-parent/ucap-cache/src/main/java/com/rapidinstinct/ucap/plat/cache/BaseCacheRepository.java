package com.rapidinstinct.ucap.plat.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import com.rapidinstinct.ucap.plat.commons.util.SystemPropertiesUtil;


/**
 * 
 * @author Gaurav Upreti
 *
 * Base cache repository class containing the common methods.
 * Common methods provide functionality to handle application data partition/application name space
 */
public abstract class BaseCacheRepository implements CacheRepository{
	//application id property used for data partition/application name space
	 private String namespace = null;
	 //cache key prefix separator. key is modified to <APPNAME>:<GIVEN KEY>
	 public static final String APPNAME_KEY_SEPERATOR = ":";

	//method provides application id. 
	public String getNameSpace() {
		namespace = namespace!=null ? namespace : SystemPropertiesUtil.getNamespace();
		return namespace;
	}
	
	//application id property setter method
	public void setNamespace(String applicationId) {
		this.namespace = applicationId;
	}

	/**
	 * Method prefix application id to avoid key clash by multiple applications
	 */
	public String prefixApplicationId(String key){
		return (new StringBuffer().append(getNameSpace()) + APPNAME_KEY_SEPERATOR + key).toString();
	}
	
	/**
	 * Method prefix application id to provided keys 
	 */
	public Collection<String> prefixApplicationId(String ...keys){
		String[] currentKeys = ArrayUtils.toArray(keys);
		List<String> newKeys = new ArrayList<String>();		
		for(String key:currentKeys){
			newKeys.add(prefixApplicationId(key));
		}		
		return newKeys;
	}

	/**
	 * Method prefix application id to provided keys 
	 */
	public Collection<String> prefixApplicationId(Collection<String> keys){		
		List<String> newKeys = new ArrayList<String>();		
		for(String key:keys){
			newKeys.add(prefixApplicationId(key));
		}		
		return newKeys;
	}

	
	/**
	 * Method prefix application id to provided keys in the map 
	 */
	public Map<String, Object> prefixApplicationId(Map<String, Object> values){
		if(values==null){
			return null;
		}
		Map<String, Object> updatedKeys = new HashMap<String, Object>();
		for(String orgKey:values.keySet()){
			updatedKeys.put(prefixApplicationId(orgKey), values.get(orgKey));
		}
		return updatedKeys;
	}
}


